var mustache = require("mustache")
var fs = require('fs')

var tmpl = fs.readFileSync("custom_help.tmpl", "utf8")
var data = JSON.parse(fs.readFileSync("custom_help.json", "utf8"))
var stream = fs.createWriteStream("custom_help.html")
stream.once('open', function(fd) {
	stream.write(mustache.to_html(tmpl, data))
	stream.end();
});
