/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "color_gen.h"

BsColorGen::BsColorGen()
{
	m_predefinedColors.append(0xFF800000);
	m_predefinedColors.append(0xFF008080);
	m_predefinedColors.append(0xFF0000ff);
	m_predefinedColors.append(0xFF660066);
	m_predefinedColors.append(0xFF333333);
	m_predefinedColors.append(0xFFff7373);
	m_predefinedColors.append(0xFF088da5);
	m_predefinedColors.append(0xFF008000);
	m_predefinedColors.append(0xFF468499);
	m_predefinedColors.append(0xFFcc0000);
	m_predefinedColors.append(0xFF800080);
	m_predefinedColors.append(0xFF191970);
	m_predefinedColors.append(0xFF006400);
	m_predefinedColors.append(0xFFff8d00);
}

/**
 * Return a visually appealing color
 * @return color
 */
QColor BsColorGen::color()
{
	if (m_predefinedColors.isEmpty()) {
		return generate();
	}

	return QColor(m_predefinedColors.takeFirst());
}

/**
 * Generate a visually appealing color
 * @return color
 */
QColor BsColorGen::generate()
{
	/*
	 * values[0] : Hue (0 .. 360)
	 * values[1] : Saturation (0...1)
	 * values[2] : Value (0...1)
	 */
	qreal hue = (qrand() / (qreal) RAND_MAX);
	return QColor::fromHsvF(hue, 0.8f, 0.5f);
}
