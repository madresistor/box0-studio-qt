/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_COLOR_BUTTON_H
#define BOX0_STUDIO_COLOR_BUTTON_H

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QColor>

class BsColorButton : public QPushButton
{
	Q_OBJECT

	Q_PROPERTY(QColor color
		READ color
		WRITE setColor
		NOTIFY colorChanged
		USER true)

	public:
		explicit BsColorButton(const QColor &color, QWidget *parent = Q_NULLPTR);
		explicit BsColorButton(QWidget *parent = Q_NULLPTR) :
			BsColorButton(Qt::black, parent) {};
		QColor color();
		void setIconSize(const QSize &size);

	Q_SIGNALS:
		void colorChanged(const QColor &color);

	public Q_SLOTS:
		void setColor(const QColor &color);
		void chooseColor();

	private:
		void updateIcon();
		QColor m_color;
};

#endif
