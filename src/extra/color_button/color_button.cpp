/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "color_button.h"
#include <QColorDialog>

/* http://stackoverflow.com/a/283262/1500988 */

static const QSize DEF_ICON_SIZE(16, 16);

BsColorButton::BsColorButton(const QColor &color, QWidget *parent) :
	QPushButton(parent),
	m_color(color)
{
	setIconSize(DEF_ICON_SIZE);
	connect(this, SIGNAL(clicked()), SLOT(chooseColor()));
}

void BsColorButton::updateIcon()
{
	QPixmap pm(iconSize());
	pm.fill(m_color);
	setIcon(pm);
}

QColor BsColorButton::color()
{
	return m_color;
}

void BsColorButton::setColor(const QColor &color)
{
	if (m_color != color) {
		m_color = color;
		updateIcon();
		Q_EMIT colorChanged(color);
	}
}

void BsColorButton::chooseColor()
{
	QColorDialog colorDialog(m_color, this);
	if (colorDialog.exec() == QDialog::Accepted) {
		setColor(colorDialog.selectedColor());
	}
}

void BsColorButton::setIconSize(const QSize &size)
{
	QPushButton::setIconSize(size);
	updateIcon();
}
