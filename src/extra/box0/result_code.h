/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_BOX0_RESULT_CODE_H
#define BOX0_STUDIO_BOX0_RESULT_CODE_H

#include <QWidget>
#include <QObject>
#include <QMessageBox>
#include <libbox0/libbox0.h>

/**
 * Manage libbox0 result code
 */
class BsResultCode : public QMessageBox
{
	Q_OBJECT

	public:
		BsResultCode(QWidget *parent = Q_NULLPTR);
		BsResultCode(b0_result_code r, QWidget *parent = Q_NULLPTR);

		void setResultCode(b0_result_code r);
		void setTitle(const QString & title);
		void setMessage(const QString & msg);
		static bool handleInternal(b0_result_code r, const QString & msg);
};

#endif
