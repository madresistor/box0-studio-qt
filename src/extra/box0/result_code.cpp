/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "result_code.h"
#include <libbox0/libbox0.h>
#include <QDebug>
#include "box0.h"

BsResultCode::BsResultCode(QWidget *parent) :
	QMessageBox(parent)
{
}

BsResultCode::BsResultCode(b0_result_code r, QWidget *parent) :
	QMessageBox(parent)
{
	setResultCode(r);
}

/**
 * Set libbox result code @a r.
 * The result code is used to extract the name and explaination of the result code.
 * if result-code >= 0, show as information
 * if result-code < 0, show as warning
 */
void BsResultCode::setResultCode(b0_result_code r)
{
	QString name, explain;
	setIcon((B0_ERR_RC(r)) ? QMessageBox::Warning : QMessageBox::Information);

	name = utf8_to_qstring(b0_result_name(r));
	explain = utf8_to_qstring(b0_result_explain(r));

	QString details = QString("%1 \n %2").arg(name).arg(explain);
	setDetailedText(details);

	setStandardButtons(QMessageBox::Ok);
	setEscapeButton(QMessageBox::Ok);
}

/**
 * Set result code dialog title
 * @param title Dialog title
 */
void BsResultCode::setTitle(const QString & title)
{
	setWindowTitle(title);
}

/**
 * Set a context specific message.
 * This message is shown to user.
 * @param msg Message
 */
void BsResultCode::setMessage(const QString & msg)
{
	setText(msg);
}

/**
 * A static member to handle result-code in background.
 * nothing is shown to user. instead the information is output using qWarning().
 * currently, if the result-code >= 0 (success), then they are ingored.
 * @param r Result code
 * @param msg Context specific message
 * @return false if error result code
 */
bool BsResultCode::handleInternal(b0_result_code r, const QString & msg)
{
	if (B0_SUC_RC(r)) {
		return true;
	}

	QString name = utf8_to_qstring(b0_result_name(r));
	QString explain = utf8_to_qstring(b0_result_explain(r));

	qWarning() << msg << "[" << name << ": " << explain << "]";
	return false;
}
