/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tab_detach.h"
#include <QIcon>
#include <QLayout>

BsTabDetach::BsTabDetach(QWidget *parent) :
	QPushButton(parent),
	m_detached(false) /* by default in attached state */
{
	setIconSize(QSize(16, 16));
	update();
}

bool BsTabDetach::isDetached(void)
{
	return m_detached;
}

void BsTabDetach::setDetached(bool detached)
{
	if (m_detached == detached) {
		return;
	}

	m_detached = detached;
	update();
}

void BsTabDetach::update()
{
	setToolTip(m_detached ? "Attach" : "Detach");
	setIcon(QIcon(m_detached ? ":/extra/tab-attach" : ":/extra/tab-detach"));
}
