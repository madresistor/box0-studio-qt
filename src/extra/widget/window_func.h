/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_WIDGET_WINDOW_FUNC_H
#define BOX0_STUDIO_WIDGET_WINDOW_FUNC_H

#include <QComboBox>
#include <QWidget>
#include <QObject>

class BsWindowFunc : public QComboBox
{
	Q_OBJECT

	public:
		enum Type {
			Rectangular,
			Hanning,
			Hamming,
			Triangular,
			Welch,
			FlatTop,
			Blackman
		};
		BsWindowFunc(QWidget *parent = Q_NULLPTR);
		Type currentType();
		static float calc(Type type, uint i, uint N);

	Q_SIGNALS:
		void currentTypeChanged(BsWindowFunc::Type type);

	private Q_SLOTS:
		void glueCurrentIndexChanged();

	private:
		void addEntry(const char *icon_name, const QString &name, Type value);
};

Q_DECLARE_METATYPE(BsWindowFunc::Type)

#endif
