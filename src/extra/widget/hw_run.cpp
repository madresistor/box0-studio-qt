/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw_run.h"
#include <QIcon>

BsHwRun::BsHwRun(QObject *parent) :
	QAction(parent),
	m_running(false)
{
	update();

	connect(this, SIGNAL(triggered()), SLOT(triggerToStateConv()));

	/*
	 * Using this timer, application will send a restart request.
	 * A immediate userWantToStop() signal is emitted.
	 * and after a dead-time (user may be changing more values)
	 *  userWantToStart() signal is emitted.
	 *
	 * If the state is already not running, the call is ignored.
	 * If the timer is already running, restart it.
	 *
	 * dead-time: 300ms should be enough
	 */
	restartTimer = new QTimer(this);
	restartTimer->setInterval(BS_HW_RUN_DEAD_TIME);
	restartTimer->setSingleShot(true);
	connect(restartTimer, SIGNAL(timeout()), SLOT(performRestart()));
}

/**
 * Set mode of the button
 * @param running true to assign runing state
 */
void BsHwRun::setRunning(bool running)
{
	if (m_running == running) {
		return;
	}

	m_running = running;
	update();
}

/**
 * Get the state
 * @return true if running
 */
bool BsHwRun::isRunning(void)
{
	return m_running;
}

/**
 * slot called when button is clicked
 * based on the current state, it will emit signal to start/stop
 * @note this method do not automaically change it state.
 *    @a setRunning() need to be called
 */
void BsHwRun::triggerToStateConv()
{
	if (!m_running) {
		Q_EMIT userWantToStart();
	} else {
		Q_EMIT userWantToStop();
	}
}

/**
 * Update the view of the button
 */
void BsHwRun::update()
{
	setText(m_running ? "Stop" : "Start");
	setIcon(QIcon(m_running ? ":/hardware/stop" : ":/hardware/start"));
}

void BsHwRun::restart()
{
	if (m_running) {
		restartTimer->start();
	}
}

void BsHwRun::performRestart()
{
	/* maybe user is so fast that can send a restart as well as stop it! */
	if (m_running) {
		Q_EMIT userWantToStop();
		Q_EMIT userWantToStart();
	}
}
