/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bitsize.h"

BsBitsize::BsBitsize(QWidget *parent) :
	QComboBox(parent)
{
	connect(this, SIGNAL(currentIndexChanged(int)), SLOT(glueCurrentIndexChange()));
}

/**
 * Add a bitsize value to dropdown
 * @param value Bitsize value
 * @note if the value already exists, it is ignored
 */
void BsBitsize::addValue(uint value)
{
	if (findData(value) != -1) {
		return;
	}

	QString v = QString("%1 bits").arg(value);
	addItem(v, value);
}

/**
 * Get the current bitsize value
 * @return current-bitsize-value
 */
uint BsBitsize::currentValue()
{
	return currentData().toUInt();
}

void BsBitsize::glueCurrentIndexChange()
{
	Q_EMIT currentValueChanged(currentValue());
}
