/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015, 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "speed.h"

BsSpeed::BsSpeed(QWidget *parent) :
	QComboBox(parent),
	m_unit("SPS") /* backward compatible */
{
}

void BsSpeed::addValue(uint value)
{
	QString v;
	if (value < 1E3) {
		v = QString("%1 %2").arg(value);
	} else if (value < 1E6) {
		v = QString("%1 K%2").arg(value / 1.0E3);
	} else {
		v = QString("%1 M%2").arg(value / 1.0E6);
	}

	addItem(v.arg(m_unit), value);
}

uint BsSpeed::currentValue()
{
	return currentData().toUInt();
}
