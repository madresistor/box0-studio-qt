/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "window_func.h"
#include "extra/sigproc/window_func.h"

BsWindowFunc::BsWindowFunc(QWidget *parent) :
	QComboBox(parent)
{
	QSize iconSize(24, 24);
	setIconSize(iconSize);

	addEntry("rectangular", "Rectangular", Rectangular);
	addEntry("hamming", "Hamming", Hamming);
	addEntry("hanning", "Hanning", Hanning);
	addEntry("triangular", "Triangular", Triangular);
	addEntry("welch", "Welch", Welch);
	addEntry("flat-top", "Flat Top", FlatTop);
	addEntry("blackman", "Blackman", Blackman);

	/* current index changed to current type changed */
	connect(this, SIGNAL(currentIndexChanged(int)),
		SLOT(glueCurrentIndexChanged()));
}

void BsWindowFunc::addEntry(const char *icon_name, const QString &name, Type value)
{
	QString path = QString(":/window-func/%1").arg(icon_name);
	QIcon icon(path);
	addItem(icon, name, QVariant::fromValue(value));
}

BsWindowFunc::Type BsWindowFunc::currentType()
{
	return currentData().value<BsWindowFunc::Type>();
}

void BsWindowFunc::glueCurrentIndexChanged()
{
	Q_EMIT currentTypeChanged(currentType());
}

float BsWindowFunc::calc(BsWindowFunc::Type type, uint i, uint N)
{
	switch(type) {
	case Hamming: return bs_wfunc_hamming(i, N);
	case Hanning: return bs_wfunc_hanning(i, N);
	case Rectangular: return 1;
	case Triangular: return bs_wfunc_triangular(i, N);
	case Welch: return bs_wfunc_welch(i, N);
	case FlatTop: return bs_wfunc_flat_top(i, N);
	case Blackman: return bs_wfunc_blackman(i, N);
	}

	/* something bad is going on! */
	return 1;
}
