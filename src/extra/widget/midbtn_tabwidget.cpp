/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ref: http://www.qtcentre.org/threads/17334 */

#include "midbtn_tabwidget.h"
#include <QTabBar>
#include <QMouseEvent>

BsMidBtnTabWidget::BsMidBtnTabWidget(QWidget *parent) :
	QTabWidget(parent),
	m_targetTab(-1)
{
	tabBar()->installEventFilter(this);
}

bool BsMidBtnTabWidget::eventFilter(QObject *o, QEvent *e)
{
	/* let it process */
	bool ret = QTabWidget::eventFilter(o, e);

	QTabBar *tab_bar = tabBar();
	if (o == tab_bar) {
		/* selective mouse only */
		QEvent::Type type = e->type();
		if (type != QEvent::MouseButtonPress && type != QEvent::MouseButtonRelease) {
			return ret;
		}

		/* middle button only */
		QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(e);
		if (mouseEvent->button() != Qt::MidButton) {
			return ret;
		}

		/* get the tab */
		int tab = tab_bar->tabAt(mouseEvent->pos());
		if (tab == -1) {
			m_targetTab = -1;
			return ret;
		}

		/* processs the event */
		if (type == QEvent::MouseButtonPress) {
			m_targetTab = tab;
		} else if (type == QEvent::MouseButtonRelease) {
			if (m_targetTab == tab) {
				Q_EMIT tabCloseRequested(tab);
			}

			m_targetTab = -1;
		}

		return true;
	}

	return ret;
}
