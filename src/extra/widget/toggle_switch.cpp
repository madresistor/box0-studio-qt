/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "toggle_switch.h"
#include <QPainter>
#include <QDebug>
#include <QSizePolicy>
#include <QSize>
#include <QSvgRenderer>

QSvgRenderer* BsToggleSwitch::pin_normal_upImage = Q_NULLPTR;
QSvgRenderer* BsToggleSwitch::pin_normal_downImage = Q_NULLPTR;

QSvgRenderer* BsToggleSwitch::pin_disable_upImage = Q_NULLPTR;
QSvgRenderer* BsToggleSwitch::pin_disable_downImage = Q_NULLPTR;

BsToggleSwitch::BsToggleSwitch(QWidget *parent) :
	QAbstractButton(parent),
	m_output(true)
{
	if (pin_normal_upImage == Q_NULLPTR) {
		pin_normal_upImage = new QSvgRenderer(QString(":/toggle-svg/normal/up"));
	}

	if (pin_disable_upImage == Q_NULLPTR) {
		pin_disable_upImage = new QSvgRenderer(QString(":/toggle-svg/disable/up"));
	}

	if (pin_normal_downImage == Q_NULLPTR) {
		pin_normal_downImage = new QSvgRenderer(QString(":/toggle-svg/normal/down"));
	}

	if (pin_disable_downImage == Q_NULLPTR) {
		pin_disable_downImage = new QSvgRenderer(QString(":/toggle-svg/disable/down"));
	}

	/* input mode led color */
	onColor1 = QColor(255, 0 ,0);
	onColor2 = QColor(192, 0, 0);
	offColor1 = QColor(28, 0, 0);
	offColor2 = QColor(128, 0, 0);

	//setMinimumSize(24,24);
	setCheckable(true);
	setChecked(false);

	update();
}

void BsToggleSwitch::setOutput(bool output)
{
	m_output = output;
	update();
}

void BsToggleSwitch::paintEvent(QPaintEvent *event)
{
	if (m_output) {
		outputPaintEvent(event);
	} else {
		inputPaintEvent(event);
	}
}

void BsToggleSwitch::resizeEvent(QResizeEvent */* event */)
{
	update();
}

void BsToggleSwitch::outputPaintEvent(QPaintEvent */* event */)
{
	QSvgRenderer *arr[2 /* ENABLED */][2 /* CHECKED */] = {
		{pin_disable_downImage, pin_disable_upImage},
		{pin_normal_downImage, pin_normal_upImage}
	};

	QPainter painter(this);
	QSvgRenderer *svg = arr[isEnabled() ? 1 : 0][isChecked() ? 1 : 0];

	qreal w = width(), h = height();
	qreal l = qMin(w, h);
	svg->render(&painter, QRectF((w - l) / 2, (h - l) / 2, l, l));
}


/* This method is taken from
 * Project: http://qt-apps.org/content/show.php/QLedIndicator?content=118610
 * Licence: LGPL 3.0 or later
 * Author: Tn <thenobody@poczta.fm>
 */
void BsToggleSwitch::inputPaintEvent(QPaintEvent */* event */) {
	qreal realSize = qMin(width(), height());

	QRadialGradient gradient;
	QPainter painter(this);
	QPen pen(Qt::black);
	pen.setWidth(1);

	qreal scaledSize = 1500;

	painter.setRenderHint(QPainter::Antialiasing);
	painter.translate(width()/2, height()/2);
	painter.scale(realSize/scaledSize, realSize/scaledSize);

	gradient = QRadialGradient (QPointF(-500,-500), 1500, QPointF(-500,-500));
	gradient.setColorAt(0, QColor(224,224,224));
	gradient.setColorAt(1, QColor(28,28,28));
	painter.setPen(pen);
	painter.setBrush(QBrush(gradient));
	painter.drawEllipse(QPointF(0,0), 500, 500);

	gradient = QRadialGradient (QPointF(500,500), 1500, QPointF(500,500));
	gradient.setColorAt(0, QColor(224,224,224));
	gradient.setColorAt(1, QColor(28,28,28));
	painter.setPen(pen);
	painter.setBrush(QBrush(gradient));
	painter.drawEllipse(QPointF(0,0), 450, 450);

	painter.setPen(pen);
	if (isChecked()) {
		gradient = QRadialGradient (QPointF(-500,-500), 1500, QPointF(-500,-500));
		gradient.setColorAt(0, onColor1);
		gradient.setColorAt(1, onColor2);
	} else {
		gradient = QRadialGradient (QPointF(500,500), 1500, QPointF(500,500));
		gradient.setColorAt(0, offColor1);
		gradient.setColorAt(1, offColor2);
	}
	painter.setBrush(gradient);
	painter.drawEllipse(QPointF(0,0), 400, 400);
}

bool BsToggleSwitch::hitButton(const QPoint &pos) const
{
	if (m_output) {
		return QAbstractButton::hitButton(pos);
	}

	return false;
}

void BsToggleSwitch::click()
{
	if (m_output) {
		QAbstractButton::click();
	}
}

void BsToggleSwitch::animateClick(int msec)
{
	if (m_output) {
		QAbstractButton::animateClick(msec);
	}
}

void BsToggleSwitch::wheelEvent(QWheelEvent *e)
{
	if (!m_output) {
		e->ignore();
		return;
	}

	QPoint numPixels = e->pixelDelta();
	QPoint numDegrees = e->angleDelta() / 8;
	qreal numSteps;

	if (!numPixels.isNull()) {
		numSteps = numPixels.y() / 15.0;
	} else if (!numDegrees.isNull()) {
		numSteps = numDegrees.y() / 15.0;
	} else {
		numSteps = 0;
	}

	if (numSteps > 0) {
		setChecked(true);
	} else if (numSteps < 0) {
		setChecked(false);
	}

	e->accept();
}
