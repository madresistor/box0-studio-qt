/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_WIDGET_TOGGLE_SWITCH_H
#define BOX0_STUDIO_WIDGET_TOGGLE_SWITCH_H

#include <QObject>
#include <QWidget>
#include <libbox0/libbox0.h>
#include <QPoint>
#include <QAbstractButton>
#include <QTimerEvent>
#include <QResizeEvent>
#include <QPaintEvent>
#include <QWheelEvent>
#include <QPixmap>
#include <QSvgRenderer>
#include <stdbool.h>

/* a portion of this class is from QLedIndicator (TODO: place project URL) */
class BsToggleSwitch : public QAbstractButton
{
	Q_OBJECT

	public:
		BsToggleSwitch(QWidget *widget = Q_NULLPTR);
		void setOutput(bool output);

	public Q_SLOTS:
		void animateClick(int msec = 100);
		void click();

	protected:
		void paintEvent (QPaintEvent *event);
		void resizeEvent(QResizeEvent *event);
		bool hitButton(const QPoint &pos) const;
		void wheelEvent(QWheelEvent *event);

	private:
		void outputPaintEvent(QPaintEvent *event);
		void inputPaintEvent(QPaintEvent *event);
		bool m_output;
		QColor onColor1, onColor2, offColor1, offColor2;

		static QSvgRenderer *pin_normal_upImage, *pin_normal_downImage;
		static QSvgRenderer *pin_disable_upImage, *pin_disable_downImage;
};

#endif
