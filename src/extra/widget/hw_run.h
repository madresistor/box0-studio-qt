/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_WIDGET_HW_RUN_H
#define BOX0_STUDIO_WIDGET_HW_RUN_H

#include <QObject>
#include <QAction>
#include <QTimer>

/*
 * The button assume the the design:
 *  User click the button to start/stop.
 *
 *  Application will capture the event and try to perform the event.
 *  If the application succeed, it will update the state.
 *
 *  The button will not automatically update it state.
 */

class BsHwRun : public QAction
{
	Q_OBJECT

	public:
		BsHwRun(QObject *parent);
		void setRunning(bool running = true);
		bool isRunning(void);

	Q_SIGNALS:
		void userWantToStart(void);
		void userWantToStop(void);

	public Q_SLOTS:
		void restart();

	private Q_SLOTS:
		void triggerToStateConv();
		void performRestart();

	private:
		void update();
		bool m_running;
		QTimer *restartTimer;
};

/* unit: milliseconds
 * This is the time for which the program wait for the user to enter more values.
 * After the interval has passed, the input will be considered as final */
#define BS_HW_RUN_DEAD_TIME 200

#endif
