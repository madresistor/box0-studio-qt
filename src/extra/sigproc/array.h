/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_SIGPROC_ARRAY_H
#define BOX0_STUDIO_SIGPROC_ARRAY_H

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

float bs_array_min(float *data, size_t count, size_t offset, size_t stride);
float bs_array_max(float *data, size_t count, size_t offset, size_t stride);
float bs_array_rms(float *data, size_t count, size_t offset, size_t stride);
float bs_array_p2p(float *data, size_t count, size_t offset, size_t stride);
float bs_array_avg(float *data, size_t count, size_t offset, size_t stride);

#ifdef __cplusplus
}
#endif

#endif
