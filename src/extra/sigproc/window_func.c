/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "window_func.h"
#include <math.h>

float bs_wfunc_raised_cosine(float ALPHA, float BETA, unsigned i, unsigned N)
{
	float tmp = (2.0 * M_PI * i) / (N - 1);
	return ALPHA - (BETA * cosf(tmp));
}

float bs_wfunc_triangular(unsigned i, unsigned N)
{
	/* http://edoc.mpg.de/395068 */
	float z = (2.0 * i) / N;
	return z > 1 ? (2.0 - z) : z;
}

float bs_wfunc_welch(unsigned i, unsigned N)
{
	/* http://edoc.mpg.de/395068 */
	float z = (2.0 * i) / N;
	return 1 - powf(z - 1, 2);
}

float bs_wfunc_blackman(unsigned i, unsigned N)
{
	const float a0 = 7938.0 / 18608.0;
	const float a1 = 9240.0 / 18608.0;
	const float a2 = 1430.0 / 18608.0;

	/* https://en.wikipedia.org/wiki/Window_function#Blackman_windows */
	float z = (2.0 * M_PI * i) / N;
	float res0 = a0;
	float res1 = a1 * cosf(z);
	float res2 = a2 * cosf(2 * z);
	return res0 - res1 + res2;
}

float bs_wfunc_flat_top(unsigned i, unsigned N)
{
	/* http://edoc.mpg.de/395068 */
	/* Stanford Research Systems (SRS) SR785 spectrum analyzer */
	float z = (2.0 * M_PI * i) / N;
	float res0 = 1.0;
	float res1 = 1.93 * cosf(z);
	float res2 = 1.29 * cosf(2 * z);
	float res3 = 0.388 * cosf(3 * z);
	float res4 = 0.028 * cosf(4 * z);
	return res0 - res1 + res2 - res3 + res4;
}
