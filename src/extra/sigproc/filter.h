/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_SIGPROC_FILTER_H
#define BOX0_STUDIO_SIGPROC_FILTER_H

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct bs_filter bs_filter;

bs_filter *bs_filter_init_lp(bs_filter *param, double sampling_freq,
		double cutoff_freq);

bs_filter *bs_filter_init_hp(bs_filter *param, double sampling_freq,
		double cutoff_freq);

bs_filter *bs_filter_init_bp(bs_filter *param, double sampling_freq,
		double cutoff_freq_lower, double cutoff_freq_upper);

bs_filter *bs_filter_init_br(bs_filter *param, double sampling_freq,
		double cutoff_freq_lower, double cutoff_freq_upper);

void bs_filter_apply(bs_filter *param, float *data,
		size_t size, size_t offset, size_t stride);

void bs_filter_state_reset(bs_filter *param);

void bs_filter_fini(bs_filter *param);

#ifdef __cplusplus
}
#endif

#endif
