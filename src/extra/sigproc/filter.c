/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "filter.h"
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "extra/iir/iir.h"

/* All filter of order above 2 can cause problem.
 * see: http://dsp.stackexchange.com/q/11163
 * see: http://www.bores.com/courses/intro/iir/5_struct.htm
 * So, be cautious before changing this value
 */
#define FILTER_ORDER 2

struct bs_filter {
	/* see lib/iir/iir.txt for explination
	 * for M,N,x,y,c,d,scaling_factor
	 */

	unsigned order;

	/* input related */
	size_t M;
	double *x /* 0 = newest; (M-1) = oldest */;
	double *c;
	double scaling_factor;

	/* output related */
	size_t N;
	double *y /* 0 = newest; (N-1) = oldest */;
	double *d;
};

/**
 * Prepare the object, if @a filter is NULL, allocate memory
 * @param filter optional, reuse
 * @param mult The number multiplied to M and N with order
 */
static bs_filter *prepare_obj(bs_filter *filter, unsigned mult)
{
	if (filter == NULL) {
		filter = malloc(sizeof(struct bs_filter));
		filter->order = FILTER_ORDER;
	} else {
		free(filter->x);
		free(filter->y);
		free(filter->d);
		free(filter->c);
	}

	filter->M = filter->order * mult;
	filter->N = filter->order * mult;
	filter->x = calloc(filter->M + 1, sizeof(double));
	filter->y = calloc(filter->N + 1, sizeof(double));

	return filter;
}

/**
 * Calculate fraction of pi from @a sampling_freq and @a cuttoff_freq
 * @param sampling_freq Sampling Frequency
 * @param cutoff_freq Cut Off Frequency
 * @return range [0, 1]
 */
static double fraction_of_pi(double sampling_freq, double cutoff_freq)
{
	return (2 * cutoff_freq) / sampling_freq;
}

/**
 * Create a duplicate copy of the int array as double array
 *   and free the original int array
 * @param arr int array
 * @param count number of element in @a arr
 * @return double array of length @a count with @a arr content in it.
 */
static double *array_int_to_double(int *arr, size_t count)
{
	size_t i;
	double *ret = malloc(sizeof(double) * count);

	for (i = 0; i < count; i++) {
		ret[i] = arr[i];
	}

	free(arr);
	return ret;
}

/**
 * Initalize a Low Pass filter
 * @param filter (if not NULL, reuse)
 * @param sampling_freq Sampling frequency of data
 * @param cutoff_freq Cut off Frequency
 * @return filter object
 */
bs_filter *bs_filter_init_lp(bs_filter *filter, double sampling_freq, double cutoff_freq)
{
	filter = prepare_obj(filter, 1);
	double fcf = fraction_of_pi(sampling_freq, cutoff_freq);
	filter->c = array_int_to_double(ccof_bwlp(filter->order), filter->M + 1);
	filter->d = dcof_bwlp(filter->order, fcf);
	filter->scaling_factor = sf_bwlp(filter->order, fcf);
	return filter;
}

/**
 * Initalize a High Pass filter
 * @param filter (if not NULL, reuse)
 * @param sampling_freq Sampling frequency of data
 * @param cutoff_freq Cut off Frequency
 * @return filter object
 */
bs_filter *bs_filter_init_hp(bs_filter *filter, double sampling_freq, double cutoff_freq)
{
	filter = prepare_obj(filter, 1);
	double fcf = fraction_of_pi(sampling_freq, cutoff_freq);
	filter->c = array_int_to_double(ccof_bwhp(filter->order), filter->M + 1);
	filter->d = dcof_bwhp(filter->order, fcf);
	filter->scaling_factor = sf_bwhp(filter->order, fcf);
	return filter;
}

/**
 * Initalize a Band Pass filter
 * @param filter (if not NULL, reuse)
 * @param sampling_freq Sampling frequency of data
 * @param cutoff_freq_lower Cut off Frequency (Lower)
 * @param cutoff_freq_upper Cut off Frequency (Upper)
 * @return filter object
 */
bs_filter *bs_filter_init_bp(bs_filter *filter, double sampling_freq,
		double cutoff_freq_lower, double cutoff_freq_upper)
{
	filter = prepare_obj(filter, 2);
	double f1f = fraction_of_pi(sampling_freq, cutoff_freq_lower);
	double f2f = fraction_of_pi(sampling_freq, cutoff_freq_upper);
	filter->c = array_int_to_double(ccof_bwbp(filter->order), filter->M + 1);
	filter->d = dcof_bwbp(filter->order, f1f, f2f);
	filter->scaling_factor = sf_bwbp(filter->order, f1f, f2f);
	return filter;
}

/**
 * Initalize a Band Reject filter
 * @param filter (if not NULL, reuse)
 * @param sampling_freq Sampling frequency of data
 * @param cutoff_freq_lower Cut off Frequency (Lower)
 * @param cutoff_freq_upper Cut off Frequency (Upper)
 * @return filter object
 */
bs_filter *bs_filter_init_br(bs_filter *filter, double sampling_freq,
		double cutoff_freq_lower, double cutoff_freq_upper)
{
	filter = prepare_obj(filter, 2);
	double f1f = fraction_of_pi(sampling_freq, cutoff_freq_lower);
	double f2f = fraction_of_pi(sampling_freq, cutoff_freq_upper);
	filter->c = ccof_bwbs(filter->order, f1f, f2f);
	filter->d = dcof_bwbs(filter->order, f1f, f2f);
	filter->scaling_factor = sf_bwbs(filter->order, f1f, f2f);
	return filter;
}

/**
 * Filter finished
 * @param filter Filter
 */
void bs_filter_fini(bs_filter *filter)
{
	free(filter->x);
	free(filter->y);
	free(filter->d);
	free(filter->c);
	free(filter);
}

/**
 * Apply filtering on data
 * @param filter Filter
 * @param data Data
 * @param size Number of values in @a data
 * @param offset the starting index of data
 * @param stride the number of samples to skip for next data
 */
void bs_filter_apply(bs_filter *filter, float *data,
		size_t size, size_t offset, size_t stride)
{
	size_t i, j;

	for (i = offset; i < size; i += stride) {
		double tmp = 0;

		/* shift and compute */
		for (j = filter->M; j > 0; j--) {
			filter->x[j] = filter->x[j - 1];

			/* c0*x[n] + c1*x[n-1] + ... + cM*x[n-M] */
			tmp += filter->c[j] * filter->x[j];
		}
		filter->x[0] = data[i];
		tmp += filter->x[0] * filter->c[0];
		tmp *= filter->scaling_factor;

		/* shift and compute */
		for (j = filter->N; j > 0; j--) {
			filter->y[j] = filter->y[j - 1];

			/* - (d1*y[n-1] + d2*y[n-2] + ... + dN*y[n-N]) */
			tmp -= filter->y[j] * filter->d[j];
		}
		data[i] = filter->y[0] = tmp;
	}
}

/**
 * Clear the state of the filter
 * State: the Cached input and output
 * @param filter Filter
 */
void bs_filter_state_reset(bs_filter *filter)
{
	memset(filter->x, 0, sizeof(double) * (filter->M + 1));
	memset(filter->y, 0, sizeof(double) * (filter->N + 1));
}
