/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "array.h"
#include <math.h>

/**
 * Find the maximum value of samples in @a data
 * @param data Data
 * @param count Number of samples in @a data
 * @param offset Number of samples to skip from start
 * @param stride Number of samples to skip for next sample
 * @return maximum value
 * @return NAN if no valid value found
 */
float bs_array_max(float *data, size_t count, size_t offset, size_t stride)
{
	float value = -INFINITY;

	for (size_t i = offset; i < count; i += stride) {
		float tmp = data[i];
		if (tmp > value) value  = tmp;
	}

	return isfinite(value) ? value : NAN;
}

/**
 * Find the minimum value of samples in @a data
 * @param data Data
 * @param count Number of samples in @a data
 * @param offset Number of samples to skip from start
 * @param stride Number of samples to skip for next sample
 * @return minimum value
 * @return NAN if no valid value found
 */
float bs_array_min(float *data, size_t count, size_t offset, size_t stride)
{
	float value = +INFINITY;

	for (size_t i = offset; i < count; i += stride) {
		float tmp = data[i];
		if (tmp < value) value  = tmp;
	}

	return isfinite(value) ? value : NAN;
}

/**
 * Find the Peak to Peak value of samples in the @a data
 * @note Peak to Peak = Maximum - Minimum
 * @param data Data
 * @param count Number of samples in @a data
 * @param offset Number of samples to skip from start
 * @param stride Number of samples to skip for next sample
 * @return value
 * @return NAN if no valid value found
 */
float bs_array_p2p(float *data, size_t count,
		size_t offset, size_t stride)
{
	float min = +INFINITY, max = -INFINITY;

	for (size_t i = offset; i < count; i += stride) {
		float tmp = data[i];
		if (tmp > max) max  = tmp;
		if (tmp < min) min = tmp;
	}

	return (isfinite(min) && isfinite(max) && (max > min)) ? (max - min) : NAN;
}

/**
 * Find the average value of samples in @a data
 * @param data Data
 * @param count Number of samples in @a data
 * @param offset Number of samples to skip from start
 * @param stride Number of samples to skip for next sample
 * @return value
 * @return NAN if no data found
 */
float bs_array_avg(float *data, size_t count, size_t offset, size_t stride)
{
	float value = 0;
	size_t len = 0;

	for (size_t i = offset; i < count; i += stride) {
		len += 1;
		value += data[i];
	}

	return (len > 0) ? (value / len) : NAN;
}

/**
 * Find the RMS value of samples in @a data
 * @param data Data
 * @param count Number of samples in @a data
 * @param offset Number of samples to skip from start
 * @param stride Number of samples to skip for next sample
 * @return RMS value
 * @return NAN if no data
 */
float bs_array_rms(float *data, size_t count, size_t offset, size_t stride)
{
	float value = 0;
	size_t len = 0;

	for (size_t i = offset; i < count; i += stride) {
		float tmp = data[i];
		len += 1;
		value += tmp * tmp;
	}

	return (len > 0) ? sqrt(value / len) : NAN;
}
