/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_SIGPROC_WINDOW_FUNC_H
#define BOX0_STUDIO_SIGPROC_WINDOW_FUNC_H

#ifdef __cplusplus
extern "C" {
#endif

/* generic */
float bs_wfunc_raised_cosine(float ALPHA, float BETA, unsigned i, unsigned N);

/* https://en.wikipedia.org/wiki/Window_function#Triangular_window */
float bs_wfunc_triangular(unsigned i, unsigned N);

/* https://en.wikipedia.org/wiki/Window_function#Welch_window */
float bs_wfunc_welch(unsigned i, unsigned N);

/* https://en.wikipedia.org/wiki/Window_function#Blackman_windows */
float bs_wfunc_blackman(unsigned i, unsigned N);

/* https://en.wikipedia.org/wiki/Window_function#Flat_top_window */
float bs_wfunc_flat_top(unsigned i, unsigned N);

/* http://en.wikipedia.org/wiki/Window_function#Hamming_window */
#define bs_wfunc_hamming(i, N) bs_wfunc_raised_cosine(0.53836, 0.46164, i, N)

/* https://en.wikipedia.org/wiki/Window_function#Hann_.28Hanning.29_window */
#define bs_wfunc_hanning(i, N) bs_wfunc_raised_cosine(0.5, 0.5, i, N)

#ifdef __cplusplus
}
#endif

#endif
