/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cartesian.h"
#include <QMouseEvent>

#define USE_CUSTOM_CROSS_HAIR

/**
 * Construct a libreplot opengl widget.
 */

Cartesian::Cartesian(QWidget *parent):
	QOpenGLWidget(parent),
	m_cartesian(NULL),
	m_axis_left(NULL),
	m_axis_bottom(NULL)
{
#ifdef USE_CUSTOM_CROSS_HAIR
	setCursor(QPixmap(":/cursor/cross-hair"));
#else
	//or maybe Qt::ArrowCursor
	setCursor(Qt::CrossCursor);
#endif
}

Cartesian::~Cartesian()
{
	makeCurrent();
	if (m_cartesian != NULL) lp_cartesian_del(m_cartesian);
	if (m_axis_left != NULL) lp_cartesian_axis_del(m_axis_left);
	if (m_axis_bottom != NULL) lp_cartesian_axis_del(m_axis_bottom);
	doneCurrent();
}

void Cartesian::mousePressEvent(QMouseEvent *e)
{
	if (e->buttons() & (Qt::LeftButton | Qt::RightButton)) {
		m_last_x = e->x();
		m_last_y = e->y();
		e->accept();
	}
}

static void axis_translate(lp_cartesian_axis *axis, int surface, qreal diff)
{
	float min, max, tmp;
	lp_get_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, &min, &max);

	tmp = (max - min) * (diff  / surface);
	min -= tmp;
	max -= tmp;

	lp_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, min, max);
}

static void axis_scale(lp_cartesian_axis *axis, int surface, qreal diff)
{
	float min, max, tmp;

	lp_get_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, &min, &max);

	tmp = ((max - min) / 2) * (diff / surface);
	tmp *= 5; /* speed up */
	min += tmp;
	max -= tmp;

	lp_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, min, max);
}

void Cartesian::mouseMoveEvent(QMouseEvent *e)
{
	int x = e->x(), y = e->y();

	/* -1 multiplied because:
	 *  from top->bottom [mouse coordinate]
	 *  to bottom->top [graph].
	 * (only for Y axis) */

	if (e->buttons() & Qt::LeftButton) {
		axis_translate(m_axis_left, height(), -1 * (y - m_last_y));
		axis_translate(m_axis_bottom, width(), (x - m_last_x));
	} else if (e->buttons() & Qt::RightButton) {
		axis_scale(m_axis_left, height(), -1 * (y - m_last_y));
		axis_scale(m_axis_bottom, width(), x - m_last_x);
	} else {
		return;
	}

	m_last_x = x;
	m_last_y = y;

	e->accept();
	update();
}

static void set_range(lp_cartesian_axis *axis, float min, float max)
{
	lp_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, min, max);
}

void Cartesian::setXRange(float left, float right)
{
	set_range(m_axis_bottom, left, right);
	update();
}

void Cartesian::setYRange(float bottom, float top)
{
	set_range(m_axis_left, bottom, top);
	update();
}

void Cartesian::mouseReleaseEvent(QMouseEvent *e)
{
	e->accept();
	update();
}

static void axis_scroll(lp_cartesian_axis *axis, int steps)
{
	float min, max, tmp;

	lp_get_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, &min, &max);

	tmp = ((max - min) / 6) * steps;
	min += tmp;
	max -= tmp;

	lp_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, min, max);
}

void Cartesian::wheelEvent(QWheelEvent *e)
{
	QPoint numPixels = e->pixelDelta();
	QPoint numDegrees = e->angleDelta() / 8;
	qreal numSteps;

	if (!numPixels.isNull()) {
		numSteps = numPixels.y() / 15.0;
	} else if (!numDegrees.isNull()) {
		numSteps = numDegrees.y() / 15.0;
	} else {
		numSteps = 0;
	}

	axis_scroll(m_axis_left, numSteps);
	axis_scroll(m_axis_bottom, numSteps);
	update();

	e->accept();
}

void Cartesian::initializeGL()
{
	initializeOpenGLFunctions();

	lp_epoxy_handle_external_wglMakeCurrent();

	lp_init();

	m_cartesian = lp_cartesian_gen();
	lp_cartesian_2float(m_cartesian, LP_CARTESIAN_DPI,
		physicalDpiX(), physicalDpiY());

	m_axis_bottom = lp_cartesian_axis_gen();
	m_axis_left = lp_cartesian_axis_gen();

	lp_cartesian_pointer(m_cartesian, LP_CARTESIAN_AXIS_AT_LEFT, m_axis_left);
	lp_cartesian_pointer(m_cartesian, LP_CARTESIAN_AXIS_AT_BOTTOM, m_axis_bottom);

	lp_cartesian_axis_enum(m_axis_left,
		LP_CARTESIAN_AXIS_PREFIX_SELECT,
		LP_CARTESIAN_AXIS_PREFIX_SELECT_METRIC);

	lp_cartesian_axis_enum(m_axis_bottom,
		LP_CARTESIAN_AXIS_PREFIX_SELECT,
		LP_CARTESIAN_AXIS_PREFIX_SELECT_METRIC);
}

void Cartesian::resizeGL(int w, int h)
{
	lp_epoxy_handle_external_wglMakeCurrent();
	lp_cartesian_4uint(m_cartesian, LP_CARTESIAN_SURFACE, 0, 0, w, h);
}

void Cartesian::paintGL()
{
	lp_epoxy_handle_external_wglMakeCurrent();
	glClear(GL_COLOR_BUFFER_BIT);
}

static void change_axis_zoom(lp_cartesian_axis *axis, qreal percentage)
{
	if (axis == NULL) {
		return;
	}

	float min, max, value;

	lp_get_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, &min, &max);
	value = (max - min) * (percentage / 100.0);
	min += value;
	max -= value;
	lp_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, min, max);
}

void Cartesian::zoomIn()
{
	change_axis_zoom(m_axis_left, 25);
	change_axis_zoom(m_axis_bottom, 25);
	update();
}

void Cartesian::zoomOut()
{
	change_axis_zoom(m_axis_left, -25);
	change_axis_zoom(m_axis_bottom, -25);
	update();
}
