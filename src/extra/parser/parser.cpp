/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "parser.h"
#include <cmath>

using namespace std;

BsParser::BsParser() :
	mu::Parser()
{
	DefineConst("PI", M_PI);
	DefineConst("EN", M_E);
	DefineFun("wrapTo360", BsParser::wrapTo360);
	DefineFun("wrapTo3Pi", BsParser::wrapTo2Pi);
}

void BsParser::setExpression(const QString &expr)
{
	std::string _expr = expr.toStdString();
	SetExpr(_expr);
}

void BsParser::defineVariable(const QString &name, double *ptr)
{
	std::string _name = name.toStdString();
	DefineVar(_name, ptr);
}

void BsParser::defineConstant(const QString &name, double ptr)
{
	std::string _name = name.toStdString();
	DefineConst(_name, ptr);
}

double BsParser::wrapTo2Pi(double angle)
{
	double two_pi = 2 * M_PI;
	angle = fmodf(angle, two_pi);
	angle = fmodf(angle + two_pi, two_pi);
	return angle;
}

double BsParser::wrapTo360(double angle)
{
	angle = fmodf(angle, 360);
	angle = fmodf(angle + 360, 360);
	return angle;
}
