/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "exception.h"
#include <QString>

BsParserException::BsParserException(mu::Parser::exception_type &e, QWidget *parent) :
	QMessageBox(parent)
{
	setIcon(QMessageBox::Warning);
	setWindowTitle("Calculation failed!");

	setStandardButtons(QMessageBox::Ok);
	setEscapeButton(QMessageBox::Ok);

	setException(e);
}

void BsParserException::setException(mu::Parser::exception_type &e)
{
	QString msg = QString::fromStdString(e.GetMsg());
	QString expr = QString::fromStdString(e.GetExpr());
	QString token = QString::fromStdString(e.GetToken());
	QString pos = QString::number(e.GetPos());
	QString code = QString::number(e.GetCode());
	QString details = QString(
		"MuParser (the equation solving library) has returned error while calculating.\n"
		"Message: %1\n"
		"Formula: %2\n"
		"Token: %3\n"
		"Position: %4\n"
		"Errc: %5")
	.arg(msg)
	.arg(expr)
	.arg(token)
	.arg(pos)
	.arg(code);

	setText(msg);
	setDetailedText(details);
}
