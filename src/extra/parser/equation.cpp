/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "equation.h"

BsParserEquation::BsParserEquation(QWidget *parent) :
	QLineEdit(parent)
{
}

void BsParserEquation::insertAsVariable(const QString &value)
{
	insert(QString("%1 ").arg(value));
	setFocus();
}

void BsParserEquation::insertAsConstant(const QString &value)
{
	insert(QString("%1 ").arg(value));
	setFocus();
}

void BsParserEquation::insertAsFunction(const QString &value)
{
	insert(QString("%1()").arg(value));
	setFocus();

	/* move backward one position to accept arguments */
	int pos = cursorPosition();
	setCursorPosition(pos - 1);
}

void BsParserEquation::insertAsOperator(const QString &value)
{
	insert(QString("%1 ").arg(value));
	setFocus();
}
