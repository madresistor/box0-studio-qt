/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_PARSER_EQUATION_H
#define BOX0_STUDIO_PARSER_EQUATION_H

#include <QObject>
#include <QWidget>
#include <QLineEdit>
#include <QString>

class BsParserEquation : public QLineEdit
{
	Q_OBJECT

	public:
		BsParserEquation(QWidget *parent);

	public Q_SLOTS:
		void insertAsFunction(const QString &);
		void insertAsConstant(const QString &);
		void insertAsVariable(const QString &);
		void insertAsOperator(const QString &);
};

#endif
