/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_AIN_SNAPSHOT_SAMPLER_H
#define BOX0_STUDIO_AIN_SNAPSHOT_SAMPLER_H

#include <QWidget>
#include <QObject>
#include <QThread>
#include <libbox0/libbox0.h>
#include <QTimer>

/**
 * AIN snapshot mode sampler.
 * It can be used to sample data form the specified module
 */
class BsAinSnapshotSampler : public QThread
{
	Q_OBJECT

	public:
		BsAinSnapshotSampler(QWidget *parent);
		void start(b0_ain *ain, unsigned int bitsize, unsigned long speed);
		void requestInterruption();
		uint updateInterval(void);

		void dispose(float *data);

	Q_SIGNALS:
		/**
		 * Output data from the sampler
		 * @param speed Speed at which data was capture
		 * @param data pointer to data
		 * @param size number of samples in @a data
		 * @note receiver need to delete @a delete
		 */
		void output(unsigned long speed, unsigned int bitsize, float *data,
						uint size);

	private Q_SLOTS:
		void sample();

	protected:
		void run();
		b0_ain *m_module;
		unsigned int m_bitsize;
		unsigned long m_speed;
};

#endif
