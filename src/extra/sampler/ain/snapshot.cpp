/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "snapshot.h"
#include "extra/box0/result_code.h"
#include <QTimer>

BsAinSnapshotSampler::BsAinSnapshotSampler(QWidget *parent) :
	QThread(parent)
{
}

/**
 * The number of milliseconds after which the data is fetched.
 * @return number of milliseconds
 */
uint BsAinSnapshotSampler::updateInterval(void)
{
	/* unit: milliseconds */
	return 200;
}

/**
 * Start the sampler.
 * @param ain AIN module
 */
void BsAinSnapshotSampler::start(b0_ain *ain, unsigned int bitsize,
					unsigned long speed)
{
	m_module = ain;
	m_bitsize = bitsize;
	m_speed = speed;
	QThread::start();
}

/**
 * Request the thread to exit.
 */
void BsAinSnapshotSampler::requestInterruption()
{
	exit(0);
	QThread::requestInterruption();
}

/**
 * Sample data from module and pass on the data to "output" signal.
 * It is the duty of the receiver to free the memory (of the values).
 * This method will query the bitsize and speed.
 *  and accordingly calculate the maximum number of samples to fetch.
 * @note the array allocated by this method should passed to
 *  BsAinSnapshotSampler::dispose() when done by the receiver of the data
 */
void BsAinSnapshotSampler::sample()
{
	/* max number of samples */
	size_t byte_per_sample = (m_bitsize + 7) / 8;
	size_t size = m_module->buffer_size / byte_per_sample;

	float *data = new float [size];

	b0_result_code r = b0_ain_snapshot_start_float(m_module, data, size);
	if (B0_ERR_RC(r)) {
		BsResultCode::handleInternal(r, "b0_ain_snapshot_start_float failed");
		return;
	}

	Q_EMIT output(m_speed, m_bitsize, data, size);
}

void BsAinSnapshotSampler::run()
{
	QTimer timer;
	/* NOTE: directly calling the thread object method sample() */
	connect(&timer, SIGNAL(timeout()), this, SLOT(sample()), Qt::DirectConnection);
	timer.start(updateInterval());

	exec();
}

/**
 * Dispose the memory allocated by sampler
 * @param data Data
 */
void BsAinSnapshotSampler::dispose(float *data)
{
	/* FIXME: memory allocation is an expensive operation, reuse memory */
	delete[] data;
}
