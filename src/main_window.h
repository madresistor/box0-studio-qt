/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_MAIN_WINDOW_H
#define BOX0_STUDIO_MAIN_WINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include "widgets/widget/widget.h"
#include <libbox0/libbox0.h>
#include <QLabel>
#include <QList>

#include "ui_main_window.h"

class BsMainWindow : public QMainWindow
{
	Q_OBJECT

	public:
		BsMainWindow(QWidget *parent = Q_NULLPTR);
		~BsMainWindow();

	public Q_SLOTS:
		void addWidget(BsWidget *widget);

	private Q_SLOTS:
		void removeWidget(int index);
		void findDevice();
		bool pingDevice();
		void setDeviceStatus(const QString &status, const QColor &color);
		void showAsWindow(BsWidget*, bool make_full_window);

	protected:
		void closeEvent(QCloseEvent *event);

	private:
		Ui::BsMainWindow ui;
		b0_device *device = NULL;
		QLabel *device_status;
		QList<BsWidget *> full_windows;
};

#endif
