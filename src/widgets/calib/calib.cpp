/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "calib.h"
#include <QDebug>
#include "extra/box0/result_code.h"
#include "extra/box0/box0.h"
#include "extra/sigproc/array.h"
#include <libbox0/misc/box0-v5/box0-v5.h>
#include <libbox0/misc/box0-v5/calib.h>
#include <QMessageBox>
#include <QThread>
#include <QDebug>

/*
 * Device caliberation
 * ===================
 *
 * This caliberation program currently only support box0-v5.
 * Caliberation is performed by:
 * 1. Reading Analog In data (Ground all pins)
 * 2. Reading Analog Out data using Analog In
 * 3. Calculating offset
 * 4. Store to device
 */

BsCalib::BsCalib(QWidget *parent) :
	BsWidget(parent)
{
	ui.setupUi(this);
	setupTabDetach();

	connect(ui.intro_next, SIGNAL(clicked()), SLOT(introNextClicked()));
	connect(ui.ain_input_next, SIGNAL(clicked()), SLOT(ainInputNextClicked()));
	connect(ui.aout_input_next, SIGNAL(clicked()), SLOT(aoutInputNextClicked()));
	connect(ui.complete_save, SIGNAL(clicked()), SLOT(completeSaveClicked()));

	selectIntoTab();
}

BsCalib::~BsCalib()
{
	if (ain_calib.offset != nullptr) {
		delete[] ain_calib.offset;
	}

	if (aout_calib.offset != nullptr) {
		delete[] aout_calib.offset;
	}
}

void BsCalib::selectIntoTab()
{
	ui.tab->setTabEnabled(0, true);
	ui.tab->setCurrentIndex(0);

	for (int i = 1; i < ui.tab->count(); i++) {
		ui.tab->setTabEnabled(i, false);
	}
}

void BsCalib::selectNextTab()
{
	int current_index = ui.tab->currentIndex();
	int next_index = current_index + 1;

	ui.tab->setTabEnabled(next_index, true);
	ui.tab->setCurrentIndex(next_index);
	ui.tab->setTabEnabled(current_index, false);
}

static void assign_data(QTableWidget *w, uint8_t **ch_labels,
				qreal *values, size_t count)
{
	w->clearContents();
	w->setRowCount(count);

	for (size_t i = 0; i < count; i++) {
		QString ch_name;

		if (ch_labels[i] != NULL) {
			ch_name = utf8_to_qstring(ch_labels[i]);
		} else {
			ch_name = QString("CH%1").arg(i);
		}

		/* Channel label */
		QTableWidgetItem *ch_item = new QTableWidgetItem(ch_name);
		ch_item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		w->setItem(i, 0, ch_item);

		/* Offset value */
		QTableWidgetItem *offset_item = new QTableWidgetItem();
		offset_item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled |
								Qt::ItemIsEditable);
		offset_item->setData(Qt::DisplayRole, values[i] * 1000);
		w->setItem(i, 1, offset_item);
	}
}

void BsCalib::selectCompleteTab()
{
	int current_index = ui.tab->currentIndex();
	int next_index = ui.tab->count() - 1;

	ui.tab->setTabEnabled(next_index, true);
	ui.tab->setCurrentIndex(next_index);
	ui.tab->setTabEnabled(current_index, false);

	assign_data(ui.complete_ain_table, ain_module->label.chan,
			ain_calib.offset, ain_calib.count);
	assign_data(ui.complete_aout_table, aout_module->label.chan,
			aout_calib.offset, aout_calib.count);
}

void BsCalib::introNextClicked()
{
	selectNextTab();
}

void BsCalib::ainInputNextClicked()
{
	selectNextTab();
	startAinCalib();
}

void BsCalib::startAinCalib()
{
	b0_result_code r;

	/* Prepare module for snapshot */
	r = b0_ain_snapshot_prepare(ain_module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AIN0"));
		rc.setMessage(tr("Snapshot prepare failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* Prepare channel sequence */
	size_t CHAN_COUNT = ain_module->chan_count;
	uint chan_seq[CHAN_COUNT];
	for (size_t i = 0; i < CHAN_COUNT; i++) {
		chan_seq[i] = i;
	}

	/* apply channel sequence to module */
	r = b0_ain_chan_seq_set(ain_module, chan_seq, CHAN_COUNT);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AIN0"));
		rc.setMessage(tr("Channel sequence set failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* Get current bitsize */
	unsigned int bitsize;
	r = b0_ain_bitsize_speed_get(ain_module, &bitsize, NULL);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AIN0"));
		rc.setMessage(tr("Bitsize speed set failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* Read data from module */
	unsigned int bytes_per_sample = (bitsize + 7) / 8;
	size_t COUNT = ain_module->buffer_size / bytes_per_sample;
	float data[COUNT];
	r = b0_ain_snapshot_start_float(ain_module, data, COUNT);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AIN0"));
		rc.setMessage(tr("Snapshot start failed"));
		rc.exec();
		deleteLater();
		return;
	}

	if (ain_calib.offset != nullptr) {
		delete[] ain_calib.offset;
	}

	ain_calib.count = CHAN_COUNT;
	ain_calib.offset = new qreal[ain_calib.count];

	for (size_t i = 0; i < ain_calib.count; i++) {
		ain_calib.offset[i] = bs_array_avg(data, COUNT, i, CHAN_COUNT);
	}

	selectNextTab();
}

void BsCalib::aoutInputNextClicked()
{
	selectNextTab();
	startAoutCalib();
}

void BsCalib::startAoutCalib()
{
	b0_result_code r;

	/* Prepare aout module for snapshot mode */
	r = b0_aout_snapshot_prepare(aout_module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AOUT0"));
		rc.setMessage(tr("Snapshot prepare failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* Prepare ain module for snapshot mode */
	r = b0_ain_snapshot_prepare(ain_module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AIN0"));
		rc.setMessage(tr("Snapshot prepare failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* Prepare channel sequence */
	size_t CHAN_COUNT = aout_module->chan_count;
	uint chan_seq[CHAN_COUNT];
	for (size_t i = 0; i < CHAN_COUNT; i++) {
		chan_seq[i] = i;
	}

	/* Set channel sequence to ain module */
	r = b0_ain_chan_seq_set(ain_module, chan_seq, CHAN_COUNT);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AIN0"));
		rc.setMessage(tr("Channel sequence set failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* Set channel sequence to aout module */
	r = b0_aout_chan_seq_set(aout_module, chan_seq, CHAN_COUNT);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AOUT0"));
		rc.setMessage(tr("Channel sequence set failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* Start aout module */
	float aout_data[CHAN_COUNT];
	for (size_t i = 0; i < CHAN_COUNT; i++) {
		aout_data[i] = 0;
	}

	r = b0_aout_snapshot_start_float(aout_module, aout_data, CHAN_COUNT);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AOUT0"));
		rc.setMessage(tr("Start failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* Get current bitsize */
	unsigned int bitsize;
	r = b0_ain_bitsize_speed_get(ain_module, &bitsize, NULL);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AIN0"));
		rc.setMessage(tr("Bitsize speed set failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* Wait for the AOUT module to become stable */
	QThread::msleep(50);

	/* Start ain module */
	unsigned int bytes_per_sample = (bitsize + 7) / 8;
	size_t COUNT = ain_module->buffer_size / bytes_per_sample;
	float ain_data[COUNT];
	r = b0_ain_snapshot_start_float(ain_module, ain_data, COUNT);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AIN0"));
		rc.setMessage(tr("Snapshot start failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* Stop aout module */
	r = b0_aout_snapshot_stop(aout_module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AOUT0"));
		rc.setMessage(tr("Snapshot stop failed"));
		rc.exec();
		deleteLater();
		return;
	}

	if (aout_calib.offset != nullptr) {
		delete[] aout_calib.offset;
	}

	aout_calib.count = CHAN_COUNT;
	aout_calib.offset = new qreal[aout_calib.count];

	for (size_t i = 0; i < aout_calib.count; i++) {
		aout_calib.offset[i] = bs_array_avg(ain_data, COUNT, i, CHAN_COUNT);

		/* Use AIN calibration */
		aout_calib.offset[i] -= ain_calib.offset[i];
	}

	/* complete */
	selectCompleteTab();
}

static void table_to_calib_values_array(QTableWidget *w,
				b0v5_calib_value *values, size_t count)
{
	for (size_t i = 0; i < count; i++) {
		values[i].gain = 1;
		values[i].offset = w->item(i, 1)->data(Qt::DisplayRole).toFloat() / 1000.0;
	}
}

void BsCalib::completeSaveClicked()
{
	size_t ain0_count = ui.complete_ain_table->rowCount();
	b0v5_calib_value ain0_values[ain0_count];
	table_to_calib_values_array(ui.complete_ain_table, ain0_values, ain0_count);

	size_t aout0_count = ui.complete_aout_table->rowCount();
	b0v5_calib_value aout0_values[aout0_count];
	table_to_calib_values_array(ui.complete_aout_table, aout0_values, aout0_count);

	b0v5_calib calib = {
		/* .ain0 = */ {
			/* .values = */ ain0_values,
			/* .count = */ ain0_count
		},

		/* .aout0 = */ {
			/* .values = */ aout0_values,
			/* .count = */ aout0_count
		}
	};

	/* Write to device */
	b0_result_code r = b0v5_calib_write(device, &calib);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Save"));
		rc.setMessage(tr("Write to device failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* yea! */
	QMessageBox::information(this, tr("Save"), tr("Successfully saved to device"));
}

void BsCalib::setDevice(b0_device *dev)
{
	b0_result_code r;

	selectIntoTab();

	if (dev == NULL) {
		/* TODO: stop any ongoing process */
		device = NULL;
		ain_module = NULL;
		aout_module = NULL;
		return;
	}

	r = b0v5_valid_test(dev);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Unsupported device"));
		rc.setMessage(tr("Not a supported (expecting box0-v5)"));
		rc.exec();
		deleteLater();
		return;
	}

	/* search for AIN module to bind to */
	r = b0_ain_open(dev, &ain_module, 0);
	if (B0_ERR_RC(r)) {
		ain_module = NULL;
		BsResultCode rc(r);
		rc.setTitle(tr("AIN0"));
		rc.setMessage(tr("Open failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* search for AOUT module to bind to */
	r = b0_aout_open(dev, &aout_module, 0);
	if (B0_ERR_RC(r)) {
		BsResultCode::handleInternal(b0_ain_close(ain_module),
			"Unable to close AIN module");
		ain_module = NULL;
		aout_module = NULL;

		BsResultCode rc(r);
		rc.setTitle(tr("AOUT0"));
		rc.setMessage(tr("Open failed"));
		rc.exec();
		deleteLater();
		return;
	}

	device = dev;
}

/**
 * Before accepting close event, perform:
 *  - if running, stop
 *  - if module is opened, close it
 * @param event Close event
 */
void BsCalib::closeEvent(QCloseEvent *event)
{
	if (false /* FIXME: running */) {
		/* FIXME: STOP */
	}

	if (ain_module != NULL) {
		/* close the module */
		BsResultCode::handleInternal(b0_ain_close(ain_module),
			"unable to close AIN module");
		ain_module = NULL;
	}

	if (aout_module != NULL) {
		/* close the module */
		BsResultCode::handleInternal(b0_aout_close(aout_module),
			"unable to close AOUT module");
		aout_module = NULL;
	}

	BsWidget::closeEvent(event);
}
