/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_CALIB_H
#define BOX0_STUDIO_CALIB_H

#include <QWidget>
#include <QObject>
#include <QWidget>
#include "widgets/widget/widget.h"
#include <libbox0/libbox0.h>

#include "ui_calib.h"

class BsCalib : public BsWidget
{
	Q_OBJECT

	public:
		BsCalib(QWidget *parent = Q_NULLPTR);
		~BsCalib();

		void setDevice(b0_device *dev);

	protected:
		void closeEvent(QCloseEvent *);

	private Q_SLOTS:
		void introNextClicked();
		void ainInputNextClicked();
		void aoutInputNextClicked();
		void completeSaveClicked();

	private:
		Ui::BsCalib ui;

		void selectIntoTab();
		void selectNextTab();
		void selectCompleteTab();
		void startAinCalib();
		void startAoutCalib();

		b0_device *device = NULL;
		b0_ain *ain_module = NULL;
		b0_aout *aout_module = NULL;

		struct {
			qreal *offset = nullptr;
			size_t count = 0;
		} ain_calib, aout_calib;
};

#endif
