/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_I2C_BASIC_H
#define BOX0_STUDIO_I2C_BASIC_H

#include <QObject>
#include <QWidget>
#include <libbox0/libbox0.h>

#include "ui_i2c_basic.h"

class BsI2cBasic : public QWidget
{
	Q_OBJECT

	public:
		BsI2cBasic(QWidget *parent = 0);
		void setModule(b0_i2c *mod);

	private Q_SLOTS:
		void countChanged(int);
		void operScan();
		void operDetect();
		void operRead();
		void operWrite();
		void operSave();
		void setUiEnabled(bool enable);

	private:
		Ui::BsI2cBasic ui;
		b0_i2c *m_module;
};

#endif
