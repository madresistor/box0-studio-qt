/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "basic.h"
#include "extra/box0/box0.h"
#include <QApplication>
#include <QDateTime>
#include <QFile>
#include <QTextStream>
#include <QFileInfo>
#include <QStandardPaths>
#include <QFileDialog>
#include <QMessageBox>

/**
 * Convert character @a v to hexified string
 * @param v Value
 * @return hexified
 */
static QString hexify(char v)
{
	const char *hex = "0123456789ABCDEF";
	int h = (v >> 4) & 0xF;
	int l = v & 0xF;

	return QString("%1%2").arg(hex[h]).arg(hex[l]);
}

/**
 * Split the string by an specific number of characters
 * @param data String to split
 * @param chars Number of character to group together
 * @return hexified
 */
static QString hexify(const QByteArray &arr)
{
	QStringList list;

	for (int i = 0; i < arr.size(); i++) {
		list << arr.mid(i, 1).toHex();
	}

	return list.join(" ").toUpper();
}

/**
 * Convert libbox0 result code to a user showable string
 * @param rc Result code
 * @return details
 */
static QString rc_details(b0_result_code rc)
{
	QString name = utf8_to_qstring(b0_result_name(rc));
	QString explain = utf8_to_qstring(b0_result_explain(rc));
	return QString("%1 - %2").arg(name).arg(explain);
}

BsI2cBasic::BsI2cBasic(QWidget *parent) :
	QWidget(parent),
	m_module(NULL)
{
	ui.setupUi(this);

	connect(ui.scan, SIGNAL(clicked()), SLOT(operScan()));
	connect(ui.detect, SIGNAL(clicked()), SLOT(operDetect()));
	connect(ui.read, SIGNAL(clicked()), SLOT(operRead()));
	connect(ui.write, SIGNAL(clicked()), SLOT(operWrite()));
	connect(ui.logSave, SIGNAL(clicked()), SLOT(operSave()));

	connect(ui.count, SIGNAL(valueChanged(int)),SLOT(countChanged(int)));
	countChanged(1);
}

void BsI2cBasic::setUiEnabled(bool enable)
{
	ui.scan->setEnabled(enable);
	ui.detect->setEnabled(enable);
	ui.read->setEnabled(enable);
	ui.write->setEnabled(enable);
	ui.addr->setEnabled(enable);
	ui.version->setEnabled(enable);
	ui.count->setEnabled(enable);
	ui.data->setEnabled(enable);
}

/**
 * Scan the entire bus
 */
void BsI2cBasic::operScan()
{
	bool slave_present;
	b0_i2c_version version = (b0_i2c_version) ui.version->currentData().toInt();
	uint count = 0;

	setUiEnabled(false);

	QString dateTime = QDateTime::currentDateTime().toString();
	ui.log->append(QString(tr("<div>%1: Full bus scan started...</div>")).arg(dateTime));

	for (uint8_t sa = 0b0001000; sa < 0b1111000; sa++) {
		const b0_i2c_sugar_arg arg = {
			.addr = sa,
			.version = version
		};

		b0_result_code rc = b0_i2c_master_slave_detect(m_module, &arg, &slave_present);
		if (B0_ERR_RC(rc)) {
			ui.log->append(QString(tr("<div style='color:red'>%1: Failed (%2)</div>")).arg(hexify(sa)).arg(rc_details(rc)));
		} else if (slave_present) {
			ui.log->append(QString(tr("<div style='color:green'>%1: Found</div>")).arg(hexify(sa)));
			count++;
		}

		/* FIXME: if user close the intrument while in progress, problem can occur */
		qApp->processEvents();
	}

	ui.log->append(QString("<div>Found %1 slave(s)</div>").arg(count));
	ui.log->append("<br/>");

	setUiEnabled(true);
}

/**
 * Check if the specific slave is on bus
 */
void BsI2cBasic::operDetect()
{
	bool slave_present;
	uint8_t addr = (uint8_t) ui.addr->value();
	b0_i2c_version version = (b0_i2c_version) ui.version->currentData().toInt();
	QString dateTime = QDateTime::currentDateTime().toString();

	setUiEnabled(false);

	const b0_i2c_sugar_arg arg = {
		.addr = addr,
		.version = version
	};

	b0_result_code rc = b0_i2c_master_slave_detect(m_module, &arg, &slave_present);
	if (B0_ERR_RC(rc)) {
		ui.log->append(QString(tr("<div style='color:red'>%1: %2 - Detection failed (%2)</div>"))
				.arg(dateTime).arg(hexify(addr)).arg(rc_details(rc)));
	} else if (slave_present) {
		ui.log->append(QString(tr("<div style='color:green'>%1: %2 - Detected</div>"))
				.arg(dateTime).arg(hexify(addr)));
	} else {
		ui.log->append(QString(tr("<div style='color:blue'>%1: %2 - Not detected</div>"))
				.arg(dateTime).arg(hexify(addr)));
	}

	ui.log->append("<br/>");

	setUiEnabled(true);
}

/**
 * Write to slave
 */
void BsI2cBasic::operWrite()
{
	setUiEnabled(false);

	QString dateTime = QDateTime::currentDateTime().toString();
	uint8_t addr = (uint8_t) ui.addr->value();
	b0_i2c_version version = (b0_i2c_version) ui.version->currentData().toInt();
	size_t count = (size_t) ui.count->value();
	QByteArray data = QByteArray::fromHex(ui.data->text().toLatin1());
	data.resize(count);
	const uint8_t *data_ptr = (const uint8_t *) data.constData();
	QString data_hex = hexify(data);

	const b0_i2c_sugar_arg arg = {
		.addr = addr,
		.version = version
	};

	b0_result_code rc = b0_i2c_master_write(m_module, &arg, data_ptr, count);
	if (B0_ERR_RC(rc)) {
		ui.log->append(QString(tr("<div style='color:red'>%1: %2 - Write failed %3 (%4)</div>"))
			.arg(dateTime).arg(hexify(addr)).arg(data_hex).arg(rc_details(rc)));
	} else {
		ui.log->append(QString(tr("<div style='color:green'>%1: %2 - Written (%3)</div>"))
			.arg(dateTime).arg(hexify(addr)).arg(data_hex));
	}

	ui.log->append("<br/>");

	setUiEnabled(true);
}

/**
 * Read from slave
 */
void BsI2cBasic::operRead()
{
	setUiEnabled(false);

	QString dateTime = QDateTime::currentDateTime().toString();
	uint8_t addr = (uint8_t) ui.addr->value();
	b0_i2c_version version = (b0_i2c_version) ui.version->currentData().toInt();
	size_t count = (size_t) ui.count->value();

	QByteArray data(count, '\0');
	uint8_t *data_ptr = (uint8_t *) data.data();

	const b0_i2c_sugar_arg arg = {
		.addr = addr,
		.version = version
	};

	b0_result_code rc = b0_i2c_master_read(m_module, &arg, data_ptr, count);
	if (B0_ERR_RC(rc)) {
		ui.log->append(QString(tr("<div style='color:red'>%1: %2 - Read failed (%3)</div>"))
			.arg(dateTime).arg(hexify(addr)).arg(rc_details(rc)));
	} else {
		QString data_hex = hexify(data);
		ui.log->append(QString(tr("<div style='color:green'>%1: %2 - Readed (%3)</div>"))
			.arg(dateTime).arg(hexify(addr)).arg(data_hex));
		ui.data->setText(data_hex);
	}

	ui.log->append("<br/>");

	setUiEnabled(true);
}

/**
 * Save log to disk
 */
void BsI2cBasic::operSave()
{
	QString docDir = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
	QString path = QString("%1/%2").arg(docDir).arg("i2c-log.html");
	QString filename = QFileDialog::getSaveFileName(this, tr("Save to file"),
		path, "Hyper Text Markup Language (*.html, *.htm)");

	if (filename.isEmpty()) {
		return;
	}

	/* Ref: http://stackoverflow.com/a/9822246/1500988 */
	if (QFileInfo(filename).suffix().isEmpty()) {
		filename.append(".html");
	}

	QFile file(filename);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
		QMessageBox::warning(this, tr("Writing failed"), tr("Unable to open file."));
		return;
	}

	QTextStream out(&file);
	out << ui.log->document()->toHtml();
	file.close();
}

/**
 * Called when count input value changes
 * @param count New value of count
 */
void BsI2cBasic::countChanged(int count)
{
	/* Backup data */
	QByteArray data = QByteArray::fromHex(ui.data->text().toLatin1());

	/* Memory effeciency if not a big concern */
	QString bytes = "HH";

	/* "HH" */
	QString inputMask = bytes;

	if (count > 1) {
		/* "HH HH HH ..... HH" */
		QString bytes_with_space = bytes + " ";
		inputMask.prepend(bytes_with_space.repeated(count - 1));
	}

	ui.data->setInputMask(">" + inputMask);

	data.resize(count);
	ui.data->setText(hexify(data));
}

QHash<b0_i2c_version, QString> VERSIONS = {
	{B0_I2C_VERSION_SM, "Standard Mode"},
	{B0_I2C_VERSION_FM, "Fast Mode"},
	{B0_I2C_VERSION_HS, "High speed"},
	{B0_I2C_VERSION_HS, "High speed (cleanup 1)"},
	{B0_I2C_VERSION_FMPLUS, "Fast Mode Plus"},
	{B0_I2C_VERSION_UFM, "Ultra Fast Mode"},
	{B0_I2C_VERSION_VER5, "Version 5"},
	{B0_I2C_VERSION_VER6, "Version 6"},
};

void BsI2cBasic::setModule(b0_i2c *mod)
{
	m_module = mod;
	ui.version->clear();

	for (size_t i = 0; i < mod->version.count; i++) {
		b0_i2c_version v = mod->version.values[i];
		ui.version->addItem(VERSIONS.value(v), v);
	}
}
