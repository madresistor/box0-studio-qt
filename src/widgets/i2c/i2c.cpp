/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "i2c.h"
#include <QMessageBox>
#include <QApplication>
#include <QDesktopWidget>
#include "extra/box0/result_code.h"

BsI2c::BsI2c(QWidget *parent) :
	BsInstrument(B0_I2C, parent)
{
	ui.setupUi(this);
}

void BsI2c::loadModule(b0_device *dev, int index)
{
	b0_result_code r;

	/* search for module to bind to */
	r = b0_i2c_open(dev, &module, index);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("I2C"));
		rc.setMessage(tr("Open failed"));
		rc.exec();
		deleteLater();
		return;
	}

	r = b0_i2c_master_prepare(module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("I2C"));
		rc.setMessage(tr("Prepare for master mode failed"));
		rc.exec();
		deleteLater();
		return;
	}

	ui.basic->setModule(module);
	showModuleInTitle(&module->header);
}

void BsI2c::closeEvent(QCloseEvent *event)
{
	if (module != NULL) {
		/* close the module */
		BsResultCode::handleInternal(b0_i2c_close(module),
			"unable to close I2C module");

		module = NULL;
	}

	BsInstrument::closeEvent(event);
}
