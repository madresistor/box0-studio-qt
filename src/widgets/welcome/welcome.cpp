/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "welcome.h"

#include <QApplication>
#include <QDesktopWidget>

/* instruments */
#include "widgets/instrument/instrument.h"
#include "widgets/osc/osc.h"
#include "widgets/func_gen/func_gen.h"
#include "widgets/dio/dio.h"
#include "widgets/voltmeter/voltmeter.h"
#include "widgets/pwm/pwm.h"
#include "widgets/power_supply/power_supply.h"
#include "widgets/device_info/device_info.h"
#include "widgets/i2c/i2c.h"
#include "widgets/spi/spi.h"
#include "widgets/calib/calib.h"

#include "extra/box0/result_code.h"

#include "about.h"

/**
 * List of widget type that currenly we know
 */
enum {
	OSC,
	FUNC_GEN,
	PWM,
	DIO,
	VOLTMETER,
	POWER_SUPPLY,
	I2C,
	SPI,
	DEV_INFO,
	CALIB
};

BsWelcome::BsWelcome(QWidget *parent) :
	BsWidget(parent)
{
	ui.setupUi(this);

	/* show user the list of available widgets */
	QSignalMapper *sm = new QSignalMapper(this);
	addToSignalMapper(sm, ui.osc, OSC);
	addToSignalMapper(sm, ui.fgen,FUNC_GEN);
	addToSignalMapper(sm, ui.pwm, PWM);
	addToSignalMapper(sm, ui.voltmeter, VOLTMETER);
	addToSignalMapper(sm, ui.dio, DIO);
	addToSignalMapper(sm, ui.ps, POWER_SUPPLY);
	addToSignalMapper(sm, ui.i2c, I2C);
	addToSignalMapper(sm, ui.spi, SPI);
	addToSignalMapper(sm, ui.actionDevInfo, DEV_INFO);
	addToSignalMapper(sm, ui.actionCalib, CALIB);
	connect(sm, SIGNAL(mapped(int)), SLOT(addWidget(int)));

	/* show about Box0 Studio dialog */
	connect(ui.actionAboutBox0Studio, SIGNAL(triggered()), SLOT(showAbout()));
}

/**
 * This is a common code method that is used to add a
 *   new widget to available widget list.
 * @param sm Signal mapper (for demux)
 * @param cb Button to link
 * @param cat A Unique ID of the widget that crosspond to addWidget
 */
void BsWelcome::addToSignalMapper(QSignalMapper *sm, QToolButton *tb, int cat)
{
	sm->setMapping(tb, cat);
	connect(tb, SIGNAL(clicked()), sm, SLOT(map()));
}

/**
 * addToSignalMapper() version for QAction
 * @param cm Signal Mapper (for demux)
 * @param act Action to link
 * @param cat A Unique ID of the widget that crosspond to addWidget
 */
void BsWelcome::addToSignalMapper(QSignalMapper *sm, QAction *act, int cat)
{
	sm->setMapping(act, cat);
	connect(act, SIGNAL(triggered()), sm, SLOT(map()));
}

/**
 * Add a widget.
 * This slot is called when user click any of the widget button.
 * @a type contain the Type of widget to add
 * @param type Widget type to add
 */
void BsWelcome::addWidget(int type)
{
	/* there is no device? */
	if (device == NULL) {
		/* TODO: show a message to user that we
		 *  have no device to open the widget for */
		return;
	}

	BsWidget *win;
	QWidget *p = this;

	switch (type) {
	case OSC:
		win = new BsOsc(p);
	break;
	case FUNC_GEN:
		win = new BsFuncGen(p);
	break;
	case PWM:
		win = new BsPwm(p);
	break;
	case DIO:
		win = new BsDio(p);
	break;
	case VOLTMETER:
		win = new BsVoltmeter(p);
	break;
	case POWER_SUPPLY:
		win = new BsPowerSupply(p);
	break;
	case I2C:
		win = new BsI2c(p);
	break;
	case SPI:
		win = new BsSpi(p);
	break;
	case DEV_INFO:
		win = new BsDeviceInfo(p);
	break;
	case CALIB:
		win = new BsCalib(p);
	break;
	default:
		/* ? */
		return;
	}

	win->setDevice(device);
	Q_EMIT addWidget(win);
}

/**
 * Ignore close event.
 * This is only useful when the User want to close the widget.
 * If the top level manager window decide to "kill" this widget (usecase: exit),
 *   it will simply ignore the event->ignore() and will remove the widget
 */
void BsWelcome::closeEvent(QCloseEvent *event)
{
	/* Block user from closing this window */
	event->ignore();
}

/**
 * Cache the @a dev pointer to device.
 * Later when a widget is added, it is used for the new widget setDevice()
 * @param dev Device pointer
 */
void BsWelcome::setDevice(b0_device *dev)
{
	device = dev;
}

void BsWelcome::showAbout()
{
	BsAbout about;
	about.exec();
}
