/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_WELCOME_H
#define BOX0_STUDIO_WELCOME_H

#include <QWidget>
#include <QObject>
#include <QWidget>
#include <QCloseEvent>
#include <QSignalMapper>
#include <QToolButton>
#include <QAction>

#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>

#include "widgets/widget/widget.h"
#include "widgets/instrument/instrument.h"

#include "ui_welcome.h"

class BsWelcome : public BsWidget
{
	Q_OBJECT

	public:
		BsWelcome(QWidget *parent = Q_NULLPTR);

		void setDevice(b0_device *dev);

	Q_SIGNALS:
		/**
		 * This signal is emitted when the @a BsWelcome (on behalf of User)
		 *  want to add a new widget.
		 * @a BsWelcome is a special class that is only granted the
		 *  facily to add widgets.
		 * @param widget Widget to be added
		 */
		void addWidget(BsWidget *widget);

	private Q_SLOTS:
		void addWidget(int type);
		void showAbout();

	protected:
		void closeEvent(QCloseEvent *event);

	private:
		Ui::BsWelcome ui;

		void addToSignalMapper(QSignalMapper *sm, QToolButton *tb, int cat);
		void addToSignalMapper(QSignalMapper *sm, QAction *act, int cat);
		b0_device *device = NULL;
};

#endif
