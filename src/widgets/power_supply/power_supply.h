/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_POWER_SUPPLY_H
#define BOX0_STUDIO_POWER_SUPPLY_H

#include "widgets/instrument/instrument.h"
#include <QWidget>
#include <libbox0/libbox0.h>

#include "ui_power_supply.h"

class BsPowerSupply : public BsWidget
{
	Q_OBJECT

	public:
		BsPowerSupply(QWidget *parent = Q_NULLPTR);
		void setDevice(b0_device *dev);

	private Q_SLOTS:
		void unitValueChanged(bool value);

	private:
		Ui::BsPowerSupply ui;
		b0_device *dev = NULL;
};

#endif
