/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_POWER_SUPPLY_UNIT_H
#define BOX0_STUDIO_POWER_SUPPLY_UNIT_H

#include <QWidget>

#include "ui_power_supply_unit.h"

class BsPowerSupplyUnit : public QWidget
{
	Q_OBJECT

	public:
		BsPowerSupplyUnit(QWidget *parent = Q_NULLPTR);
		void setName(const QString &name);

	Q_SIGNALS:
		/** this signal is emitted when user change value */
		void valueChanged(bool value);

	private Q_SLOTS:
		void buttonClicked();

	private:
		Ui::BsPowerSupplyUnit ui;
};

#endif
