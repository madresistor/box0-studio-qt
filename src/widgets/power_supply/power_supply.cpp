/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "power_supply.h"
#include "extra/box0/result_code.h"
#include <QDebug>
#include <libbox0/misc/box0-v5/box0-v5.h>
#include <libbox0/misc/box0-v5/power-supply.h>
#include <libbox0/backend/usb/usb.h>

BsPowerSupply::BsPowerSupply(QWidget *parent) :
	BsWidget(parent)
{
	ui.setupUi(this);
	setupTabDetach();

	ui.pm5->setName("±5V");
	connect(ui.pm5, SIGNAL(valueChanged(bool)), SLOT(unitValueChanged(bool)));

	ui.p3v3->setName("+3.3V");
	connect(ui.p3v3, SIGNAL(valueChanged(bool)), SLOT(unitValueChanged(bool)));
}

void BsPowerSupply::setDevice(b0_device *dev)
{
	if (B0_ERR_RC(b0v5_valid_test(dev))) {
		qWarning() << "Unable to confirm as box0-v5";
		deleteLater();
		dev = NULL;
		return;
	}

	this->dev = dev;
}

/**
 * This slot is called when user interact with the button of the unit
 *  and intent to change the state of the unit.
 * This will check the sender object to check if the unit is ±5V or +3.3V
 */
void BsPowerSupply::unitValueChanged(bool value)
{
	uint8_t selmask;

	if (dev == NULL) {
		qWarning() << "dev is NULL";
		return;
	}

	if (QObject::sender() == ui.pm5) {
		selmask = B0V5_PS_PM5;
	} else if (QObject::sender() == ui.p3v3) {
		selmask = B0V5_PS_P3V3;
	} else {
		return;
	}

	BsResultCode::handleInternal(
		b0v5_ps_en_set(dev, selmask, value ? selmask : 0x00),
		"power supply: unable to write to device");
}
