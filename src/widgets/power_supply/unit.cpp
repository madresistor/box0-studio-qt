/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "unit.h"

BsPowerSupplyUnit::BsPowerSupplyUnit(QWidget *parent) :
	QWidget(parent)
{
	ui.setupUi(this);

	connect(ui.btnOn, SIGNAL(clicked()), SLOT(buttonClicked()));
	connect(ui.btnOff, SIGNAL(clicked()), SLOT(buttonClicked()));
}

/**
 * Set name of the unit.
 * @param name Name
 */
void BsPowerSupplyUnit::setName(const QString &name)
{
	ui.lblName->setText(name);
}

/**
 * Slot to capture when a button is clicked.
 *  This would check if the sender object is On or Off button
 *  and accordingly set the value.
 */
void BsPowerSupplyUnit::buttonClicked()
{
	bool value;

	if (QObject::sender() == ui.btnOn) {
		value = true;
	} else if (QObject::sender() == ui.btnOff) {
		value = false;
	} else {
		return;
	}

	Q_EMIT valueChanged(value);
}
