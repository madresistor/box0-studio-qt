/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "widget.h"
#include <QPushButton>
#include <QMenuBar>
#include <QStatusBar>

/**
 * Intialize @a BsWidget.
 * At the moment, tab attach/detach is not initalized.
 * @a BsWidget is the base widget which all widget need to subclass.
 */
BsWidget::BsWidget(QWidget *parent) :
	QMainWindow (parent)
{
}

/**
 * Sub class can call this method to initalize tab attach/detach button.
 * The tab attach/detach button is shown at the right of the Menu bar.
 */
void BsWidget::setupTabDetach()
{
	QMenuBar *mb = menuBar();
	tab_detach = new BsTabDetach(this);
	mb->setCornerWidget(tab_detach);

	connect(tab_detach, SIGNAL(clicked()), SLOT(tabDetachClicked()));
}

/**
 * Show message in status bar.
 * @param message Message to be shown
 * @param timeout Time in Milliseconds (till which the message is shown) or
 *    0 to show till not cleared using @a clearMessage()
 */
void BsWidget::showMessage(const QString & message, int timeout)
{
	statusBar()->showMessage(message, timeout);
}

void BsWidget::clearMessage()
{
	statusBar()->clearMessage();
}

/**
 * When tab attach/detach button is clicked, this method is called.
 * This method will check the exact state of the button and flip the state.
 *   from detach to attach
 *   from attach to detach
 * The new state is converted passed to @a BsMainWindow using the
 *  @a showAsWindow signal.
 */
void BsWidget::tabDetachClicked()
{
	bool detached = tab_detach->isDetached();

	detached = !detached;

	tab_detach->setDetached(detached);
	Q_EMIT showAsWindow(this, detached);
}
