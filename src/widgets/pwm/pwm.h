/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_PWM_H
#define BOX0_STUDIO_PWM_H

#include <QObject>
#include <QWidget>
#include <QIcon>
#include <QTimer>
#include "widgets/instrument/instrument.h"
#include "extra/widget/hw_run.h"

#include <libbox0/libbox0.h>

#include "ui_pwm.h"

class BsPwm : public BsInstrument
{
	Q_OBJECT

	public:
		BsPwm(QWidget *parent = Q_NULLPTR);
		void loadModule(b0_device *dev, int index);

	protected:
		void closeEvent(QCloseEvent *event);

	private slots:
		void start();
		void stop();
		void calcMinError();
		void syncChannelWidth();

	private:
		Ui::BsPwm ui;

		BsHwRun *ui_hw_run;
		b0_pwm *module = NULL;

		/* when duty cycle is changed, this act as a dead-time timer
		 *  to wait for user to enter full value */
		QTimer *m_widthTimer;

		uint m_current_bitsize;
		b0_pwm_reg m_last_period;
};

#endif
