/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pwm.h"
#include <QApplication>
#include <QDesktopWidget>
#include "extra/box0/result_code.h"
#include "extra/box0/box0.h"
#include <QDebug>

BsPwm::BsPwm(QWidget *parent) :
	BsInstrument(B0_PWM, parent)
{
	ui.setupUi(this);

	/* Start/Stop button */
	ui_hw_run = new BsHwRun(this);
	ui.toolBar->addAction(ui_hw_run);
	connect(ui_hw_run, SIGNAL(userWantToStart()), SLOT(start()));
	connect(ui_hw_run, SIGNAL(userWantToStop()), SLOT(stop()));

	connect(ui.btnCalcMinError, SIGNAL(clicked()), SLOT(calcMinError()));

	/* restart when changed */
	connect(ui.dspinFreq, SIGNAL(valueChanged(double)), ui_hw_run, SLOT(restart()));
	connect(ui.dspinError, SIGNAL(valueChanged(double)), ui_hw_run, SLOT(restart()));

	/* keep duty cycle sync with device */
	m_widthTimer = new QTimer(this);
	m_widthTimer->setInterval(BS_HW_RUN_DEAD_TIME);
	m_widthTimer->setSingleShot(true);
	connect(m_widthTimer, SIGNAL(timeout()), SLOT(syncChannelWidth()));
	connect(ui.dspinDutyCycle, SIGNAL(valueChanged(double)),
		m_widthTimer, SLOT(start()));
}

/**
 * Open and initalize a PWM module from device @a dev with @a index
 * @param dev Device
 * @param index Index
 */
void BsPwm::loadModule(b0_device *dev, int index)
{
	b0_result_code r;

	/* search for module to bind to */
	r = b0_pwm_open(dev, &module, index);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("PWM"));
		rc.setMessage(tr("Open failed"));
		rc.exec();
		deleteLater();
		return;
	}

	r = b0_pwm_output_prepare(module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("PWM"));
		rc.setMessage(tr("Prepare failed"));
		rc.exec();
		deleteLater();
		return;
	}

	showModuleInTitle(&module->header);

	if (module->label.pin[0] != NULL) {
		QString label = utf8_to_qstring(module->label.pin[0]);
		QString dutyCycle = ui.lblDutyCycle->text();
		ui.lblDutyCycle->setText(QString("%1 (%2)").arg(dutyCycle).arg(label));
	}

	m_current_bitsize = module->bitsize.values[0];
}

/**
 * Called when user want to start PWM
 */
void BsPwm::start()
{
	b0_result_code r;

	/* stop if the timer is running,
	 *  anyway the whole thing is going to be calculated */
	m_widthTimer->stop();

	qreal freq = ui.dspinFreq->value();
	qreal duty_cycle = ui.dspinDutyCycle->value();
	qreal error = ui.dspinError->value();

	unsigned long speed;
	b0_pwm_reg period;
	r = b0_pwm_output_calc(module, m_current_bitsize, freq, &speed, &period,
				error, true);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Calculation failed"));
		rc.setMessage(tr("Unable to calculate values for PWM output"));
		rc.exec();
		return;
	}

	r = b0_pwm_speed_set(module, speed);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Speed set failed"));
		rc.setMessage(tr("Unable to set speed value to PWM"));
		rc.exec();
		return;
	}

	r = b0_pwm_period_set(module, period);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Period set failed"));
		rc.setMessage(tr("Unable to set period value to PWM"));
		rc.exec();
		return;
	}

	b0_pwm_reg width = B0_PWM_OUTPUT_CALC_WIDTH(period, duty_cycle);
	r = b0_pwm_width_set(module, 0, width);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Width set failed"));
		rc.setMessage(tr("Unable to set width value to PWM"));
		rc.exec();
		return;
	}

	m_last_period = period;
	r = b0_pwm_output_start(module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Start failed"));
		rc.setMessage(tr("Unable to start PWM"));
		rc.exec();
		return;
	}

	qreal freq_actual = B0_PWM_OUTPUT_CALC_FREQ(speed, period);
	QString msg = QString("Running at %1 Hertz (%2 Hertz ± %3% with %4% duty cycle)")
		.arg(freq_actual, 0, 'f', 3)
		.arg(freq, 0, 'f', 3)
		.arg(error, 0, 'f', 3)
		.arg(duty_cycle, 0, 'f', 3);

	showMessage(msg);

	ui_hw_run->setRunning(true);
}

/**
 * Called when user want to stop PWM
 */
void BsPwm::stop()
{
	BsResultCode::handleInternal(
		b0_pwm_output_stop(module), "unable to  stop module");
	ui_hw_run->setRunning(false);
}

void BsPwm::closeEvent(QCloseEvent *event)
{
	if (module != NULL) {
		/* if pwm is in running, stop it */
		if (ui_hw_run->isRunning()) {
			stop();
		}

		/* close the module */
		BsResultCode::handleInternal(b0_pwm_close(module),
			"unable to close PWM module");
		module = NULL;
	}

	BsInstrument::closeEvent(event);
}

/**
 * Calculate the minimum error (that is possible) and show it to the user
 * @todo perform calculation in background thread
 */
void BsPwm::calcMinError()
{
	b0_result_code r;
	qreal max_error = 100;
	qreal freq = ui.dspinFreq->value(), actual_freq;
	unsigned long speed;
	b0_pwm_reg period;

	showMessage("Calculating...");

	/* TODO: perform calculation on non-gui thread */
	r = b0_pwm_output_calc(module, m_current_bitsize, freq, &speed, &period,
						max_error, true);
	if (B0_ERR_RC(r)) {
		clearMessage();
		BsResultCode rc(r);
		rc.setTitle(tr("Error occured"));
		rc.setMessage(tr("Error in calculating minimum error calculation"));
		rc.exec();
		return;
	}

	actual_freq = B0_PWM_OUTPUT_CALC_FREQ(speed, period);
	max_error = B0_PWM_OUTPUT_CALC_FREQ_ERR(freq, actual_freq);

	ui.dspinError->setValue(max_error);
	showMessage("Minimum error calculated.", 3000);
}

void BsPwm::syncChannelWidth()
{
	b0_result_code r;

	if (!ui_hw_run->isRunning()) {
		/* looks like module isn't running anymore */
		return;
	}

	qreal duty_cycle = ui.dspinDutyCycle->value();
	b0_pwm_reg width = B0_PWM_OUTPUT_CALC_WIDTH(m_last_period, duty_cycle);
	r = b0_pwm_width_set(module, 0, width);
	if (B0_ERR_RC(r)) {
		stop();
		BsResultCode rc(r);
		rc.setTitle(tr("Set failed"));
		rc.setMessage(tr("Unable to set width value"));
		rc.exec();
		return;
	}

	showMessage(QString("Duty cycle set to %1%").arg(duty_cycle), 3000);
}
