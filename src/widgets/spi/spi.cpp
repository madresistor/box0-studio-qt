/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spi.h"
#include <QMessageBox>
#include <QApplication>
#include <QDesktopWidget>
#include "extra/box0/result_code.h"

BsSpi::BsSpi(QWidget *parent) :
	BsInstrument(B0_SPI, parent)
{
	ui.setupUi(this);
}

void BsSpi::loadModule(b0_device *dev, int index)
{
	b0_result_code r;

	/* search for module to bind to */
	r = b0_spi_open(dev, &module, index);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("SPI"));
		rc.setMessage(tr("Open failed"));
		rc.exec();
		deleteLater();
		return;
	}

	r = b0_spi_master_prepare(module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("SPI"));
		rc.setMessage(tr("Prepare for master mode failed"));
		rc.exec();
		deleteLater();
		return;
	}

	ui.basic->setModule(module);
	showModuleInTitle(&module->header);
}

void BsSpi::closeEvent(QCloseEvent *event)
{
	if (module != NULL) {
		/* close the module */
		BsResultCode::handleInternal(b0_spi_close(module),
			"unable to close SPI module");

		module = NULL;
	}

	BsInstrument::closeEvent(event);
}
