/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "basic.h"
#include "extra/box0/box0.h"
#include <QApplication>
#include <QDateTime>
#include <QFile>
#include <QTextStream>
#include <QFileInfo>
#include <QStandardPaths>
#include <QFileDialog>
#include <QMessageBox>
#include <QButtonGroup>
#include <QDebug>

/**
 * Split the string by an specific number of characters
 * @param data String to split
 * @param chars Number of character to group together
 * @return hexified
 */
static QString hexify(const QByteArray &arr, uint part_len = 1)
{
	QStringList list;

	for (uint i = 0; i < (uint) arr.size(); i += part_len) {
		list << arr.mid(i, part_len).toHex();
	}

	return list.join(" ").toUpper();
}

/**
 * Convert libbox0 result code to a user showable string
 * @param rc Result code
 * @return details
 */
static QString rc_details(b0_result_code rc)
{
	QString name = utf8_to_qstring(b0_result_name(rc));
	QString explain = utf8_to_qstring(b0_result_explain(rc));
	return QString("%1 - %2").arg(name).arg(explain);
}

BsSpiBasic::BsSpiBasic(QWidget *parent) :
	QWidget(parent),
	m_module(NULL)
{
	ui.setupUi(this);
	ui.speed->setUnit("Hz");

	connect(ui.logSave, SIGNAL(clicked()), SLOT(operSave()));

	QButtonGroup *duplex = new QButtonGroup(this);
	duplex->addButton(ui.full_duplex);
	duplex->addButton(ui.half_duplex_read);
	duplex->addButton(ui.half_duplex_write);

	QButtonGroup *mode = new QButtonGroup(this);
	mode->addButton(ui.mode0, 0);
	mode->addButton(ui.mode1, 1);
	mode->addButton(ui.mode2, 2);
	mode->addButton(ui.mode3, 3);

	connect(ui.count, SIGNAL(valueChanged(int)),SLOT(syncData()));
	connect(ui.bitsize, SIGNAL(currentIndexChanged(int)),SLOT(syncData()));
	connect(duplex, SIGNAL(buttonToggled(int,bool)),SLOT(duplexChanged()));
	connect(mode, SIGNAL(buttonToggled(int,bool)),SLOT(modeToggled(int,bool)));
	connect(ui.execute, SIGNAL(clicked()), SLOT(execute()));
	connect(ui.ss, SIGNAL(currentIndexChanged(int)), SLOT(updatePolarity()));
	connect(ui.active_low, SIGNAL(toggled(bool)), SLOT(updatePolarity(bool)));
	connect(ui.active_high, SIGNAL(toggled(bool)), SLOT(updatePolarity(bool)));

	/* Select mode 0.
	 * This is also done to make sure that the image is properly shown.
	 * The default pixmap is not rendered properly. (blur) */
	modeToggled(0, true);

	syncData();
	duplexChanged();
}

void BsSpiBasic::setUiEnabled(bool enable)
{
	ui.ss->setEnabled(enable);
	ui.speed->setEnabled(enable);
	ui.bitsize->setEnabled(enable);
	ui.count->setEnabled(enable);
	ui.polarity->setEnabled(enable);
	ui.mode->setEnabled(enable);
	ui.bit_order->setEnabled(enable);
	ui.duplex->setEnabled(enable);

	bool fd = ui.full_duplex->isChecked();
	ui.data_input->setEnabled(enable and (fd or ui.half_duplex_read->isChecked()));
	ui.data_output->setEnabled(enable and (fd or ui.half_duplex_write->isChecked()));
}

void BsSpiBasic::duplexChanged()
{
	bool fd = ui.full_duplex->isChecked();
	bool input = fd or ui.half_duplex_read->isChecked();
	bool output = fd or ui.half_duplex_write->isChecked();

	ui.data_input->setEnabled(input);
	ui.label_data_input->setEnabled(input);

	ui.data_output->setEnabled(output);
	ui.label_data_output->setEnabled(output);
}

void BsSpiBasic::modeToggled(int mode, bool checked)
{
	if (checked) {
			QString file = QString(":/spi/mode/%1").arg(mode);
			QPixmap img = QIcon(file).pixmap(ui.mode_img->size());
			ui.mode_img->setPixmap(img);
	}
}

/**
 * Save log to disk
 */
void BsSpiBasic::operSave()
{
	QString docDir = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
	QString path = QString("%1/%2").arg(docDir).arg("spi-log.html");
	QString filename = QFileDialog::getSaveFileName(this, tr("Save to file"),
		path, "Hyper Text Markup Language (*.html, *.htm)");

	if (filename.isEmpty()) {
		return;
	}

	/* Ref: http://stackoverflow.com/a/9822246/1500988 */
	if (QFileInfo(filename).suffix().isEmpty()) {
		filename.append(".html");
	}

	QFile file(filename);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
		QMessageBox::warning(this, tr("Writing failed"), tr("Unable to open file."));
		return;
	}

	QTextStream out(&file);
	out << ui.log->document()->toHtml();
	file.close();
}

/**
 * Synchronise data inputs with bitsize and count value
 */
void BsSpiBasic::syncData()
{
	int count = ui.count->value();
	uint bitsize = ui.bitsize->currentData().toUInt();
	uint bytesize = (bitsize + 7) / 8;

	/* Backup data */
	QByteArray data_input = QByteArray::fromHex(ui.data_input->text().toLatin1());
	QByteArray data_output = QByteArray::fromHex(ui.data_output->text().toLatin1());

	/* Memory effeciency if not a big concern */
	QString bytes = QString("HH").repeated(bytesize);

	/* "HH" */
	QString inputMask = bytes;

	if (count > 1) {
		/* "HH HH HH ..... HH" */
		QString bytes_with_space = bytes + " ";
		inputMask.prepend(bytes_with_space.repeated(count - 1));
	}

	inputMask.prepend(">");
	ui.data_input->setInputMask(inputMask);
	ui.data_output->setInputMask(inputMask);

	data_input.resize(count * bytesize);
	data_output.resize(count * bytesize);
	ui.data_input->setText(hexify(data_input, bytesize));
	ui.data_output->setText(hexify(data_output,bytesize));
}

void BsSpiBasic::setModule(b0_spi *mod)
{
	m_module = mod;
	ui.ss->clear();
	ui.speed->clear();
	ui.bitsize->clear();

	for (size_t i = 0; i < mod->ss_count; i++) {
		const uint8_t *_name = mod->label.ss[i];
		QString name = (_name != NULL) ?
			utf8_to_qstring(mod->label.ss[i]) :
			QString("SS%1").arg(i);
		ui.ss->addItem(name, QVariant(static_cast<uint>(i)));
	}

	for (size_t i = 0; i < mod->speed.count; i++) {
		ui.speed->addValue(mod->speed.values[i]);
	}

	ssize_t bitsize_8_index = -1;

	for (size_t i = 0; i < mod->bitsize.count; i++) {
		ui.bitsize->addValue(mod->bitsize.values[i]);

		if (mod->bitsize.values[i] == 8) {
			bitsize_8_index = i;
		}
	}

	if (bitsize_8_index != -1) {
		/* If the device support 8bit then select it by default.
		 * Generally 8bit bus is used */
		ui.bitsize->setCurrentIndex(bitsize_8_index);
	}

	/* Read polarity from device */
	unsigned ss = (unsigned) ui.ss->currentData().toUInt();
	QString ss_str = ui.ss->currentText();
	bool active_high;
	if (B0_ERR_RC(b0_spi_active_state_get(m_module, ss, &active_high))) {
		qWarning() << "Failed to read " << ss_str << " polarity from device";
	} else {
		ui.active_high->setChecked(active_high);
		ui.active_low->setChecked(!active_high);
	}
}

/**
 * @param yes If true, update polarity. If false, ignore the call.
 */
void BsSpiBasic::updatePolarity(bool yes)
{
	if (!yes) {
		return;
	}

	unsigned ss = (uint8_t) ui.ss->currentData().toUInt();
	bool active_high = ui.active_high->isChecked();

	QString ss_str = ui.ss->currentText();
	QString active_high_str = active_high ? "Active HIGH" : "Active LOW";
	QString dateTime = QDateTime::currentDateTime().toString();

	setUiEnabled(false);

	b0_result_code rc = b0_spi_active_state_set(m_module, ss, active_high);
	if (B0_ERR_RC(rc)) {
		ui.log->append(QString(tr("<div style='color:red'>%1: %2 - Polarity to %3 change failed (%4)</div>"))
				.arg(dateTime).arg(ss_str).arg(active_high_str).arg(rc_details(rc)));
	} else {
		ui.log->append(QString(tr("<div style='color:green'>%1: %2 - Polartity to %3 changed</div>"))
				.arg(dateTime).arg(ss_str).arg(active_high_str));
	}

	setUiEnabled(true);
}

void BsSpiBasic::execute()
{
	setUiEnabled(false);

	QString dateTime = QDateTime::currentDateTime().toString();

	const b0_spi_task_flags flag_mode[] = {
		[0] = B0_SPI_TASK_MODE0,
		[1] = B0_SPI_TASK_MODE1,
		[2] = B0_SPI_TASK_MODE2,
		[3] = B0_SPI_TASK_MODE3
	};

	unsigned ss = (uint8_t) ui.ss->currentData().toUInt();
	unsigned long speed = (unsigned long) ui.speed->currentValue();
	unsigned bitsize = ui.bitsize->currentValue();
	size_t count = (size_t) ui.count->value();
	b0_spi_task_flags mode = flag_mode[ui.mode0->group()->checkedId()];
	b0_spi_task_flags bit_first = ui.msb_first->isChecked() ?
						B0_SPI_TASK_MSB_FIRST : B0_SPI_TASK_LSB_FIRST;

	const b0_spi_sugar_arg arg = {
		.addr = ss,
		.flags = static_cast<b0_spi_task_flags>(mode | bit_first),
		.bitsize = bitsize,
		.speed = speed
	};

	QByteArray data_input = QByteArray::fromHex(ui.data_input->text().toLatin1());
	QByteArray data_output = QByteArray::fromHex(ui.data_output->text().toLatin1());
	data_input.resize(count);
	data_output.resize(count);

	QString data_output_hex = hexify(data_output);

	uint8_t *data_input_ptr = (uint8_t *) data_input.data();
	const uint8_t *data_output_ptr = (const uint8_t *) data_output.constData();

	QString ss_str = ui.ss->currentText();

	if (ui.full_duplex->isChecked()) {
		b0_result_code rc = b0_spi_master_fd(m_module, &arg, data_output_ptr, data_input_ptr, count);
		if (B0_ERR_RC(rc)) {
			ui.log->append(QString(tr("<div style='color:red'>%1: %2 - Full duplex operation failed %3 (%4)</div>"))
				.arg(dateTime).arg(ss_str).arg(data_output_hex).arg(rc_details(rc)));
		} else {
			QString data_input_hex = hexify(data_input);
			ui.log->append(QString(tr("<div style='color:green'>%1: %2 - Written %3 and readed %4</div>"))
				.arg(dateTime).arg(ss_str).arg(data_output_hex).arg(data_input_hex));
			ui.data_input->setText(data_input_hex);
		}
	} else if (ui.half_duplex_read->isChecked()) {
		b0_result_code rc = b0_spi_master_hd_read(m_module, &arg, data_input_ptr, count);
		if (B0_ERR_RC(rc)) {
			ui.log->append(QString(tr("<div style='color:red'>%1: %2 - Half duplex read failed (%3)</div>"))
				.arg(dateTime).arg(ss_str).arg(rc_details(rc)));
		} else {
			QString data_input_hex = hexify(data_input);
			ui.log->append(QString(tr("<div style='color:green'>%1: %2 - Readed %3 </div>"))
				.arg(dateTime).arg(ss_str).arg(data_input_hex));
			ui.data_input->setText(data_input_hex);
		}
	} else if (ui.half_duplex_write->isChecked()) {
		b0_result_code rc = b0_spi_master_hd_write(m_module, &arg, data_output_ptr, count);
		if (B0_ERR_RC(rc)) {
			ui.log->append(QString(tr("<div style='color:red'>%1: %2 - Half duplex write failed %3 (%4)</div>"))
				.arg(dateTime).arg(ss_str).arg(data_output_hex).arg(rc_details(rc)));
		} else {
			ui.log->append(QString(tr("<div style='color:green'>%1: %2 - Written %3 </div>"))
				.arg(dateTime).arg(ss_str).arg(data_output_hex));
		}
	} else {
		/* lawl?! */
	}

	ui.log->append("<br/>");

	setUiEnabled(true);
}
