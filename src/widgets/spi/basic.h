/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_SPI_BASIC_H
#define BOX0_STUDIO_SPI_BASIC_H

#include <QObject>
#include <QWidget>
#include <libbox0/libbox0.h>

#include "ui_spi_basic.h"

class BsSpiBasic : public QWidget
{
	Q_OBJECT

	public:
		BsSpiBasic(QWidget *parent = 0);
		void setModule(b0_spi *mod);

	private Q_SLOTS:
		void operSave();
		void syncData();
		void duplexChanged();
		void updatePolarity(bool yes = true);
		void modeToggled(int mode, bool checked);
		void setUiEnabled(bool enable);
		void execute();

	private:
		Ui::BsSpiBasic ui;
		b0_spi *m_module;
};

#endif
