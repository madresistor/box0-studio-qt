/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_VOLTMETER_H
#define BOX0_STUDIO_VOLTMETER_H

#include <QWidget>
#include "widgets/instrument/instrument.h"
#include <libbox0/libbox0.h>
#include <QTimerEvent>
#include <QAction>
#include <QComboBox>
#include <QThread>
#include "sampler.h"
#include "extra/widget/hw_run.h"

#include "ui_voltmeter.h"

class BsVoltmeter : public BsInstrument
{
	Q_OBJECT

	public:
		BsVoltmeter(QWidget *parent = Q_NULLPTR);
		void loadModule(b0_device *dev, int index);
		~BsVoltmeter();

	private slots:
		void feed(qreal value);
		void start();
		void stop();

	protected:
		void closeEvent(QCloseEvent *);

	private:
		void searchFavourableSpeed();
		void buildChannelList();

		Ui::BsVoltmeter ui;
		b0_ain *module = NULL;

		/* the showest and favourable sped that will be used in transfer */
		unsigned int stream_bitsize;
		unsigned long stream_speed;

		/* channel to show */
		QString m_channelName;
		QString m_unit;
		int m_precision;
		char m_format;
		uint m_running_chan;

		BsHwRun *ui_hw_run;
		QComboBox *ui_channel;

		BsVoltmeterSampler *sampler;

		struct {
			/* calib_output = (uncalib_input * gain) - offset; */
			qreal *gain = nullptr;
			qreal *offset = nullptr;
		} calib;
};

#endif
