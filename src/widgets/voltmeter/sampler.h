/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_VOLTMETER_SAMPLER_H
#define BOX0_STUDIO_VOLTMETER_SAMPLER_H

#include <QWidget>
#include <QThread>
#include "extra/sampler/ain/stream.h"

class BsVoltmeterSampler : public BsAinStreamSampler
{
	Q_OBJECT

	public:
		BsVoltmeterSampler(QWidget *parent);

	Q_SIGNALS:
		/**
		 * This signal is emitted when the sampler want to pass on the
		 *  average value result
		 * @param avg Averaged value
		 */
		void output(qreal avg);

	private Q_SLOTS:
		void average(float *values, uint count);
};

#endif
