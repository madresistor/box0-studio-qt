/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "voltmeter.h"
#include <QApplication>
#include <QDesktopWidget>
#include "extra/box0/result_code.h"
#include <QDebug>
#include "extra/box0/box0.h"
#include <libbox0/misc/box0-v5/box0-v5.h>
#include <libbox0/misc/box0-v5/calib.h>

BsVoltmeter::BsVoltmeter(QWidget *parent) :
	BsInstrument(B0_AIN, parent)
{
	ui.setupUi(this);

	/* Start/Stop button */
	ui_hw_run = new BsHwRun(this);
	ui.toolBar->addAction(ui_hw_run);
	connect(ui_hw_run, SIGNAL(userWantToStart()), SLOT(start()));
	connect(ui_hw_run, SIGNAL(userWantToStop()), SLOT(stop()));

	ui.toolBar->addSeparator();

	QFont font;
	font.setPointSize(10);

	/* TODO: let user select channel using ui_channel combo box */
	ui_channel = new QComboBox(this);
	ui_channel->setMinimumSize(100, 32);
	ui_channel->setFont(font);
	ui.toolBar->addWidget(ui_channel);

	connect(ui_channel, SIGNAL(currentIndexChanged(int)),
			ui_hw_run, SLOT(restart()));

	/* sampler */
	sampler = new BsVoltmeterSampler(this);
	connect(sampler, SIGNAL(output(qreal)), SLOT(feed(qreal)),
		Qt::QueuedConnection);
}

BsVoltmeter::~BsVoltmeter()
{
	if (calib.gain != nullptr) {
		delete[] calib.gain;
	}

	if (calib.offset != nullptr) {
		delete[] calib.offset;
	}
}

/**
 * Read calibration from device
 * It check if the device is box0-v5 and read the calibration data for AIN0
 */
static void read_calib(b0_device *dev, b0_ain *mod,
			qreal *gain, qreal *offset)
{
	/* reset previous calib */
	for (uint i = 0; i < mod->chan_count; i++) {
		gain[i] = 1;
		offset[i] = 0;
	}

	if (B0_ERR_RC(b0v5_valid_test(dev))) {
		/* not box0-v5 */
		return;
	}

	if (mod->header.index != 0) {
		/* box0-v5 only have AIN0 calib data */
		return;
	}

	/* read calibration data */
	b0v5_calib_value values[mod->chan_count];
	b0v5_calib calib = {
		/* .ain0 = */ {
			/* .values = */ values,
			/* .count = */ mod->chan_count
		},

		/* .aout0 = */ {
			/* .values = */ NULL,
			/* .count = */ 0
		}
	};

	if (B0_ERR_RC(b0v5_calib_read(dev, &calib))) {
		qWarning("Unable to read box0-v5 AIN0 calib");
		return;
	}

	for (uint i = 0; i < mod->chan_count; i++) {
		gain[i] = values[i].gain;
		offset[i] = values[i].offset;
	}
}

/**
 * Load the AIN module from @a dev with @a index
 * @param dev Device of the module
 * @param index Index of the module
 */
void BsVoltmeter::loadModule(b0_device *dev, int index)
{
	b0_result_code r;

	/* find module */
	r = b0_ain_open(dev, &module, index);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AIN"));
		rc.setMessage(tr("Open failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* prepare */
	r = b0_ain_stream_prepare(module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AIN"));
		rc.setMessage(tr("Prepare failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* dont show more than 3 digits after decimals */
	/* TODO: determine based on AIN resolution */
	m_precision = 3;
	m_format = 'f';
	m_unit = ref_type_to_unit(module->ref.type);

	/* set the range */
	qreal max = module->ref.high;
	qreal min = module->ref.low;
	ui.scale->setRange(min, max);
	//~ ui.scale->setLabelsFormat(format, precision);

	buildChannelList();
	searchFavourableSpeed();
	showModuleInTitle(&module->header);

	if (calib.gain != nullptr) {
		delete[] calib.gain;
	}

	if (calib.offset != nullptr) {
		delete[] calib.offset;
	}

	calib.gain = new qreal[module->chan_count];
	calib.offset = new qreal[module->chan_count];
	read_calib(dev, module, calib.gain, calib.offset);
}

/**
 * Build a channel list from the modules channel
 */
void BsVoltmeter::buildChannelList()
{
	/* Store the current channel index */
	int current_chan = ui_channel->currentIndex();

	/* clear any previous list */
	ui_channel->clear();

	for (size_t i = 0; i < module->chan_count; i++) {
		QString channelName;

		if (module->label.chan[i] != NULL) {
			channelName = utf8_to_qstring(module->label.chan[i]);
		} else {
			channelName = QString("CH%1").arg(i);
		}

		ui_channel->addItem(channelName, (int) i);
	}

	/* Switch to last channel */
	if (current_chan == -1) {
		current_chan = 0;
	}
	ui_channel->setCurrentIndex(current_chan);


}

/**
 * Search for a favourable speed
 * The judging criteria is:
 *  - slowest speed,
 *  - height bitsize
 */
void BsVoltmeter::searchFavourableSpeed()
{
	stream_bitsize = 0;
	stream_speed = 0;

	for (size_t i = 0; i < module->stream.count; i++) {
		struct b0_ain::b0_ain_mode_bitsize_speeds::b0_ain_bitsize_speeds
			*value = &module->stream.values[i];

		/* Try to find heigher or same bitsize */
		if (value->bitsize < stream_bitsize) {
			continue;
		}

		stream_bitsize = value->bitsize;

		for (size_t j = 0; j < value->speed.count; j++) {
			if (value->speed.values[j] > stream_speed) {
				stream_speed = value->speed.values[j];
			}
		}
	}

	if (!stream_bitsize || !stream_speed) {
		qDebug() << "no favourable speed,bitsize combination found";
		return;
	}

	/* bitsize speed */
	b0_result_code r = b0_ain_bitsize_speed_set(module, stream_bitsize, stream_speed);
	if (!BsResultCode::handleInternal(r, "b0_ain_bitsize_speed_set() failed")) {
		QMessageBox::warning(this, tr("Bitsize, Speed failed"),
			tr("Unable to set AIN bitsize,speed"));
		return;
	}
}

/**
 * Called when stopped and user want to stop
 */
void BsVoltmeter::start()
{
	b0_result_code r;

	if (!stream_bitsize || !stream_speed) {
		QMessageBox::warning(this, tr("Speed failed"),
			tr("Cannot find any favourable configuration"));
		return;
	}

	/* channel */
	m_running_chan = ui_channel->currentData().toUInt();
	r = b0_ain_chan_seq_set(module, &m_running_chan, 1);
	if (!BsResultCode::handleInternal(r, "b0_ain_chan_seq_set() failed")) {
		QMessageBox::warning(this, tr("Channel failed"),
			tr("Unable to set AIN channel"));
		return;
	}

	/* start the module */
	r = b0_ain_stream_start(module);
	if (!BsResultCode::handleInternal(r, "b0_ain_stream_start() failed")) {
		/* looks like we have a problem */
		QMessageBox::warning(this, tr("Start failed"),
			tr("Unable to start AIN module in stream mode"));
		return;
	}

	m_channelName = ui_channel->currentText();

	/* we are ready to accept data */
	uint samples_per_callback = stream_speed / 4;
	sampler->start(module, samples_per_callback);

	ui_hw_run->setRunning(true);
}

/**
 * Called when running and user want to stop
 */
void BsVoltmeter::stop()
{
	/* signal and wait for the thread to exit */
	sampler->requestInterruption();
	sampler->wait();

	/* send a stop */
	BsResultCode::handleInternal(b0_ain_stream_stop(module),
		"Unable to stop ain module");

	ui.label->setText(QString());
	ui.scale->setValue(0);

	ui_hw_run->setRunning(false);
}

/**
 * Show the value in the UI
 * @param value Voltage value
 */
void BsVoltmeter::feed(qreal value)
{
	QString svalue;

	/* Apply calibration */
	value = (value * calib.gain[m_running_chan]) - calib.offset[m_running_chan];

	/* fetch the value from variable and place in scale, and label */
	if (abs(value) >= 1) {
		svalue = QString::number(value, m_format, m_precision);
	} else {
		svalue = QString("%1 m").arg(value * 1000, 0, m_format, m_precision);
	}

	QString text = QString("%1: %2%3").arg(m_channelName).arg(svalue).arg(m_unit);
	ui.label->setText(text);
	ui.scale->setValue(value);
}

/**
 * Before accepting close event, perform:
 *  - if running, stop
 *  - if module is opened, close it
 * @param event Close event
 */
void BsVoltmeter::closeEvent(QCloseEvent *event)
{
	if (module != NULL) {
		/* if voltmeter is in running, stop it */
		if (ui_hw_run->isRunning()) {
			stop();
		}

		/* close the module */
		BsResultCode::handleInternal(b0_ain_close(module),
			"unable to close AIN module");
		module = NULL;
	}

	BsInstrument::closeEvent(event);
}
