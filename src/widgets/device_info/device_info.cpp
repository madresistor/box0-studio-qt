/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "device_info.h"
#include <QDebug>
#include "extra/box0/result_code.h"
#include "extra/box0/box0.h"

BsDeviceInfo::BsDeviceInfo(QWidget *parent) :
	BsWidget(parent)
{
	ui.setupUi(this);
	setupTabDetach();
}

/**
 * Extract the device information and set them to the inputs
 */
void BsDeviceInfo::setDevice(b0_device *dev)
{
	if (dev == NULL) {
		ui.name->clear();
		ui.manuf->clear();
		ui.serial->clear();
		return;
	}

	ui.name->setText(utf8_to_qstring(dev->name));
	ui.manuf->setText(utf8_to_qstring(dev->manuf));
	ui.serial->setText(utf8_to_qstring(dev->serial));
}
