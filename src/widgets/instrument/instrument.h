/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_INSTRUMENT_WINDOW_H
#define BOX0_STUDIO_INSTRUMENT_WINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QKeyEvent>
#include <QAction>
#include <QColor>
#include <QList>
#include <QStatusBar>
#include <QMenuBar>
#include <libbox0/libbox0.h>
#include "widgets/widget/widget.h"

/**
 * Instrument is a special widget that perform a work that require a single module
 * This help in commonizing code
 * Instrument will show a dialog Of available type of module (if multiple)
 *  and pass on the module index to @a loadModule
 */
class BsInstrument : public BsWidget
{
	Q_OBJECT

	public:
		BsInstrument(b0_module_type type, QWidget *parent);
		void setDevice(b0_device *dev);
		virtual void loadModule(b0_device *dev, int index) = 0;

	protected:
		void showModuleInTitle(b0_module *mod);

	private:
		b0_module_type m_module_type;
};

#endif
