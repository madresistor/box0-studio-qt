/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "module_selector.h"
#include <QListWidgetItem>
#include <Qt>
#include <QMetaType>
#include "extra/box0/box0.h"

BsModuleSelector::BsModuleSelector(b0_device *dev, int type, QWidget *parent) :
	QDialog(parent)
{
	unsigned i;
	ui.setupUi(this);

	/* search and add the modules to list that match the type */
	for (i =0; i < dev->modules_len; i++) {
		b0_module *mod = dev->modules[i];
		if (mod->type == type) {
			addModule(mod);
		}
	}

	/* select the first entry */
	ui.list->setCurrentRow(0);
}

int BsModuleSelector::exec(void)
{
	int count = ui.list->count();

	/* there is nothing to select */
	if (count <= 0) {
		return QDialog::Rejected;
	}

	/* there is only one entry to select */
	if (count == 1) {
		return QDialog::Accepted;
	}

	/* let the user decide */
	return QDialog::exec();
}

/**
 * Return the selected module (by user)
 * @return module
 */
b0_module* BsModuleSelector::selectedModule(void)
{
	QListWidgetItem *item = ui.list->currentItem();
	if (item == Q_NULLPTR) {
		return NULL;
	}

	return (b0_module *) item->data(Qt::UserRole).value<void *>();
}

/**
 * Add a module to the list from which user can select the module
 * @param mod Module
 */
void BsModuleSelector::addModule(b0_module *mod)
{
	QListWidgetItem *item = new QListWidgetItem(ui.list);
	item->setText(utf8_to_qstring(mod->name));
	item->setData(Qt::UserRole, QVariant::fromValue((void *) mod));
}
