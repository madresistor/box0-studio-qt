/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "instrument.h"
#include "module_selector.h"
#include "extra/box0/box0.h"

/**
 * All instruments are attachable/detachable
 */
BsInstrument::BsInstrument(b0_module_type module_type, QWidget *parent) :
	BsWidget(parent),
	m_module_type(module_type)
{
	setupTabDetach();
}

/**
 * Set the current module name in title
 * @param mod module
 */
void BsInstrument::showModuleInTitle(b0_module *mod)
{
	if (mod->name == NULL) {
		return;
	}

	/* show module name in window title */
	QString text = utf8_to_qstring(mod->name);
	setWindowTitle(QString("%1 — %2").arg(windowTitle()).arg(text));
}

/**
 * Set device to instrument.
 * This will show a dialog to user to select from available modules
 * When user select one of a module, it is passed on to @a loadModule()
 * If no modules are selected, calls @a deleteLater() to hide the widget from user
 * @param device Device
 */
void BsInstrument::setDevice(b0_device *device)
{
	/* search for the module */
	BsModuleSelector sel(device, m_module_type);
	if (!sel.exec()) {
		deleteLater();
		return;
	}

	/* get selected module */
	b0_module *mod = sel.selectedModule();
	if (mod == NULL) {
		deleteLater();
		return;
	}

	loadModule(device, mod->index);
}
