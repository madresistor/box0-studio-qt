/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_FUNC_GEN_H
#define BOX0_STUDIO_FUNC_GEN_H

#include <QWidget>
#include <QObject>
#include "widgets/instrument/instrument.h"
#include <libbox0/libbox0.h>
#include <QTimer>
#include <QSpinBox>
#include <QList>
#include <QButtonGroup>
#include "chan/chan.h"
#include "extra/widget/bitsize.h"
#include "extra/widget/hw_run.h"

#include "ui_func_gen.h"

class BsFuncGen : public BsInstrument
{
	Q_OBJECT

	public:
		BsFuncGen(QWidget *parent = Q_NULLPTR);
		void loadModule(b0_device *dev, int index);

	protected:
		void closeEvent(QCloseEvent *);

	private Q_SLOTS:
		void start();
		void stop();
		void stopTimeout();
		void setGraphLineWidth(qreal value);

	private:
		Ui::BsFuncGen ui;

		BsHwRun *ui_hw_run;

		/* after a reapeat (finite), this will cause a stop */
		QTimer repeatStop;

		/* box0 related */
		b0_aout *module = NULL;

		QSpinBox *ui_repeat;
		BsBitsize *ui_bitsize;

		QList<BsFuncGenChan *> channels;

		QButtonGroup *m_channelCheckGroup = Q_NULLPTR;
};

#endif
