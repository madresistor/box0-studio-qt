/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "func_gen.h"
#include <QMessageBox>
#include <QTimer>
#include <QSettings>
#include "chan/chan.h"
#include "extra/box0/result_code.h"
#include "extra/box0/box0.h"
#include <QDebug>
#include <libbox0/misc/box0-v5/box0-v5.h>
#include <libbox0/misc/box0-v5/calib.h>

/*
 * TODO: let user choose different bitsize for AOUT
 * TODO: user select different waveform for different channel
 * TODO: support Sweep
 */

const static QString GROUP_ID = "func-gen";
const static QString GRAPH_LINE_WIDTH_ID = "graph-line-width";
const static qreal GRAPH_LINE_WIDTH_DEF = 2;

BsFuncGen::BsFuncGen(QWidget *parent) :
	BsInstrument(B0_AOUT, parent)
{
	ui.setupUi(this);

	/* Start/Stop button */
	ui_hw_run = new BsHwRun(this);
	ui.toolBar->addAction(ui_hw_run);
	connect(ui_hw_run, SIGNAL(userWantToStart()), SLOT(start()));
	connect(ui_hw_run, SIGNAL(userWantToStop()), SLOT(stop()));

	/* qtcentre.org/threads/41016 */
	QWidget *spacerHoriz = new QWidget(ui.toolBar);
	QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	spacerHoriz->setSizePolicy(sizePolicy);
	ui.toolBar->addWidget(spacerHoriz);

	/* config */
	ui.dockConfig->setVisible(false);
	QAction *dockLeftAction = ui.dockConfig->toggleViewAction();
	dockLeftAction->setIcon(QIcon(":/dock/right"));
	ui.menuView->addAction(dockLeftAction);
	ui.toolBar->addAction(dockLeftAction);

	/* load settings */
	QSettings s;
	s.beginGroup(GROUP_ID);
	ui.graphLineWidth->setValue(s.value(GRAPH_LINE_WIDTH_ID, GRAPH_LINE_WIDTH_DEF).toReal());

	connect(ui.outputRepeat, SIGNAL(valueChanged(int)), ui_hw_run, SLOT(restart()));
	connect(ui.outputBitsize, SIGNAL(currentIndexChanged(int)), ui_hw_run, SLOT(restart()));
	connect(ui.graphLineWidth, SIGNAL(valueChanged(double)), SLOT(setGraphLineWidth(qreal)));
}

void BsFuncGen::setGraphLineWidth(qreal value)
{
	for (int i = 0; i < channels.size(); i++) {
		channels.at(i)->setLineWidth(value);
	}
}

/**
 * Read calibration from device
 * It check if the device is box0-v5 and read the calibration data for AOUT0
 */
static void read_calib(b0_device *dev, b0_aout *mod,
							QList<BsFuncGenChan *> &chans)
{
	/* reset previous calib */
	for (int i = 0; i < chans.count(); i++) {
		BsFuncGenChan *ch = chans.at(i);
		ch->setCalib(1, 0);
	}

	if (B0_ERR_RC(b0v5_valid_test(dev))) {
		/* not box0-v5 */
		return;
	}

	if (mod->header.index != 0) {
		/* box0-v5 only have AOUT0 calib data */
		return;
	}

	/* read calibration data */
	b0v5_calib_value values[mod->chan_count];
	b0v5_calib calib = {
		/* .ain0 = */ {
			/* .values = */ NULL,
			/* .count = */ 0
		},

		/* .aout0 = */ {
			/* .values = */ values,
			/* .count = */ mod->chan_count
		}
	};

	if (B0_ERR_RC(b0v5_calib_read(dev, &calib))) {
		qWarning("Unable to read box0-v5 AOUT0 calib");
		return;
	}

	for (int i = 0; i < chans.count(); i++) {
		chans.at(i)->setCalib(values[i].gain, values[i].offset);
	}
}

void BsFuncGen::loadModule(b0_device *dev, int index)
{
	b0_result_code r;

	/* search for module to bind to */
	r = b0_aout_open(dev, &module, index);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AOUT"));
		rc.setMessage(tr("Open failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* search for module to bind to */
	r = b0_aout_snapshot_prepare(module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AOUT"));
		rc.setMessage(tr("Prepare failed"));
		rc.exec();
		deleteLater();
		return;
	}

	/* populate bitsize */
	for (size_t i = 0; i < module->snapshot.count; i++) {
		ui.outputBitsize->addValue(module->snapshot.values[i].bitsize);
	}

	ui.outputBitsize->setCurrentIndex(0);

	/* allocate a fresh check group. */
	if (m_channelCheckGroup) {
		m_channelCheckGroup->deleteLater();
	}
	m_channelCheckGroup = new QButtonGroup(this);
	m_channelCheckGroup->setExclusive(true);
	connect(m_channelCheckGroup, SIGNAL(buttonToggled(int,bool)),
		ui_hw_run, SLOT(restart()));

	/* settings */
	QSettings s;
	s.beginGroup(GROUP_ID);
	qreal graphLineWidth =
		s.value(GRAPH_LINE_WIDTH_ID, GRAPH_LINE_WIDTH_DEF).toReal();

	for (size_t i = 0; i < module->chan_count; i++) {
		QString ch_name;
		if (module->label.chan[i] != NULL) {
			ch_name = utf8_to_qstring(module->label.chan[i]);
		} else {
			ch_name = QString("CH %1").arg(i);
		}

		BsFuncGenChan *ch = new BsFuncGenChan(this);
		ch->setTitle(ch_name);
		ch->setModule(module);
		ch->setBitsize(ui.outputBitsize->currentValue());
		ch->setLineWidth(graphLineWidth);
		ui.verticalLayout->addWidget(ch);
		channels.append(ch);

		connect(ui.outputBitsize, SIGNAL(currentValueChanged(uint)),
			ch, SLOT(setBitsize(uint)));

		QAbstractButton *ch_chk = ch->getSelectable();
		ch_chk->setChecked(i == 0);
		m_channelCheckGroup->addButton(ch_chk, i);

		/* only emitted if the channel is selected */
		connect(ch, SIGNAL(performRestart()), ui_hw_run, SLOT(restart()));
	}

	read_calib(dev, module, channels);

	/* TODO: test if module support infinite and count for repetation */

	/* repeat is finite */
	repeatStop.setSingleShot(true);
	connect(&repeatStop, SIGNAL(timeout()), SLOT(stopTimeout()));

	showModuleInTitle(&module->header);
}

void BsFuncGen::start()
{
	b0_result_code r;

	unsigned int chan_num = m_channelCheckGroup->checkedId();
	QWidget *item = ui.verticalLayout->itemAt(chan_num)->widget();
	BsFuncGenChan *chan = dynamic_cast<BsFuncGenChan *>(item);

	/* send a stop [in case the device is running] */
	b0_aout_snapshot_stop(module);

	unsigned long bestSpeed = 0;
	size_t bestSamplesCount = 0;

	if (!chan->snapshotCalc(&bestSamplesCount, &bestSpeed)) {
		qDebug() << "snapshot calc failed";
		return;
	}

	/* read data from curve */
	float *data = new float[bestSamplesCount];
	chan->getSamples(data, bestSamplesCount);

	unsigned long repeat = ui.outputRepeat->value();

	qDebug() << "Repeat:" << repeat;

	r = b0_aout_repeat_set(module, repeat);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Repeat set failed"));
		rc.setMessage(tr("Error occured while configuring repeat mode"));
		rc.exec();
		return;
	}

	unsigned int bitsize = ui.outputBitsize->currentValue();
	r = b0_aout_bitsize_speed_set(module, bitsize, bestSpeed);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Bitsize, Speed set"));
		rc.setMessage(tr("Unable to set bitsize,speed to AOUT module"));
		rc.exec();
		return;
	}

	r = b0_aout_chan_seq_set(module, &chan_num, 1);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Channel sequence set"));
		rc.setMessage(tr("Unable to set channel sequence to AOUT module"));
		rc.exec();
		return;
	}

	/* send data to device  and start*/
	r = b0_aout_snapshot_start_float(module, data, bestSamplesCount);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Start failed"));
		rc.setMessage(tr("Unable to start AOUT module"));
		rc.exec();
		return;
	}

	delete[] data;

	ui_hw_run->setRunning(true);

	/* if this is repeat (for finite time) stop after the waveform is over */
	if (repeat) {
		uint ms = (uint) ceil((repeat * 1000.0 * bestSamplesCount) / bestSpeed);
		qDebug() << "repeat timer duration: " << ms << "ms";
		repeatStop.start(ms);
	}
}

void BsFuncGen::stopTimeout()
{
	if (!ui_hw_run->isRunning()) {
		/* just for safty */
		return;
	}

	ui_hw_run->setRunning(false);
}

void BsFuncGen::stop()
{
	/* stop any timer that will cause stopTimeout */
	repeatStop.stop();

	BsResultCode::handleInternal(
		b0_aout_snapshot_stop(module), "unable to  stop module");

	stopTimeout();
}

void BsFuncGen::closeEvent(QCloseEvent *event)
{
	if (module != NULL) {
		/* if plotting is in running, stop it */
		if (ui_hw_run->isRunning()) {
			stop();
		}

		/* close the module */
		BsResultCode::handleInternal(b0_aout_close(module),
			"unable to close AOUT module");
		module = NULL;
	}

	/* Store settings */
	QSettings s;
	s.beginGroup(GROUP_ID);
	qreal graphLineWidth = ui.graphLineWidth->value();
	s.setValue(GRAPH_LINE_WIDTH_ID, graphLineWidth);

	BsInstrument::closeEvent(event);
}
