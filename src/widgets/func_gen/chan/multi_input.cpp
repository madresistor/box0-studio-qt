/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "multi_input.h"

BsFuncGenChanMultiInput::BsFuncGenChanMultiInput(QWidget *parent) :
	QTabWidget(parent)
{
	addInput(m_standard = new BsFuncGenChanInputStandard(this), "Standard");
	addInput(m_custom = new BsFuncGenChanInputCustom(this), "Custom");

	connect(this, SIGNAL(currentChanged(int)), SIGNAL(dataChanged()));
}

void BsFuncGenChanMultiInput::addInput(BsFuncGenChanInput *input,
			const QString &name)
{
	connect(input, SIGNAL(dataChanged()), SIGNAL(dataChanged()));
	addTab(input, name);
}

void BsFuncGenChanMultiInput::setBitsize(uint bitsize)
{
	m_standard->setBitsize(bitsize);
	m_custom->setBitsize(bitsize);
}

void BsFuncGenChanMultiInput::setModule(b0_aout *module)
{
	m_standard->setModule(module);
	m_custom->setModule(module);
}

BsFuncGenChanInput *BsFuncGenChanMultiInput::currentInput()
{
	QWidget *w = currentWidget();
	return static_cast<BsFuncGenChanInput *>(w);
}

size_t BsFuncGenChanMultiInput::getData(float *x, float *y, size_t count)
{
	return currentInput()->getData(x, y, count);
}

bool BsFuncGenChanMultiInput::snapshotCalc(size_t *sampleCount, unsigned long *speed)
{
	return currentInput()->snapshotCalc(sampleCount, speed);
}
