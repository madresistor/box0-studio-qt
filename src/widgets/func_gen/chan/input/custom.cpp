/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "custom.h"
#include <cmath>
#include <QMessageBox>
#include <QDebug>
#include "custom_help.h"
#include "extra/parser/exception.h"

using namespace std;

BsFuncGenChanInputCustom::BsFuncGenChanInputCustom(QWidget *parent) :
	BsFuncGenChanInput(parent)
{
	ui.setupUi(this);

	m_parser.defineVariable("x", &m_parser_x);
	m_parser.defineVariable("i", &m_parser_i);
	m_parser.defineVariable("FREQ", &m_parser_freq);
	m_parser.defineVariable("COUNT", &m_parser_count);

	connect(ui.freq, SIGNAL(valueChanged(double)), SIGNAL(dataChanged()));
	connect(ui.count, SIGNAL(valueChanged(int)), SIGNAL(dataChanged()));
	connect(ui.update, SIGNAL(clicked()), SIGNAL(dataChanged()));
	connect(ui.equation, SIGNAL(currentIndexChanged(int)),
			SIGNAL(dataChanged()));
	connect(ui.help, SIGNAL(clicked()), SLOT(showHelp()));
}

bool BsFuncGenChanInputCustom::snapshotCalc(size_t *sampleCount, unsigned long *speed)
{
	*sampleCount = (size_t) ui.count->value();
	*speed = ui.freq->value();
	return true;
}

void BsFuncGenChanInputCustom::setBitsize(uint value)
{
	BsFuncGenChanInput::setBitsize(value);
	updateCountMaximum();
}

void BsFuncGenChanInputCustom::setModule(b0_aout *module)
{
	BsFuncGenChanInput::setModule(module);
	updateCountMaximum();
	ui.count->setValue(ui.count->maximum());
}

void BsFuncGenChanInputCustom::updateCountMaximum()
{
	if (m_bitsize == 0 || m_module == NULL) {
		return;
	}

	uint bytes = (uint) ceil(m_bitsize / 8.0);
	uint count = m_module->buffer_size / bytes;
	ui.count->setMaximum(count);
	qDebug() << "max count " << count << " bitsize" << m_bitsize;
}

size_t BsFuncGenChanInputCustom::getData(float *x, float *y, size_t size)
{
	float max_y = +INFINITY, min_y = -INFINITY;
	size_t i, stride = 1;
	size_t count = ui.count->value();

	if (m_module != NULL) {
		min_y = m_module->ref.low;
		max_y = m_module->ref.high;
	}

	m_parser_count = count;
	m_parser_freq = ui.freq->value();

	if (size > count) {
		size = count;
	} else if (size < count) {
		/* reduce size to integer multiple of count */
		stride = (count + size - 1) / size; /* CEIL(count / size) */
		size = count / stride;
	}

	/* http://muparser.beltoforion.de/mup_interface.html#idEvalSimple
	 * defining variable just for value change result in performance
	 * degredation because it reset bytecodes
	 *
	 * WARNING: Not thread safe
	 */
	m_parser.setExpression(ui.equation->currentText());

	try {
		for (i = 0; i < size; i++) {
			qreal tmp = (i / m_parser_freq) * stride;

			if (x != NULL) {
				x[i] = tmp;
			}

			m_parser_i = i * stride;
			m_parser_x = tmp;

			tmp = m_parser.Eval();
			tmp = fmin(tmp, max_y);
			tmp = fmax(tmp, min_y);
			y[i] = tmp;
		}
	} catch (mu::Parser::exception_type &e) {
		size = i;
		BsParserException v(e, this);
		v.exec();
	}

	return size;
}

/**
 * Show a help dialog to user
 */
void BsFuncGenChanInputCustom::showHelp()
{
	QDialog *d = new BsFuncGenChanInputCustomHelp(this);
	d->show();
}
