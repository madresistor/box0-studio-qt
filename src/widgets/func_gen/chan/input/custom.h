/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_FUNC_GEN_CHAN_INPUT_CUSTOM_H
#define BOX0_STUDIO_FUNC_GEN_CHAN_INPUT_CUSTOM_H

#include <QObject>
#include "input.h"
#include "extra/parser/parser.h"

#include "ui_func_gen_chan_input_custom.h"

/**
 * Channel custom equation support.
 * In this, the user enter equation, select the frequency at which the
 *  waveform need to be output and the number of samples to generate.
 * In standard waveform, all these paramters are done by code,
 *  here these are provided by user.
 */

class BsFuncGenChanInputCustom : public BsFuncGenChanInput
{
	Q_OBJECT

	public:
		BsFuncGenChanInputCustom(QWidget *parent = Q_NULLPTR);
		size_t getData(float *x, float *y, size_t size);
		bool snapshotCalc(size_t *sampleCount, unsigned long *speed);

		void setModule(b0_aout *module);
		void setBitsize(uint value);

	private Q_SLOTS:
		void showHelp();

	private:
		Ui::BsFuncGenChanInputCustom ui;

		void updateCountMaximum();

		BsParser m_parser;
		double m_parser_i, m_parser_x, m_parser_count, m_parser_freq;
};

#endif
