/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_FUNC_GEN_CHAN_INPUT_H
#define BOX0_STUDIO_FUNC_GEN_CHAN_INPUT_H

#include <QObject>
#include <QWidget>
#include <libbox0/libbox0.h>

class BsFuncGenChanInput : public QWidget
{
	Q_OBJECT

	public:
		BsFuncGenChanInput(QWidget *parent = Q_NULLPTR);

		/**
		 * @param x X data (can be NULL)
		 * @param y Y data
		 * @param size Maximum samples that can be stored in @a x and @a y
		 * @return @a size if @a size is provided by snapshotCalc() or
		 *   else *may* return number smaller than @a size
		 */
		virtual size_t getData(float *x, float *y, size_t size) = 0;

		/**
		 * @param sampleCount number of samples that will be used to output to device
		 * @param speed speed to set to device
		 * @return false if failed
		 */
		virtual bool snapshotCalc(size_t *sampleCount, unsigned long *speed) = 0;

	public Q_SLOTS:
		void setModule(b0_aout *module);
		void setBitsize(uint bitsize);

	Q_SIGNALS:
		void dataChanged();

	protected:
		b0_aout *m_module = NULL;
		uint8_t m_bitsize = 0;
};

#endif
