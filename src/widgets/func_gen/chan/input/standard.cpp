/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "standard.h"
#include <QDebug>
#include <QMessageBox>

#include "extra/box0/box0.h"

float square(float angle);
float triangular(float angle);
float saw_tooth(float angle);
float noise(float arg);

float square(float angle)
{
	angle = fmodf(angle, (2 * M_PI));

	return (angle >= M_PI) ? -1 : 1;
}

float triangular(float angle)
{
	angle = fmodf(angle, (2 * M_PI));

	if (angle < (M_PI_2)) {
		return angle / M_PI_2;
	} else if (angle < (3 * M_PI_2)) {
		return 2 - (angle / M_PI_2);
	} else {
		return (angle / M_PI_2) - 4;
	}
}

float saw_tooth(float angle)
{
	angle = fmodf(angle, (2 * M_PI));

	if (angle < M_PI) {
		return angle / M_PI;
	} else {
		return (angle / M_PI) - 2;
	}
}

float noise(float arg)
{
	Q_UNUSED(arg);

	/* Ref: http://stackoverflow.com/questions/2704521 */
	float f = (float)rand() / RAND_MAX;
	return -1.0 + f * 2.0;
}

float constant(float arg)
{
	Q_UNUSED(arg);
	return 0;
}

#define FUNC_GEN_CONSTANT 0
#define FUNC_GEN_SINE 1
#define FUNC_GEN_TRIANGULAR 2
#define FUNC_GEN_SAW_TOOTH 3
#define FUNC_GEN_SQUARE 4
#define FUNC_GEN_NOISE 5

BsFuncGenChanInputStandard::BsFuncGenChanInputStandard(QWidget *parent) :
	BsFuncGenChanInput(parent)
{
	ui.setupUi(this);

	QSize iconSize(24, 24);
	ui.waveform->setIconSize(iconSize);

	addWaveform("constant", "Constant");
	addWaveform("sine", "Sine");
	addWaveform("triangular", "Triangular");
	addWaveform("saw_tooth", "Saw Tooth");
	addWaveform("square", "Square");
	addWaveform("noise", "Noise");

	/* enable disable stuff as per waveform type */
	connect(ui.waveform, SIGNAL(currentIndexChanged(int)),
		SLOT(standardWaveformChanged(int)));

	/* signal external entities that data has changed */
	connect(ui.waveform, SIGNAL(currentIndexChanged(int)), SIGNAL(dataChanged()));
	connect(ui.freq, SIGNAL(valueChanged(double)), SIGNAL(dataChanged()));
	connect(ui.amplitude, SIGNAL(valueChanged(double)), SIGNAL(dataChanged()));
	connect(ui.offset, SIGNAL(valueChanged(double)), SIGNAL(dataChanged()));
	connect(ui.phase, SIGNAL(valueChanged(double)), SIGNAL(dataChanged()));

	ui.waveform->setCurrentIndex(FUNC_GEN_SINE);
}

void BsFuncGenChanInputStandard::addWaveform(const char *icon_tag, const QString &title)
{
	QIcon icon(QString(":/func_gen/waveform/%1").arg(icon_tag));
	ui.waveform->addItem(icon, title);
}

size_t BsFuncGenChanInputStandard::getData(float *x, float *y, size_t size)
{
	float amplitude = ui.amplitude->value();
	float frequency = ui.freq->value();
	float offset = ui.offset->value();
	float phase /* Radian */ = ui.phase->value() * ((2 * M_PI) / 360.0);
	float two_pi = 2 * M_PI;

	float min_y = -INFINITY, max_y = +INFINITY;

	if (m_module != NULL) {
		min_y = m_module->ref.low;
		max_y = m_module->ref.high;
	}

	float (*eval)(float arg) = NULL;

	switch(ui.waveform->currentIndex()) {
	case FUNC_GEN_SINE:
		eval = sinf;
	break;
	case FUNC_GEN_CONSTANT:
		eval = constant;
	break;
	case FUNC_GEN_TRIANGULAR:
		eval = triangular;
	break;
	case FUNC_GEN_SAW_TOOTH:
		eval = saw_tooth;
	break;
	case FUNC_GEN_SQUARE:
		eval = square;
	break;
	case FUNC_GEN_NOISE:
		eval = noise;
	break;
	default:
		qDebug() << "unknown waveform selected";
		return 0;
	break;
	}

	for (size_t i = 0; i < size; i++) {
		float tmp = i / (size * frequency);
		if (x != NULL) {
			x[i] = tmp;
		}

		qreal angle = (tmp * frequency * two_pi) + phase;
		tmp = eval(angle);
		tmp *= amplitude;
		tmp += offset;
		tmp = fminf(tmp, max_y);
		tmp = fmaxf(tmp, min_y);

		y[i] = tmp;
	}

	return size;
}

void BsFuncGenChanInputStandard::standardWaveformChanged(int index)
{
	bool frequency = true;
	bool amplitude = true;
	bool offset = true;
	bool phase = true;

	switch (index) {
	case FUNC_GEN_NOISE:
		frequency = false;
		phase = false;
	break;

	case FUNC_GEN_CONSTANT:
		frequency = false;
		phase = false;
		amplitude = false;
	break;

	case FUNC_GEN_SQUARE:
	case FUNC_GEN_SINE:
	case FUNC_GEN_TRIANGULAR:
	case FUNC_GEN_SAW_TOOTH:
	break;

	default:
		qDebug() << "unknown waveform selected";
	return;
	}

	ui.freq->setEnabled(frequency);
	ui.offset->setEnabled(offset);
	ui.amplitude->setEnabled(amplitude);
	ui.phase->setEnabled(phase);
}

bool BsFuncGenChanInputStandard::snapshotCalc(size_t *sampleCount,
					unsigned long *speed)
{
	struct b0_aout::b0_aout_mode_bitsize_speeds::b0_aout_bitsize_speeds
		*bitsize_speeds = NULL, *item;
	for (size_t i = 0; i < m_module->snapshot.count; i++) {
		item = &m_module->snapshot.values[i];
		if (item->bitsize == m_bitsize) {
			bitsize_speeds = item;
			break;
		}
	}

	Q_ASSERT(bitsize_speeds != NULL);

	int wfm = ui.waveform->currentIndex();
	if (wfm == FUNC_GEN_CONSTANT) {
		*sampleCount = 1;
		*speed = bitsize_speeds->speed.values[0];
		return true;
	} else if (wfm == FUNC_GEN_NOISE) {
		uint bytes_per_sample = (m_bitsize + 7) / 8;
		*sampleCount = m_module->buffer_size / bytes_per_sample;
		unsigned long maximum = bitsize_speeds->speed.values[0];
		for (size_t i = 1; i < bitsize_speeds->speed.count; i++) {
			maximum = qMax(maximum, bitsize_speeds->speed.values[i]);
		}
		*speed = maximum;
		return true;
	}

	qreal frequency = ui.freq->value();
	b0_result_code r = b0_aout_snapshot_calc(m_module, frequency, m_bitsize,
			sampleCount, speed);

	if (B0_ERR_RC(r)) {
		QMessageBox::warning(this, tr("Configuration failed"),
			tr("No favourable speed and buffer size configuration found"));
		return false;
	}

	return true;
}

void BsFuncGenChanInputStandard::setModule(b0_aout *module)
{
	BsFuncGenChanInput::setModule(module);

	if (module == NULL) {
		return;
	}

	/* show unit in inputs */
	QString unit = ref_type_to_unit(module->ref.type);
	if (!unit.isEmpty()) {
		unit = unit.prepend(' ');
	}

	ui.offset->setSuffix(unit);
	ui.amplitude->setSuffix(unit);
}
