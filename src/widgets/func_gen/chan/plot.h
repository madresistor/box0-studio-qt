/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_FUNC_GEN_PLOT_H
#define BOX0_STUDIO_FUNC_GEN_PLOT_H

#include <QObject>
#include <QList>
#include "extra/replot/cartesian.h"
#include "multi_input.h"
#include <libbox0/libbox0.h>

class BsFuncGenChanPlot : public Cartesian
{
	Q_OBJECT

	public:
		BsFuncGenChanPlot(QWidget *parent = Q_NULLPTR);
		~BsFuncGenChanPlot();
		void setInput(BsFuncGenChanMultiInput *input);
		void setLineWidth(qreal value);

	public Q_SLOTS:
		void dataChanged();
		void setModule(b0_aout *module);

	protected:
		void initializeGL() Q_DECL_OVERRIDE;
		void paintGL() Q_DECL_OVERRIDE;

	private:
		void updateYAxisUnit();

		float m_curve_width = 1;

		lp_cartesian_curve *m_curve = NULL;
		float *m_array_x = NULL, *m_array_y = NULL;
		size_t m_array_size = 0;
		float m_min_x = 0, m_max_x = 1, m_min_y = 0, m_max_y = 1;
		BsFuncGenChanMultiInput *m_input;

		/** store unit of Y axis */
		QString m_unit;
};

#endif
