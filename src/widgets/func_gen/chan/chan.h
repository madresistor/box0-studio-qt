/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_FUNC_GEN_CHAN_H
#define BOX0_STUDIO_FUNC_GEN_CHAN_H

#include <QWidget>
#include <QObject>
#include <QAbstractButton>
#include <libbox0/libbox0.h>

#include "ui_func_gen_chan.h"

class BsFuncGenChan : public QWidget
{
	Q_OBJECT

	public:
		explicit BsFuncGenChan(QWidget *parent);
		void setLineWidth(qreal value);
		void setTitle(const QString &title);
		void setCalib(qreal gain, qreal offset);

		void getSamples(float *samples, size_t size);
		bool snapshotCalc(size_t *sampleCount, unsigned long *speed);

		QAbstractButton *getSelectable();

	public Q_SLOTS:
		void setModule(b0_aout *module);
		void setBitsize(uint value);

	Q_SIGNALS:
		/* emit'd when underlying data has been changed for the selected channel */
		void performRestart();

	private Q_SLOTS:
		void glueDataChanged();

	private:
		Ui::BsFuncGenChan ui;
		struct {
			/* calib_output = (uncalib_input * gain) - offset; */
			qreal offset, gain;
		} calib;
};

#endif
