/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "chan.h"
#include <QMessageBox>

BsFuncGenChan::BsFuncGenChan(QWidget *parent) :
	QWidget(parent)
{
	ui.setupUi(this);

	/* update graph */
	ui.plot->setInput(ui.input);
	connect(ui.input, SIGNAL(dataChanged()), ui.plot, SLOT(dataChanged()));
	connect(ui.input, SIGNAL(dataChanged()), SLOT(glueDataChanged()));
	ui.plot->dataChanged();
}

/**
 * Set calibration
 * @param gain Gain
 * @param offset Offset
 */
void BsFuncGenChan::setCalib(qreal gain, qreal offset)
{
	this->calib.gain = gain;
	this->calib.offset = offset;
}

/**
 * Set module
 * @param module Module
 */
void BsFuncGenChan::setModule(b0_aout *module)
{
	ui.input->setModule(module);
	ui.plot->setModule(module);
}

/**
 * Set the current bitsize
 * @param value new bitsize
 */
void BsFuncGenChan::setBitsize(uint value)
{
	ui.input->setBitsize(value);
}

void BsFuncGenChan::getSamples(float *samples, size_t size)
{
	ui.input->getData(NULL, samples, size);

	/* Apply calibration */
	for (size_t i = 0; i < size; i++) {
		samples[i] = (samples[i] * this->calib.gain) - this->calib.offset;
	}
}

void BsFuncGenChan::setLineWidth(qreal value)
{
	ui.plot->setLineWidth(value);
}

void BsFuncGenChan::setTitle(const QString &title)
{
	ui.chkEnableChan->setText(title);
}

bool BsFuncGenChan::snapshotCalc(size_t *sampleCount, unsigned long *speed)
{
	return ui.input->snapshotCalc(sampleCount, speed);
}

QAbstractButton *BsFuncGenChan::getSelectable()
{
	return ui.chkEnableChan;
}

void BsFuncGenChan::glueDataChanged()
{
	if (ui.chkEnableChan->isChecked()) {
		Q_EMIT performRestart();
	}
}
