/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "plot.h"
#include <QVariant>
#include <QDebug>

#include "extra/box0/box0.h"

/* number of samples that will be used to show to user */
#define VIEW_SAMPLES 5000

BsFuncGenChanPlot::BsFuncGenChanPlot(QWidget *parent) :
	Cartesian(parent)
{
}

BsFuncGenChanPlot::~BsFuncGenChanPlot()
{
	makeCurrent();
	lp_cartesian_curve_del(m_curve);
	doneCurrent();

	if (m_array_x != NULL) {
		delete[] m_array_x;
	}

	if (m_array_y != NULL) {
		delete[] m_array_y;
	}
}

void BsFuncGenChanPlot::setModule(b0_aout *module)
{
	m_min_y = module->ref.low;
	m_max_y = module->ref.high;
	m_unit = ref_type_to_unit(module->ref.type);

	if (m_axis_left != NULL) {
		makeCurrent();
		lp_cartesian_axis_2float(m_axis_left,
			LP_CARTESIAN_AXIS_VALUE_RANGE, m_min_y, m_max_y);
		updateYAxisUnit();
		doneCurrent();
	}
}

void BsFuncGenChanPlot::setInput(BsFuncGenChanMultiInput *input)
{
	m_input = input;
}

void BsFuncGenChanPlot::dataChanged()
{
	if (m_input == Q_NULLPTR) {
		qDebug() << "m_input is NULL, cannot update graph";
		return;
	}

	if (m_array_x == NULL) {
		m_array_x = new float[VIEW_SAMPLES];
	}

	if (m_array_y == NULL) {
		m_array_y = new float[VIEW_SAMPLES];
	}

	m_array_size = m_input->getData(m_array_x, m_array_y, VIEW_SAMPLES);

	m_min_x = m_array_x[0];
	m_max_x = m_array_x[m_array_size - 1];

	if (m_axis_left != NULL) {
		makeCurrent();
		lp_cartesian_axis_2float(m_axis_bottom,
			LP_CARTESIAN_AXIS_VALUE_RANGE, m_min_x, m_max_x);
		doneCurrent();
	}

	if (m_curve != NULL) {
		makeCurrent();
		lp_cartesian_curve_data(m_curve, LP_CARTESIAN_CURVE_X, 0, GL_FLOAT,
				sizeof(float), m_array_x, m_array_size);

		lp_cartesian_curve_data(m_curve, LP_CARTESIAN_CURVE_Y, 0, GL_FLOAT,
				sizeof(float), m_array_y, m_array_size);
		doneCurrent();
	}

	update();
}

void BsFuncGenChanPlot::initializeGL()
{
	Cartesian::initializeGL();

	updateYAxisUnit();

	lp_cartesian_axis_pointer(m_axis_bottom,
		LP_CARTESIAN_AXIS_VALUE_APPEND, (void *) "s");

	lp_cartesian_axis_enum(m_axis_left,
		LP_CARTESIAN_AXIS_PREFIX_SELECT,
		LP_CARTESIAN_AXIS_PREFIX_SELECT_METRIC);
	lp_cartesian_axis_enum(m_axis_bottom,
		LP_CARTESIAN_AXIS_PREFIX_SELECT,
		LP_CARTESIAN_AXIS_PREFIX_SELECT_METRIC);

	lp_cartesian_axis_2float(m_axis_left,
		LP_CARTESIAN_AXIS_VALUE_RANGE, m_min_y, m_max_y);

	lp_cartesian_axis_2float(m_axis_bottom,
		LP_CARTESIAN_AXIS_VALUE_RANGE, m_min_x, m_max_x);

	m_curve = lp_cartesian_curve_gen();

	lp_cartesian_curve_float(m_curve,
			LP_CARTESIAN_CURVE_LINE_WIDTH, m_curve_width);

	if (m_array_x != NULL) {
		lp_cartesian_curve_data(m_curve, LP_CARTESIAN_CURVE_X, 0, GL_FLOAT,
				sizeof(float), m_array_x, m_array_size);
	}

	if (m_array_y != NULL) {
		lp_cartesian_curve_data(m_curve, LP_CARTESIAN_CURVE_Y, 0, GL_FLOAT,
				sizeof(float), m_array_y, m_array_size);
	}
}

void BsFuncGenChanPlot::paintGL()
{
	Cartesian::paintGL();

	lp_cartesian_draw_start(m_cartesian);
	lp_cartesian_draw_curve(m_cartesian, m_curve);
	lp_cartesian_draw_end(m_cartesian);
}

void BsFuncGenChanPlot::setLineWidth(qreal value)
{
	m_curve_width = value;

	if (m_curve != NULL) {
		makeCurrent();
		lp_cartesian_curve_float(m_curve,
			LP_CARTESIAN_CURVE_LINE_WIDTH, value);
		doneCurrent();
	}

	update();
}

/**
 * Update the Y Axis unit.
 * @note assume makeCurrent() is already called
 */
void BsFuncGenChanPlot::updateYAxisUnit()
{
	/* not allocated till now */
	if (m_axis_left == NULL) {
		return;
	}

	std::string value = m_unit.toStdString();
	lp_cartesian_axis_pointer(m_axis_left,
		LP_CARTESIAN_AXIS_VALUE_APPEND, (void *) value.c_str());
}
