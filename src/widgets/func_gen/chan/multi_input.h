/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_FUNC_GEN_CHAN_MULTI_INPUT_H
#define BOX0_STUDIO_FUNC_GEN_CHAN_MULTI_INPUT_H

#include <QObject>
#include <QTabWidget>
#include "input/input.h"

#include "input/standard.h"
#include "input/custom.h"

class BsFuncGenChanMultiInput : public QTabWidget
{
	Q_OBJECT

	public:
		BsFuncGenChanMultiInput(QWidget *parent);

		size_t getData(float *x, float *y, size_t size);
		bool snapshotCalc(size_t *sampleCount, unsigned long *speed);

	public Q_SLOTS:
		void setBitsize(uint bitsize);
		void setModule(b0_aout *module);

	Q_SIGNALS:
		void dataChanged();

	private:
		void addInput(BsFuncGenChanInput *input, const QString &name);
		BsFuncGenChanInput *currentInput();

		BsFuncGenChanInputStandard *m_standard;
		BsFuncGenChanInputCustom *m_custom;
};

#endif
