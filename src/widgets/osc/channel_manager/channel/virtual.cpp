/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "virtual.h"
#include "widgets/osc/channel_manager/channel_manager.h"
#include "extra/parser/parser.h"
#include <QDebug>

/**
 * Algorithm:
 *
 * function start(entries):
 *  #entries is a list of virtual channel with first item mapping to recently added
 *  for i in entries:
 *    entry.processed = false
 *
 *  for i in entries:
 *    process(i)
 *
 * _____________________________________________________________________________
 * function process(entry):
 *   # if the entry is already processed, ignore
 *   if entry.processed is true:
 *     return
 *
 *   # mark the entry as process to prevent future re-processing
 *   # if circular dependency occur (that we leave user to not do), also prevent re-process
 *   entry.processed = true
 *
 *   # process all dependencies
 *   for i in dependencies(entry):
 *     process(i)
 *
 *   evaulate(entry)
 *
 * function evaluate(entry):
 *    /DO THE EQUATION EVALUATION STUFF/
 */

/**
 * Search for index by using name of the virtual channel id
 * @param id Channel ID
 * @param channels Channel list
 * @return -1 if not found
 * @return index >= 0 if found
 * @thread{sampler}
 */
static int getIndexByID(int id, const QList<BsOscChannelVirtual *> &channels)
{
	for (int i = 0; i < channels.count(); i++) {
		if (channels.at(i)->id == id) {
			return i;
		}
	}

	return -1;
}

static void process(
	BsOscChannelVirtual *channel, BsOscChannelManager *manager,
	BsParser &parser, double *physical_channels, double *virtual_channels);

/**
 * Process all dependencies of the channel
 * @param channel Channel
 * @param manager Channel Manager
 * @param parser Parser
 * @param physical_channels The memory in which physical channel temporary calculation data stored
 * @param virtual_channels The memory in which virtual channel temporary calculation data stored
 * @thread{sampler}
 */
static void process_dependencies(
	BsOscChannelVirtual *channel, BsOscChannelManager *manager,
	BsParser &parser, double *physical_channels, double *virtual_channels)
{
	/* this contain the index of the
	 *  virtual channel dependecies that need processing */
	QList<BsOscChannelVirtual *> dep_virtual_list_need_processing;

	/* set the expression to the parser.
	 * So, that we can read the expression variables
	 */
	parser.setExpression(channel->equation);

	mu::varmap_type vars = parser.GetUsedVar();
	mu::varmap_type::const_iterator item;

	for (item = vars.begin(); item != vars.end(); item++) {
		QString name = QString::fromStdString(item->first);

		/* not a virtual channel name? */
		if (!name.startsWith("M")) {
			continue;
		}

		/* try to get the ID from the name */
		bool ok;
		int id = name.remove(0, 1).toInt(&ok);

		/* just for good faith!!! */
		if (!ok) {
			continue;
		}

		/* get the index of the item by ID */
		int index = getIndexByID(id, manager->m_virtual_channel);

		/* not found! */
		if (index == -1) {
			continue;
		}

		BsOscChannelVirtual *target = manager->m_virtual_channel.at(index);

		/* already processed */
		if (target->processed) {
			continue;
		}

		dep_virtual_list_need_processing.append(target);
	}

	/* now process the un processed dependencies */
	for (int i = 0; i < dep_virtual_list_need_processing.count(); i++) {
		process(dep_virtual_list_need_processing.at(i), manager,
			parser, physical_channels, virtual_channels);
	}
}

/**
 * Try to get any dependency (preferrably a physical channel)
 * @note assume Expression is already set to @a parser.
 * @param manager Channel Manager
 * @param parser Parser containing the equation
 * @return NULL if no dependency found
 * @param channel if dependency found
 * @thread{sampler}
 */
static BsOscChannel *get_dep_preferrably_physical(
	BsOscChannelManager *manager, BsParser &parser)
{
	/* this contain the index of the
	 *  virtual channel dependecies that need processing */
	BsOscChannel *result = Q_NULLPTR;

	mu::varmap_type vars = parser.GetUsedVar();
	mu::varmap_type::const_iterator item;

	for (item = vars.begin(); item != vars.end(); item++) {
		QString name = QString::fromStdString(item->first);

		if (name.startsWith("M")) {
			if (result != Q_NULLPTR) {
				/* we already have a dependency that is virtual */
				continue;
			}

			/* try to get the ID from the name */
			bool ok;
			int id = name.remove(0, 1).toInt(&ok);

			/* just for good faith!!! */
			if (!ok) {
				continue;
			}

			/* get the index of the item by ID */
			int index = getIndexByID(id, manager->m_virtual_channel);

			/* not found! */
			if (index == -1) {
				continue;
			}

			result = manager->m_virtual_channel.at(index);
		} else if (name.startsWith("CH")) {
			/* try to get the ID from the name */
			bool ok;
			int id = name.remove(0, 2).toInt(&ok);

			/* just for good faith!!! */
			if (!ok) {
				continue;
			}

			if (!(id < manager->m_physical_channel.count())) {
				continue;
			}

			/* found one physical channel as dependency */
			result = manager->m_physical_channel.at(id);
			break;
		}
	}

	return result;
}

/**
 * Evaluate the channel values.
 * This function will try to use some other channel to
 *  know the number of samples to update.
 * It is mandatory to have another channel as source OR this function will fail
 *  to process any data for the channel.
 * @param channel Channel
 * @param manager Channel Manager
 * @param physical_channels Parser physical temporary storage
 * @param virtual_channels Parser virtual temporary storage
 * @thread{sampler}
 */
static void evalulate(BsOscChannelVirtual *channel, BsOscChannelManager *manager,
	BsParser &parser, double *physical_channels, double *virtual_channels)
{
	/* this contain the index of the
	 *  virtual channel dependecies that need processing */
	QList<BsOscChannelVirtual *> deps_channels;

	/* set the expression to parser */
	parser.setExpression(channel->equation);

	BsOscChannel *dep = get_dep_preferrably_physical(manager, parser);
	if (dep == Q_NULLPTR) {
		/* equation has no channel in it */
		return;
	}

	size_t index, length, total;

	/* TODO: proper handling for flow plot */
	if ((channel->domain.time.index == 0 && channel->domain.time.valid == 0) &&
		(dep->domain.time.valid == dep->domain.time.total)) {
		/* channel added in between */
		index = dep->domain.time.index;
		length = dep->domain.time.total;
	} else {
		index = channel->domain.time.index;
		if (index < dep->domain.time.index) {
			length = dep->domain.time.index - index;
		} else {
			length = (dep->domain.time.total - index) + dep->domain.time.index;
		}
	}

	total = channel->domain.time.total;

	if (!length) {
		return;
	}

	/* going to update the channel */
	channel->lock();

	for (size_t i = 0; i < length; i++) {
		size_t offset = (index + i) % total;

		/* set all physical data */
		for (int j = 0; j < manager->m_physical_channel.count(); j++) {
			const BsOscChannel *k = manager->m_physical_channel.at(j);
			double value = 0;

			if (k->domain.time.total > 0 && offset < k->domain.time.valid) {
				value = k->domain.time.y[offset];
			}

			physical_channels[j] = value;
		}

		/* set all virtual data */
		for (int j = 0; j < manager->m_virtual_channel.count(); j++) {
			const BsOscChannel *k = manager->m_virtual_channel.at(j);
			double value = 0;

			if (k->domain.time.total > 0 && k->domain.time.valid) {
				value = k->domain.time.y[offset];
			}

			virtual_channels[j] = value;
		}

		/* evaluate the equation */
		/* FIXME: catch exception */
		channel->domain.time.y[offset] = parser.Eval();
	}

	/* update the status */
	channel->domain.time.index = (index + length) % total;
	channel->domain.time.valid = qMin(channel->domain.time.valid + length, total);

	/* we are done updating the channel */
	channel->unlock();
}

/**
 * Process the Channel
 * @param channel Channel
 * @param manager Channel Manager
 * @param physical_channels Parser physical temporary storage
 * @param virtual_channels Parser virtual temporary storage
 * @thread{sampler}
 */
static void process(
	BsOscChannelVirtual *channel, BsOscChannelManager *manager,
	BsParser &parser, double *physical_channels, double *virtual_channels)
{
	/* already processed? */
	if (channel->processed) {
		return;
	}

	/* mark the entry as processed */
	channel->processed = true;

	/* process all the dependencies */
	process_dependencies(channel, manager,
		parser, physical_channels, virtual_channels);

	evalulate(channel, manager, parser, physical_channels, virtual_channels);
}

/**
 * Prepare the parser.
 *  Define all the variables (physical and virtual)
 *  Define all the constants
 * @param manager Channel Manager
 * @param parser Parser
 * @param physical_channels The memory in which physical channel temporary calculation data stored
 * @param virtual_channels The memory in which virtual channel temporary calculation data stored
 * @thread{sampler}
 */
static void prepare_parser(BsOscChannelManager *manager,
	BsParser &parser, double *physical_channels, double *virtual_channels)
{
	/* physical channels */
	for (int i = 0; i < manager->m_physical_channel.count(); i++) {
		QString id = BsOscChannelPhysical::strID(i);
		parser.defineVariable(id, &physical_channels[i]);
	}

	/* virtual channel */
	for (int i = 0; i < manager->m_virtual_channel.count(); i++) {
		QString id = manager->m_virtual_channel.at(i)->strID();
		parser.defineVariable(id, &virtual_channels[i]);
	}
}

/**
 * Mark all virtual channels as unprocessed
 * @param manager Channel Manager
 * @thread{sampler}
 */
static void mark_all_as_unproceesed(BsOscChannelManager *manager)
{
	for (int i = 0; i < manager->m_virtual_channel.count(); i++) {
		manager->m_virtual_channel.at(i)->processed = false;
	}
}

/**
 * Process all entries from last to first
 * @param manager Channel Manager
 * @param parser Parser (shared)
 * @param physical_channels Array to store sample for calculation (physical channel)
 * @param virtual_channels Array to store sample for calculation (virtual channel)
 * @thread{sampler}
 */
static void process_all_in_reverse(BsOscChannelManager *manager,
	BsParser &parser, double *physical_channels, double *virtual_channels)
{
	/* process elements one by one */
	for (int i = manager->m_virtual_channel.count() - 1; i >= 0; i--) {
		BsOscChannelVirtual *channel = manager->m_virtual_channel.at(i);
		process(channel, manager, parser, physical_channels, virtual_channels);
	}
}

/**
 * Process all virtual channel
 * @param manager Channel Manager
 * @thread{sampler}
 */
void bs_osc_process_virtual_channels(BsOscChannelManager *manager)
{
	/* all processing will share the same parser object for now.
	 *  Though there are some changes that we can perform processing in parallel
	 *    (if there are two independent equations)
	 * but to keep design simple, single object (is shared among all objects
	 */
	BsParser parser;

	/* a stack allocated buffer,
	 *  this will be used to store temporary calculation values
	 */
	double physical_channels[manager->m_physical_channel.count()];
	double virtual_channels[manager->m_virtual_channel.count()];

	prepare_parser(manager, parser, physical_channels, virtual_channels);

	mark_all_as_unproceesed(manager);

	process_all_in_reverse(manager, parser, physical_channels, virtual_channels);
}

/* ~~~~~~~~~~~~ snapshot ~~~~~~~~~~~~~~ */
void BsOscChannelVirtual::snapshot_configure(qreal sampling_freq, size_t max_count)
{
	if (max_count > domain.time.total) {
		/* free previous buffer since it is inadequate */
		if (domain.time.x != NULL) free(domain.time.x);
		if (domain.time.y != NULL) free(domain.time.y);

		domain.time.x = (float *) malloc(max_count * sizeof(float));
		domain.time.y = (float *) malloc(max_count * sizeof(float));

		this->sampling_freq = 0; /* force update x values */
	}

	if (this->sampling_freq != sampling_freq) {
		for (uint i = 0; i < max_count; i++) {
			domain.time.x[i] = i / sampling_freq;
		}

		this->sampling_freq = sampling_freq;
	}

	/* invalidate all previous data */
	domain.time.total = max_count;
	domain.time.valid = 0;
	domain.time.index = 0;
}
