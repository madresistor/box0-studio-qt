/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_OSC_CHANNEL_PHYSICAL_H
#define BOX0_STUDIO_OSC_CHANNEL_PHYSICAL_H

#include "channel.h"

class BsOscChannelPhysical : public BsOscChannel
{
	public:
		/* calib_data = (gain * uncalib_data) - offset; */
		struct {
			qreal offset = 0, gain = 1;
		} calib;

		static QString strID(int id) { return QString("CH%1").arg(id); }

		void stream_feedTimeDomainCircular(float *data, size_t count,
			size_t offset, size_t stride);

		void stream_feedTimeDomainFlow(float *data, size_t count,
			size_t offset, size_t stride);

		void stream_feedFreqDomain(float *data, size_t count,
			size_t offset, size_t stride, BsWindowFunc::Type window_func);

		void snapshot_feedTimeDomain(qreal sampling_freq,
			float *data, size_t count, size_t offset, size_t stride);

		void snapshot_feedFreqDomain(qreal sampling_freq,
			float *data, size_t count, size_t offset, size_t stride,
			BsWindowFunc::Type window_func);

		/**
		 * Perform calibration of data
		 * @param data All Data
		 * @param count Number of all samples in @a data
		 * @param offset Start of channel data
		 * @param stride space between channel data
		 */
		void applyCalib(float *data, size_t count, size_t offset, size_t stride);
};

#endif
