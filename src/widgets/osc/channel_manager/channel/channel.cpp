/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "channel.h"
#include <cmath>
#include <QDebug>
#include <complex>

using namespace std;

BsOscChannel::BsOscChannel() : QMutex()
{
}

BsOscChannel::~BsOscChannel()
{
	if (domain.time.x != NULL) free(domain.time.x);
	if (domain.time.y != NULL) free(domain.time.y);

	if (domain.freq.x != NULL) free(domain.freq.x);
	if (domain.freq.y != NULL) free(domain.freq.y);

	if (fft.plan != NULL) fftwf_destroy_plan(fft.plan);
	if (fft.in != NULL) fftwf_free(fft.in);
	if (fft.out != NULL) fftwf_free(fft.out);
}

/**
 * Configure the channel for stream mode
 * @param sampling_freq Frequency at which data will be sampled
 * @param time_domain_total Total number of samples to show in time domain
 * @param freq_domain_total Total number of samples to show in frequency domain
 */
void BsOscChannel::stream_configure(qreal sampling_freq,
	size_t time_domain_total, size_t freq_domain_total)
{
	/* time domain */
	if (domain.time.total < time_domain_total) {
		if (domain.time.x != NULL) free(domain.time.x);
		if (domain.time.y != NULL) free(domain.time.y);

		domain.time.x = (float *) malloc(sizeof(float) * time_domain_total);
		domain.time.y = (float *) malloc(sizeof(float) * time_domain_total);

		/* force update of time data */
		this->sampling_freq = 0;
	}

	if (this->sampling_freq != sampling_freq) {
		for (uint i = 0; i < time_domain_total; i++) {
			domain.time.x[i] = i / sampling_freq;
		}
	}

	domain.time.total = time_domain_total;
	domain.time.valid = 0;
	domain.time.index = 0;

	/* frequency domain */
	if (domain.freq.total != freq_domain_total) {
		if (domain.freq.x != NULL) free(domain.freq.x);
		if (domain.freq.y != NULL) free(domain.freq.y);

		domain.freq.total = freq_domain_total;
		domain.freq.x = (float *) malloc(sizeof(float) * freq_domain_total);
		domain.freq.y = (float *) malloc(sizeof(float) * freq_domain_total);
	}

	domain.freq.valid = 0;

	if (fft.N != (freq_domain_total * 2)) {
		if (fft.in != NULL) fftwf_free(fft.in);
		if (fft.out != NULL) fftwf_free(fft.out);
		if (fft.plan != NULL) fftwf_destroy_plan(fft.plan);

		fft.N = freq_domain_total * 2;
		fft.in = fftwf_alloc_real(fft.N);
		fft.out = fftwf_alloc_complex(fft.N);
		fft.plan = fftwf_plan_dft_r2c_1d(fft.N, fft.in, fft.out,
			/* FFTW_MEASURE  */ FFTW_ESTIMATE);
	}

	fft.in_filled = 0;

	/* common */
	this->sampling_freq = sampling_freq;
}

/**
 * Reset the channel time domain data.
 * @note array are not free'd because they may be reused
 */
void BsOscChannel::resetTimeDomain()
{
	sampling_freq = 0;
	domain.time.index = domain.time.valid = 0;
}

/**
 * Reset the channel frequency domain data.
 * @note array are not free'd because they may be reused
 */
void BsOscChannel::resetFreqDomain()
{
	domain.freq.valid = 0;
	fft.in_filled = 0;
}

/**
 * Perform FFT of the data
 * After performing FFT, it will update domain.freq
 * @note assume that the plan is already prepared and all members in @a fft are valid
 */
#define COMPLEX_ABS_VALUE(data) abs(complex<float>(data[0], data[1]))
void BsOscChannel::performFFT()
{
	fftwf_execute(fft.plan);

	size_t N = fft.N;
	size_t N_2 = N / 2;
	qreal freq_res = sampling_freq / N;
	fftwf_complex *out = fft.out;
	float *x = domain.freq.x;
	float *y = domain.freq.y;

	x[0] = 0;
	y[0] = COMPLEX_ABS_VALUE(out[0]) / N;
	for (size_t k = 1; k < N_2; k++) {
		x[k] = k * freq_res;
		y[k] = COMPLEX_ABS_VALUE(out[k]) / N_2;
	}

	domain.freq.valid = N_2;
}
