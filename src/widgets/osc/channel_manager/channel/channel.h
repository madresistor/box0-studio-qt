/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_OSC_CHANNEL_H
#define BOX0_STUDIO_OSC_CHANNEL_H

#include <QString>
#include <QColor>
#include <QMutex>
#include <stddef.h>
#include <fftw3.h>
#include "extra/widget/window_func.h"

class BsOscChannel : public QMutex {
	public:
		QString title;
		QColor color;
		QString unit;
		bool visible = true;
		qreal line_width = 1;
		qreal sampling_freq = 0;

		struct {
			/* time domain visualization */
			struct {
				size_t valid = 0, index = 0, total = 0;
				float *x = NULL, *y = NULL;
			} time;

			/* frequency domain visualization */
			struct {
				size_t valid = 0, total = 0;
				float *x = NULL, *y = NULL;
			} freq;
		} domain;

		struct {
			fftwf_complex *out = NULL;
			float *in = NULL;
			size_t N = 0, in_filled = 0;
			fftwf_plan plan = NULL;
		} fft;

		BsOscChannel();
		~BsOscChannel();

		void stream_configure(qreal sampling_freq,
			size_t time_domain_total, size_t freq_domain_total);
		void resetTimeDomain();
		void resetFreqDomain();

		void performFFT();
};

#endif
