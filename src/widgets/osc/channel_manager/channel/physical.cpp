/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "physical.h"

/**
 * Feed streaming data to Time domain
 * Copying of data is done in circular manager.
 * Copying start from the first index and goes on till the end of array.
 * At reaching end of array, go back to start of array and copy.
 *  0 -> 1 -> 2 -> ... [N - 1] -> 0 -> 1
 *
 * @param data Pointer to data
 * @param count Number of samples in @a data
 * @param offset Number of samples to ingore from begining
 * @param stride Offset (in samples) to next sample
 */
void BsOscChannelPhysical::stream_feedTimeDomainCircular(float *data, size_t count,
	size_t offset, size_t stride)
{
	size_t index = domain.time.index;
	size_t valid = domain.time.valid;
	size_t total = domain.time.total;
	float *y = domain.time.y;

	for (size_t i = offset; i < count; i += stride) {
		y[index++] = data[i];

		if (index >= total) {
			index = 0;
		}
	}

	/* increase valid limit */
	valid = qMin(valid + (count / stride), total);

	domain.time.index = index;
	domain.time.valid = valid;
	domain.time.total = total;
}

/**
 * Feed streaming data to Time domain
 * Copying of data is done in flow like manner.
 *  The latest data is always at the start and the oldest data is always at the end of array
 *
 * @param data Pointer to data
 * @param count Number of samples in @a data
 * @param offset Number of samples to ingore from begining
 * @param stride Offset (in samples) to next sample
 */
void BsOscChannelPhysical::stream_feedTimeDomainFlow(float *data,
	size_t count, size_t offset, size_t stride)
{
	size_t total = domain.time.total;
	size_t valid = domain.time.valid;
	float *y = domain.time.y;
	size_t size = count / stride;

	memmove(&y[size], y, (total - size) * sizeof(float));

	valid = qMin(valid + size, total);

	/* copy new values */
	for (size_t i = offset; i < count; i += stride) {
		y[--size] = data[i];
	}

	domain.time.valid = valid;
	domain.time.index = 0;
}

/**
 * Feed streaming data to channel
 * Copying is done to a pre-allocated array (allocated at @a stream_prepare())
 * When the array is full, FFT is performed
 * sampling frequency is already known since @a stream_prepare()
 *
 * @param data Pointer to data
 * @param count Number of samples in @a data
 * @param offset Number of samples to ingore from begining
 * @param stride Offset (in samples) to next sample
 * @param window_func Window function to apply before FFT
 */
void BsOscChannelPhysical::stream_feedFreqDomain(float *data, size_t count,
	size_t offset, size_t stride, BsWindowFunc::Type window_func)
{
	size_t N = fft.N;
	size_t in_filled = fft.in_filled;
	float *in = fft.in;

	for (size_t i = offset; i < count; i += stride) {
		qreal wf = BsWindowFunc::calc(window_func, in_filled, N);
		in[in_filled++] = data[i] * wf;
		if (in_filled >= N) {
			performFFT();
			in_filled = 0;
		}
	}

	fft.in_filled = in_filled;
}

/**
 * Feed snapshot data to channel for Frequency domain analysis
 * This method, instead of pre-allocating memory at prepare (like in @a stream_prepare()),
 *   allocate memory as per requirement and try to reuse memory as much as possible.
 *  It keep track of the array maximum size by keeping it in @a domain.time.total
 *   and the valid number of samples in @a domain.time.valid and
 *   pointer to @a domain.time.x and @a domain.time.y
 *
 * @note Aim is to reuse memory as much as possible
 * @param sampling_freq Frequency at data was sampled
 * @param data Pointer to data
 * @param count Number of samples in @a data
 * @param offset Number of samples to ingore from begining
 * @param stride Offset (in samples) to next sample
 */
void BsOscChannelPhysical::snapshot_feedTimeDomain(qreal sampling_freq,
	float *data, size_t count, size_t offset, size_t stride)
{
	size_t actual = (count - offset + (stride - 1)) / stride;

	/* if we do not have enough buffer to store,
	 *  free up the previous buffer and allocate a large buffer
	 */
	if (actual > domain.time.total) {
		this->sampling_freq = 0; /* force update x values */
		if (domain.time.x != NULL) free(domain.time.x);
		if (domain.time.y != NULL) free(domain.time.y);

		domain.time.x = (float *) malloc(actual * sizeof(float));
		domain.time.y = (float *) malloc(actual * sizeof(float));
		domain.time.total = actual;
	}

	if (this->sampling_freq != sampling_freq) {
		for (uint i = 0; i < domain.time.total; i++) {
			domain.time.x[i] = i / sampling_freq;
		}

		this->sampling_freq = sampling_freq;
	}

	size_t j = 0;
	for (size_t i = offset; i < count; i += stride) {
		domain.time.y[j++] = data[i];
	}

	domain.time.valid = j;
	domain.time.index = 0;
}

/**
 * Feed snapshot data to channel for Frequency domain analysis
 * This method, instead of pre-allocating memory at prepare (like streaming),
 *   allocate memory as per requirement and try to reuse memory as much as possible.
 *  It keep track of the array maximum size by keeping it in @a domain.freq.total
 *   and the valid number of samples in @a domain.freq.valid and
 *   pointer to @a domain.freq.x and @a domain.freq.y
 *
 * data is copied to @a fft.in with fft.filled_in containing the number of samples copied
 *
 * This also keep track of the FFT plan.
 *  If the size of the array has changed, the FFT plan is updated accordingly.
 *
 * @param sampling_freq Frequency at data was sampled
 * @param data Pointer to data
 * @param count Number of samples in @a data
 * @param offset Number of samples to ingore from begining
 * @param stride Offset (in samples) to next sample
 * @param window_func Window function apply on data before FFT
 */
void BsOscChannelPhysical::snapshot_feedFreqDomain(qreal sampling_freq,
	float *data, size_t count, size_t offset, size_t stride,
	BsWindowFunc::Type window_func)
{
	size_t actual = (count - offset + (stride - 1)) / stride;
	size_t actual_half = actual / 2;

	if (actual_half > domain.freq.total) {
		if (domain.freq.x != NULL) free(domain.freq.x);
		if (domain.freq.y != NULL) free(domain.freq.y);

		domain.freq.total = actual_half;
		domain.freq.x = (float *) malloc(sizeof(float) * actual_half);
		domain.freq.y = (float *) malloc(sizeof(float) * actual_half);
	}

	if (fft.N != actual) {
		if (fft.in != NULL) fftwf_free(fft.in);
		if (fft.out != NULL) fftwf_free(fft.out);
		if (fft.plan != NULL) fftwf_destroy_plan(fft.plan);

		fft.N = actual;
		fft.in = fftwf_alloc_real(fft.N);
		fft.out = fftwf_alloc_complex(fft.N);
		fft.plan = fftwf_plan_dft_r2c_1d(fft.N, fft.in, fft.out,
			/* FFTW_MEASURE  */ FFTW_ESTIMATE);
	}

	domain.freq.valid = 0;
	fft.in_filled = 0;

	size_t N = fft.N;
	float *in = fft.in;
	size_t in_filled = 0;

	for (size_t i = offset; i < count; i += stride) {
		qreal wf = BsWindowFunc::calc(window_func, in_filled, N);
		in[in_filled++] = data[i] * wf;
	}

	fft.in_filled = in_filled;
	this->sampling_freq = sampling_freq;
	performFFT();
}

void BsOscChannelPhysical::applyCalib(float *data, size_t count,
					size_t offset, size_t stride)
{
	for (size_t i = offset; i < count; i += stride) {
		data[i] = (data[i] * calib.gain) - calib.offset;
	}
}
