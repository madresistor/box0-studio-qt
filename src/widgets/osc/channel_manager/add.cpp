/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "add.h"
#include <QMenu>
#include "extra/parser/operator.h"
#include "extra/parser/function.h"
#include "extra/parser/constant.h"
#include "extra/parser/menu.h"
#include <QDebug>

/**
 * Virtual Channel Add/edit functionality */
BsOscChannelManagerAdd::BsOscChannelManagerAdd(
	BsOscChannelManager *channelManager, QWidget *parent) :
	QDialog(parent)
{
	ui.setupUi(this);

	BsParserFunction *func = new BsParserFunction(this);
	ui.addFunction->setMenu(func);
	connect(func, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsFunction(const QString &)));

	BsParserOperator *oper = new BsParserOperator(this);
	ui.addOperator->setMenu(oper);
	connect(oper, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsOperator(const QString &)));

	BsParserConstant *constant = new BsParserConstant(this);
	ui.addConstant->setMenu(constant);
	connect(constant, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsConstant(const QString &)));

	setupAddChannel(channelManager);
}

/** Physical channel edit functionality */
BsOscChannelManagerAdd::BsOscChannelManagerAdd(QWidget *parent) :
	QDialog(parent)
{
	ui.setupUi(this);

	ui.unit->setEnabled(false);
	ui.equationGroup->setVisible(false);

	QSize s = size();
	s.setHeight(120);
	resize(s);
}

void BsOscChannelManagerAdd::setupAddChannel(BsOscChannelManager *channelManager)
{
	BsParserMenu *menu = new BsParserMenu(this);

	/* hardware channel */
	for (int i = 0; i < channelManager->m_physical_channel.count(); i++) {
		const BsOscChannelPhysical *channel =
			channelManager->m_physical_channel.at(i);
		QString id = BsOscChannelPhysical::strID(i);
		menu->addEntry(id, channel->title);

		m_parser.defineVariable(id, &m_parser_input);
	}

	if (channelManager->m_physical_channel.count() > 0) {
		menu->addSeparator();
	}

	/* math channel */
	for (int i = 0; i < channelManager->m_virtual_channel.count(); i++) {
		const BsOscChannelVirtual *channel =
			channelManager->m_virtual_channel.at(i);

		QString label = channel->title;
		if (label.isEmpty()) {
			/* fallback: use equation if name not given */
			label = channel->equation;
		}
		QString id = channel->strID();
		menu->addEntry(id, channel->title);

		m_parser.defineVariable(id, &m_parser_input);
	}

	ui.addChannel->setMenu(menu);

	connect(menu, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsVariable(const QString &)));
}

void BsOscChannelManagerAdd::setColor(QColor color)
{
	ui.color->setColor(color);
}

QColor BsOscChannelManagerAdd::color()
{
	return ui.color->color();
}

void BsOscChannelManagerAdd::setEquation(const QString &value)
{
	ui.equation->setText(value);
}

QString BsOscChannelManagerAdd::equation()
{
	return ui.equation->text();
}

void BsOscChannelManagerAdd::setTitle(const QString &title)
{
	ui.title->setText(title);
}

QString BsOscChannelManagerAdd::title()
{
	return ui.title->text();
}

void BsOscChannelManagerAdd::setUnit(const QString &unit)
{
	ui.unit->setText(unit);
}

QString BsOscChannelManagerAdd::unit()
{
	return ui.unit->text();
}

/**
 * Done when user want to close the dialog.
 * If equation input is valid, then equation is validated.
 * if the equation is invalid, the dialog is not closed.
 * @param r Result
 */
void BsOscChannelManagerAdd::done(int r)
{
	if (r == QDialog::Accepted && ui.equationGroup->isVisible()) {
		/* confirm that the equation is valid */
		if (!equationValid()) {
			return;
		}
	}

	QDialog::done(r);
}

/**
 * Validate the equation and return the result
 * @return true on success
 */
bool BsOscChannelManagerAdd::equationValid()
{
	m_parser.setExpression(ui.equation->text());

	try {
		m_parser.Eval();
	} catch (mu::Parser::exception_type &e) {
		QString msg = QString::fromStdString(e.GetMsg());
		ui.msg->setText(msg);
		return false;
	}

	return true;
}

/**
 * When the dialog is shown, equation input is focused
 * If equation input is not being, go for the title input
 * @param event Show event
 */
/* http://stackoverflow.com/a/528902/1500988 */
void BsOscChannelManagerAdd::showEvent(QShowEvent *event)
{
	QDialog::showEvent(event);

	if (ui.equationGroup->isVisible()) {
		ui.equation->setFocus(Qt::OtherFocusReason);
	} else {
		ui.title->setFocus(Qt::OtherFocusReason);
	}
}
