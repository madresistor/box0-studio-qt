/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_OSC_CHANNEL_MANAGER_H
#define BOX0_STUDIO_OSC_CHANNEL_MANAGER_H

#include <QWidget>
#include <QString>
#include <QColor>
#include "extra/widget/window_func.h"
#include "extra/color_gen/color_gen.h"
#include <libbox0/libbox0.h>
#include "channel/channel.h"
#include "channel/physical.h"
#include "channel/virtual.h"

#include "ui_osc_channel_manager.h"

class BsOscChannelManagerModel;

class BsOscChannelManager : public QWidget
{
	Q_OBJECT

	public:
		BsOscChannelManager(QWidget *parent = Q_NULLPTR);

		void applyCalib(float *data, size_t count);

		void stream_prepare(qreal speed, const QList<uint> &chan_seq);
		void stream_process(float *data, size_t count, bool fft);
		void stream_done();

		void snapshot_prepare(const QList<uint> &chan_seq);
		void snapshot_process(qreal speed, float *data, size_t count, bool fft);
		void snapshot_done();

		void setModule(b0_ain *module);

		uint stream_timeWindowSize();
		uint stream_fftUpdateRate();

		void addVirtualChannel(const QString &title, QColor color,
			const QString &unit, const QString &equation);

		QList<unsigned int> channelSequence();

		QList<BsOscChannelPhysical *> m_physical_channel;
		QList<BsOscChannelVirtual *> m_virtual_channel;

	public Q_SLOTS:
		void setWindowFunc(BsWindowFunc::Type value);
		void addVirtualChannel();
		void editChannel();
		void removeChannel();
		void setFlowPlot(bool flow_plot);
		void setLineWidth(double line_width);

	Q_SIGNALS:
		void physicalChannelDataChanged();
		void virtualChannelDataChanged();
		void channelDataChanged();

		/* emitted when a physical channel has been checked or unchecked */
		void channelSequenceChanged();

	private Q_SLOTS:
		void channelSelectionChanged();

	private:
		void editPhysicalChannel(int index);
		void editVirtualChannel(int index);

		Ui::BsOscChannelManager ui;
		BsOscChannelManagerModel *m_model;

		bool m_prepared = false;
		bool m_acquisition_stream;
		qreal m_sampling_freq_per_chan;
		size_t m_time_domain_total, m_freq_domain_total;
		int m_id_counter_virtual = 0;
		QList<uint> m_chan_seq;
		bool m_flowPlot = false;
		BsColorGen m_color_gen;
		BsWindowFunc::Type m_windowFunc = BsWindowFunc::Rectangular;
		qreal m_line_width = 1;
};

#endif
