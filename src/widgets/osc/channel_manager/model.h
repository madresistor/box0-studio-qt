/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_OSC_CHANNEL_MANAGER_MODEL_H
#define BOX0_STUDIO_OSC_CHANNEL_MANAGER_MODEL_H

#include <QObject>
#include <QAbstractItemModel>
#include <QAbstractItemView>

#include "channel_manager.h"

class BsOscChannelManagerModel : public QAbstractItemModel
{
	Q_OBJECT

	public:
		BsOscChannelManagerModel(
			BsOscChannelManager *channelManager, QObject *parent = Q_NULLPTR);

		int columnCount(const QModelIndex &parent = QModelIndex()) const;
		int rowCount(const QModelIndex &parent = QModelIndex()) const;
		QVariant data(const QModelIndex &index, int role) const;
		Qt::ItemFlags flags(const QModelIndex &index) const;
		QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
		QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
		QModelIndex parent(const QModelIndex & index) const;
		bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

	public Q_SLOTS:
		void fakeReset();

	private:
		BsOscChannelManager *m_channelManager;
};

#endif
