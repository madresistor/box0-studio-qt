/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "channel_manager.h"
#include "model.h"
#include "add.h"
#include <QDebug>

#include "extra/box0/box0.h"
#include "extra/multi_proc.h"

/* number of seconds data to show */
#define TIME_DOMAIN_WINDOW_SIZE 3 /* unit: seconds */

/* number of update to perform in 1 seconds */
#define FREQ_DOMAIN_UPDATE_RATE 4 /* unit: Hz */

uint BsOscChannelManager::stream_timeWindowSize()
{
	return TIME_DOMAIN_WINDOW_SIZE;
}

uint BsOscChannelManager::stream_fftUpdateRate()
{
	return FREQ_DOMAIN_UPDATE_RATE;
}

BsOscChannelManager::BsOscChannelManager(QWidget *parent) :
	QWidget(parent)
{
	ui.setupUi(this);

	m_model = new BsOscChannelManagerModel(this, ui.entries);
	ui.entries->setModel(m_model);

	/* user able to add virtual channel */
	connect(ui.add, SIGNAL(clicked()), SLOT(addVirtualChannel()));

	/* user can edit channel by clicking edit button */
	connect(ui.edit, SIGNAL(clicked()), SLOT(editChannel()));

	/* user can edit channel by double clicking it */
	connect(ui.entries, SIGNAL(doubleClicked(const QModelIndex &)),
		SLOT(editChannel()));

	/* user can remove channel by clicking remove button */
	connect(ui.remove, SIGNAL(clicked()), SLOT(removeChannel()));

	/* route channel data change to generic signal */
	connect(this, SIGNAL(physicalChannelDataChanged()), SIGNAL(channelDataChanged()));

	/* route channel data change to generic signal */
	connect(this, SIGNAL(virtualChannelDataChanged()), SIGNAL(channelDataChanged()));

	/* enable/disable "remove" as per channel */
	connect(ui.entries->selectionModel(),
		SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
		SLOT(channelSelectionChanged()));
}

/**
 * Add a virtual channel
 * A new ID will be generated for the virtual channel
 * @param title Title of the channel
 * @param color Color of the channel
 * @param unit Unit of channel
 * @param equation Equation that will be used to generate data (from other channels)
 */
void BsOscChannelManager::addVirtualChannel(const QString &title,
	QColor color, const QString &unit, const QString &equation)
{
	BsOscChannelVirtual *chan = new BsOscChannelVirtual();

	chan->id = m_id_counter_virtual++;
	chan->title = title;
	chan->color = color;
	chan->unit = unit;
	chan->equation = equation;
	chan->line_width = m_line_width;

	if (m_prepared && m_acquisition_stream) {
		chan->stream_configure(m_sampling_freq_per_chan,
				m_time_domain_total, m_freq_domain_total);
	}

	m_virtual_channel.append(chan);

	m_model->fakeReset();
	Q_EMIT virtualChannelDataChanged();
}

/**
 * Set the window function that will applied on data before performing FFT
 * @param value Window Function
 * @see https://en.wikipedia.org/wiki/Window_function
 */
void BsOscChannelManager::setWindowFunc(BsWindowFunc::Type value)
{
	m_windowFunc = value;
}

void BsOscChannelManager::applyCalib(float *data, size_t count)
{
	size_t chan_seq_count = (size_t) m_chan_seq.count();

	MULTI_PROC_FOR_LOOP
	for (size_t i = 0; i < chan_seq_count; i++) {
		uint ch = m_chan_seq.at(i);
		m_physical_channel.at(ch)->applyCalib(data, count, i, chan_seq_count);
	}
}

/**
 * Prepare for stream mode
 * @param sampling_freq Sampling frequency
 * @param chan_seq Channel Sequence
 */
void BsOscChannelManager::stream_prepare(qreal sampling_freq,
		const QList<uint> &chan_seq)
{
	qreal sampling_freq_per_chan = sampling_freq / chan_seq.count();
	size_t time_domain_size = ceil(sampling_freq_per_chan * TIME_DOMAIN_WINDOW_SIZE);

	/* here `freq_domain_size` is made power of 2, because it is effecient to perform
	 *   fft of array size of power 2 */
	qreal _freq_domain_size = sampling_freq_per_chan / FREQ_DOMAIN_UPDATE_RATE;
	size_t freq_domain_size = pow(2, ceil(log2(_freq_domain_size)) - 1);

	for (int i = 0; i < m_physical_channel.count(); i++) {
		if (chan_seq.indexOf(i) == -1) {
			/* channel not present in channel sequence */
			m_physical_channel.at(i)->resetTimeDomain();
			m_physical_channel.at(i)->resetFreqDomain();
		} else {
			/* channel present in channel sequence */
			m_physical_channel.at(i)->stream_configure(
				sampling_freq_per_chan, time_domain_size, freq_domain_size);
		}
	}

	for (int i = 0; i < m_virtual_channel.count(); i++) {
		m_virtual_channel.at(i)->stream_configure(sampling_freq_per_chan,
			time_domain_size, freq_domain_size);
	}

	m_prepared = true;
	m_acquisition_stream = true;
	m_chan_seq = chan_seq;
	m_sampling_freq_per_chan = sampling_freq_per_chan;
	m_time_domain_total = time_domain_size;
	m_freq_domain_total = freq_domain_size;
}

/**
 * Process data sampled in stream mode
 * @param data pointer to Data
 * @param count number of samples in @a data
 * @param fft Perform FFT
 * @thread{sampler}
 */
void BsOscChannelManager::stream_process(float *data, size_t count, bool fft)
{
	size_t chan_seq_len = m_chan_seq.count();

	MULTI_PROC_FOR_LOOP
	for (size_t i = 0; i < chan_seq_len; i++) {
		uint j = m_chan_seq.at(i);

		BsOscChannelPhysical *chan = m_physical_channel.at(j);

		chan->lock();
		if (m_flowPlot) {
			chan->stream_feedTimeDomainFlow(data, count, i, chan_seq_len);
		} else {
			chan->stream_feedTimeDomainCircular(data, count, i, chan_seq_len);
		}
		chan->unlock();
	}

	/* perform FFT (without OpenMP) since FFTW can# internally use OMP.
	 *  # = if Box0 Studio compiled with OpenMP */
	for (size_t i = 0; i < chan_seq_len; i++) {
		uint j = m_chan_seq.at(i);
		BsOscChannelPhysical *chan = m_physical_channel.at(j);

		chan->lock();
		if (fft) {
			chan->stream_feedFreqDomain(data, count, i, chan_seq_len, m_windowFunc);
		} else {
			chan->resetFreqDomain();
		}
		chan->unlock();
	}

	/* process virtual channels */
	bs_osc_process_virtual_channels(this);
}

void BsOscChannelManager::stream_done()
{
	m_prepared = false;
}

/**
 * Prepare for snapshot mode
 * @param chan_seq Channel Sequence
 */
void BsOscChannelManager::snapshot_prepare(const QList<uint> &chan_seq)
{
	for (int i = 0; i < m_physical_channel.count(); i++) {
		m_physical_channel.at(i)->resetTimeDomain();
		m_physical_channel.at(i)->resetFreqDomain();
	}

	for (int i = 0; i < m_virtual_channel.count(); i++) {
		m_virtual_channel.at(i)->resetTimeDomain();
		m_virtual_channel.at(i)->resetFreqDomain();
	}

	m_acquisition_stream = false;
	m_chan_seq = chan_seq;
}

/**
 * Process the data, sampled in snapshot mode
 * @param sampling_freq Sampling frequency
 * @param data Pointer to data
 * @param count Number of samples in @a data
 * @param fft Perform FFT
 * @thread{sampler}
 */
void BsOscChannelManager::snapshot_process(qreal sampling_freq, float *data,
		size_t count, bool fft)
{
	size_t chan_seq_len = m_chan_seq.count();
	qreal sampling_freq_per_ch = sampling_freq / chan_seq_len;

	/* time domain */
	MULTI_PROC_FOR_LOOP
	for (size_t i = 0; i < chan_seq_len; i++) {
		uint j = m_chan_seq.at(i);
		BsOscChannelPhysical *chan = m_physical_channel.at(j);

		chan->lock();
		chan->snapshot_feedTimeDomain(sampling_freq_per_ch,
			data, count, i, chan_seq_len);
		chan->unlock();
	}

	/* freq domain */
	for (size_t i = 0; i < chan_seq_len; i++) {
		uint j = m_chan_seq.at(i);
		BsOscChannelPhysical *chan = m_physical_channel.at(j);

		chan->lock();
		if (fft) {
			chan->snapshot_feedFreqDomain(sampling_freq_per_ch,
				data, count, i, chan_seq_len, m_windowFunc);
		} else {
			chan->resetFreqDomain();
		}
		chan->unlock();
	}

	size_t max_count_per_channel = count / chan_seq_len;

	/* since all data is updated every time in snapshot mode,
	 *  here we reset virtual channels index to force calculation of all data
	 */
	for (int i = 0; i < m_virtual_channel.count(); i++) {
		BsOscChannelVirtual *chan = m_virtual_channel.at(i);
		chan->snapshot_configure(sampling_freq_per_ch, max_count_per_channel);
	}

	/* process virtual channels */
	bs_osc_process_virtual_channels(this);
}

void BsOscChannelManager::snapshot_done()
{
	m_prepared = false;
}

/**
 * Build physical channel list
 * All information of the channel is extracted from the @a module
 * @param module Module
 */
void BsOscChannelManager::setModule(b0_ain *module)
{
	qDeleteAll(m_physical_channel);
	m_physical_channel.clear();

	if (module != NULL) {
		QString unit = ref_type_to_unit(module->ref.type);

		for (uint i = 0; i < module->chan_count; i++) {
			QString title;

			if (module->label.chan[i] != NULL) {
				title = utf8_to_qstring(module->label.chan[i]);
			} else {
				title = QString::number(i);
			}

			BsOscChannelPhysical *chan = new BsOscChannelPhysical();
			chan->title = title;
			chan->unit = unit;
			chan->color = m_color_gen.color();
			chan->visible = (i == 0);
			chan->line_width = m_line_width;
			m_physical_channel.append(chan);
		}
	}

	m_model->fakeReset();
	Q_EMIT physicalChannelDataChanged();
}

/**
 * Add a new virtual channel
 * A dialog is shown to user, if accepted a new virtual channel is added
 */
void BsOscChannelManager::addVirtualChannel()
{
	BsOscChannelManagerAdd add(this, this);

	add.setColor(m_color_gen.color());

	if (add.exec() == QDialog::Accepted) {
		QString equation = add.equation();
		QString title = add.title();
		QString unit = add.unit();
		QColor color = add.color();

		addVirtualChannel(title, color, unit, equation);
	}
}
/**
 * Edit a virtual channel.
 * @param index Index of the physical channel in m_virtual_channel list
 */
void BsOscChannelManager::editVirtualChannel(int index)
{
	BsOscChannelVirtual *channel = m_virtual_channel.at(index);
	QString strID = channel->strID();

	BsOscChannelManagerAdd edit(this, this);
	edit.setWindowTitle(QString("Edit: %1").arg(strID));

	edit.setEquation(channel->equation);
	edit.setTitle(channel->title);
	edit.setUnit(channel->unit);
	edit.setColor(channel->color);

	if (edit.exec() != QDialog::Accepted) {
		return;
	}

	QString new_eq = edit.equation();
	bool eq_changed = channel->equation != new_eq;
	channel->equation = new_eq;
	channel->title = edit.title();
	channel->unit = edit.unit();
	channel->color = edit.color();

	if (eq_changed) {
		/* force re-evaluation of all data, since equation has changed */
		channel->domain.time.valid = channel->domain.time.index = 0;
	}

	m_model->fakeReset();
	Q_EMIT virtualChannelDataChanged();
}

/**
 * Edit a physical channel.
 * Edit dialog is shown to user with unit disable and equation hidden
 * @param index Index of the physical channel in m_physical_channel list (also ID)
 */
void BsOscChannelManager::editPhysicalChannel(int index)
{
	BsOscChannelPhysical *channel = m_physical_channel.at(index);
	QString strID = BsOscChannelPhysical::strID(index);

	BsOscChannelManagerAdd edit(this);
	edit.setWindowTitle(QString("Edit: %1").arg(strID));

	edit.setTitle(channel->title);
	edit.setUnit(channel->unit);
	edit.setColor(channel->color);

	if (edit.exec() != QDialog::Accepted) {
		return;
	}

	channel->title = edit.title();
	channel->color = edit.color();

	m_model->fakeReset();
	Q_EMIT physicalChannelDataChanged();
}

/**
 * slot called when user want to edit the channel
 */
void BsOscChannelManager::editChannel()
{
	int row = ui.entries->selectedRow();
	if (row == -1) {
		return;
	}

	int physical_channels = m_physical_channel.count();
	int virtual_channels = m_virtual_channel.count();

	if (row < physical_channels) {
		editPhysicalChannel(row);
	} else {
		row -= physical_channels;
		if (row < virtual_channels) {
			editVirtualChannel(row);
		}
	}
}

/**
 * slot called when user want to remove the channel
 */
void BsOscChannelManager::removeChannel()
{
	int row = ui.entries->selectedRow();
	if (row == -1) {
		return;
	}

	int next_select_row = qMax(0, row - 1);
	int physical_channels = m_physical_channel.count();
	int virtual_channels = m_virtual_channel.count();

	if (row < physical_channels) {
		/* physical channels cannot be removed. */
		return;
	}

	row -= physical_channels;
	if (row >= virtual_channels) {
		/* unknown index */
		return;
	}

	m_virtual_channel.removeAt(row);
	m_model->fakeReset();
	Q_EMIT virtualChannelDataChanged();
	ui.entries->selectRow(next_select_row);
}

void BsOscChannelManager::setLineWidth(double value)
{
	for (int i = 0; i < m_physical_channel.count(); i++) {
		m_physical_channel.at(i)->line_width = value;
	}

	for (int i = 0; i < m_virtual_channel.count(); i++) {
		m_virtual_channel.at(i)->line_width = value;
	}

	m_line_width = value;

	m_model->fakeReset();
	Q_EMIT virtualChannelDataChanged();
}

void BsOscChannelManager::setFlowPlot(bool flow_plot)
{
	m_flowPlot = flow_plot;
}

/**
 * slot called when selected channel has changed.
 * If the channel is physical, the remove button is disabled to notify
 *  user that physical channels cannot be deleted.
 */
void BsOscChannelManager::channelSelectionChanged()
{
	bool enabled = ui.entries->selectedRow() >= m_physical_channel.count();
	ui.remove->setEnabled(enabled);
}

QList<unsigned int> BsOscChannelManager::channelSequence()
{
	QList<unsigned int> channelSequence;
	for (int i = 0; i < m_physical_channel.count(); i++) {
		if (m_physical_channel.at(i)->visible) {
			channelSequence.append(i);
		}
	}

	return channelSequence;
}
