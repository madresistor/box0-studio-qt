/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "model.h"
#include <QDebug>

BsOscChannelManagerModel::BsOscChannelManagerModel(
	BsOscChannelManager *channelManager, QObject *parent) :
	QAbstractItemModel(parent),
	m_channelManager(channelManager)
{

}

int BsOscChannelManagerModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return 2;
}

int BsOscChannelManagerModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	int physical_channels = m_channelManager->m_physical_channel.count();
	int virtual_channels = m_channelManager->m_virtual_channel.count();
	return physical_channels + virtual_channels;
}

static QVariant physical_data(BsOscChannelManager *channelManager,
	int r, int c, int role)
{
	const BsOscChannelPhysical *channel =
		channelManager->m_physical_channel.at(r);

	if (role == Qt::ForegroundRole) {
		return channel->color;
	}

	switch (c) {
	case 0:
		if (role == Qt::DisplayRole) {
			return BsOscChannelPhysical::strID(r);
		} else if (role == Qt::CheckStateRole) {
			return QVariant(channel->visible ? Qt::Checked : Qt::Unchecked);
		}
	break;
	case 1:
		if (role == Qt::DisplayRole) {
			return channel->title;
		}
	break;
	}

	return QVariant();
}

static QVariant virtual_data(BsOscChannelManager *channelManager,
	int r, int c, int role)
{
	const BsOscChannelVirtual *channel =
		channelManager->m_virtual_channel.at(r);

	if (role == Qt::ForegroundRole) {
		return channel->color;
	}

	switch (c) {
	case 0:
		if (role == Qt::DisplayRole) {
			return channel->strID();
		} else if (role == Qt::CheckStateRole) {
			return QVariant(channel->visible ? Qt::Checked : Qt::Unchecked);
		}
	break;
	case 1:
		if (role == Qt::DisplayRole) {
			if (channel->title.isEmpty()) {
				return channel->equation;
			}
			return channel->title;
		}
	break;
	}

	return QVariant();
}

QVariant BsOscChannelManagerModel::data(const QModelIndex &index, int role) const
{
	if (! index.isValid()) {
		return QVariant();
	}

	int physical_channels = m_channelManager->m_physical_channel.count();
	int virtual_channels = m_channelManager->m_virtual_channel.count();
	int r = index.row();
	int c = index.column();

	if (r < physical_channels) {
		return physical_data(m_channelManager, r, c, role);
	} else {
		r -= physical_channels;
		if (r < virtual_channels) {
			return virtual_data(m_channelManager, r, c, role);
		}
	}

	return QVariant();
}


bool BsOscChannelManagerModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (!index.isValid()) {
		return false;
	}

	if (index.column() != 0) {
		return false;
	}

	int physical_channels = m_channelManager->m_physical_channel.count();
	int virtual_channels = m_channelManager->m_virtual_channel.count();
	int r = index.row();

	BsOscChannel *channel;
	bool type_physical = true;

	if (r < physical_channels) {
		channel = m_channelManager->m_physical_channel.at(r);
	} else {
		r -= physical_channels;
		if (r < virtual_channels) {
			type_physical = false;
			channel = m_channelManager->m_virtual_channel.at(r);
		} else {
			return false;
		}
	}

	if (role == Qt::CheckStateRole) {
		channel->visible = value.toBool();

		if (type_physical) {
			Q_EMIT m_channelManager->physicalChannelDataChanged();
			Q_EMIT m_channelManager->channelSequenceChanged();
		} else {
			Q_EMIT m_channelManager->virtualChannelDataChanged();
		}

		QVector<int> roles;
		roles << role;
		dataChanged(index, index, roles);

		return true;
	}

	return false;
}

Qt::ItemFlags BsOscChannelManagerModel::flags(const QModelIndex &index) const
{
	Q_UNUSED(index);
	Qt::ItemFlags flags = Qt::ItemIsSelectable | Qt::ItemIsEnabled;

	/* checkbox in column 0 only */
	if (index.column() == 0) {
		flags |= Qt::ItemIsUserCheckable;
	}

	return flags;
}

QVariant BsOscChannelManagerModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole && orientation == Qt::Vertical) {
		return QVariant(section + 1);
	}

	return QVariant();
}

QModelIndex BsOscChannelManagerModel::index(int row, int column, const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	Q_ASSERT(column == 0);
	return createIndex(row, column, (void *) 0);
}

QModelIndex BsOscChannelManagerModel::parent(const QModelIndex & index) const
{
	Q_UNUSED(index);
	return QModelIndex();
}

void BsOscChannelManagerModel::fakeReset()
{
	beginResetModel();
	endResetModel();
}
