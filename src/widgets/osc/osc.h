/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_OSC_H
#define BOX0_STUDIO_OSC_H

#include "widgets/instrument/instrument.h"
#include <QWidget>
#include <QAction>
#include <QIcon>
#include <libbox0/libbox0.h>
#include <QSpinBox>
#include <QComboBox>
#include <QThread>
#include "extra/widget/bitsize.h"
#include "extra/widget/speed.h"
#include "extra/sampler/ain/stream.h"
#include "extra/sampler/ain/snapshot.h"
#include "extra/widget/hw_run.h"

#include "ui_osc.h"

/**
 * Oscilloscope
 *  Support both stream and snapshot.
 *
 * All functions/method that start with "snapshot_" belong to snapshot mode
 *  and all those with "stream_" to stream mode.
 *
 * osc_snapshot.cpp contain all the code related to snapshot mode
 * osc_stream.cpp contain all the code related to stream mode
 * osc.cpp contain the generic code that is not dependent on mode or is glue code
 *
 * Some method will do the routing part to stream/snapshot mode functions
 */

class BsOsc : public BsInstrument
{
	Q_OBJECT

	public:
		BsOsc(QWidget *parent = Q_NULLPTR);
		void loadModule(b0_device *dev, int index);

	private Q_SLOTS:
		void start();
		void stop();
		void bitsizeChanged(uint bitsize);
		void acquisitionModeChanged();

	protected:
		void closeEvent(QCloseEvent *);

	private:
		Ui::BsOsc ui;

		/* elements in toolbar */
		BsHwRun *ui_hw_run;
		QComboBox *ui_mode;
		BsBitsize *ui_bitsize;

		/* Box0 related */
		b0_ain *module = NULL;

		QList<unsigned int> m_channelSequence;

		/* store if we are running in stream mode */
		bool m_running_stream;

	/* ~~~~~~~~~~~~~~ snapshot ~~~~~~~~~~~~~~~~  */
	private:
		BsAinSnapshotSampler *snapshot_sampler;
		QSpinBox *snapshot_ui_speed;
		QAction *snapshot_ui_speed_action;
		unsigned long m_snapshot_feed_last_speed;
		void snapshot_constructor(const QFont &font);
		void snapshot_setAcquisitionMode(bool stream);
		bool snapshot_start(const QList<uint> &channelSequence, uint bitsize);
		void snapshot_stop();
		void snapshot_bitsizeChanged(uint bitsize);

	private Q_SLOTS:
		void snapshot_feed(unsigned long speed, unsigned int bitsize,
					float *data, uint size);
		void snapshot_speedSlider(int);
		void snapshot_speedEditFinished();
		void snapshot_speedChanged();

	/* ~~~~~~~~~~~~~~ stream ~~~~~~~~~~~~~~~~  */
	private:
		BsAinStreamSampler *stream_sampler;
		BsSpeed *stream_ui_speed;
		QAction *stream_ui_speed_action;
		void stream_constructor(const QFont &font);
		void stream_setAcquisitionMode(bool stream);
		bool stream_start(const QList<uint> &channelSequence, uint bitsize);
		void stream_stop();
		void stream_bitsizeChanged(unsigned int bitsize);

	private Q_SLOTS:
		void stream_feed(float *data, uint size);

};

#endif
