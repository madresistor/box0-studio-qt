/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_OSC_CONFIG_FILTER_H
#define BOX0_STUDIO_OSC_CONFIG_FILTER_H

#include <QObject>
#include <QWidget>
#include <QGroupBox>
#include <QList>
#include "extra/sigproc/filter.h"

#include "ui_osc_config_filter.h"

class BsOscConfigFilter : public QGroupBox
{
	Q_OBJECT

	public:
		BsOscConfigFilter(QWidget *parent = Q_NULLPTR);
		void stream_prepare(qreal sampling_freq, uint count);
		void stream_process(float *data, size_t count);
		void stream_done();

		void snapshot_prepare(uint count);
		void snapshot_process(qreal sampling_freq, float *data, size_t count);
		void snapshot_done();

	private Q_SLOTS:
		void filterChanged(int);
		void rePrepare();
		void guiLimitFreqbyNyquist();

	Q_SIGNALS:
		void snapshotSamplingFreqChanged(); /* private */

	private:
		void addEntry(const QString &title, const char *file_name);
		void buildUpFilter();

		static constexpr int FILTER_NONE = 0;
		static constexpr int FILTER_LOW_PASS = 1;
		static constexpr int FILTER_BAND_PASS = 2;
		static constexpr int FILTER_BAND_REJECT = 3;
		static constexpr int FILTER_HIGH_PASS = 4;

		Ui::BsOscConfigFilter ui;
		QList<bs_filter *> m_channels;
		int m_filterSelected = FILTER_NONE;
		qreal m_sampling_freq = 0;
		size_t m_channel_count = 0;
		bool m_prepared = false, m_acquisition_stream;

		qreal m_freq_lower, m_freq_upper;
};

#endif
