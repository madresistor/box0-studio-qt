/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_OSC_CONFIG_AVERAGE_H
#define BOX0_STUDIO_OSC_CONFIG_AVERAGE_H

#include <QObject>
#include <QWidget>
#include <QGroupBox>

#include "ui_osc_config_average.h"

struct BsOscConfigStreamAverageChannelInfo {
	public:
		qreal m_value;
		size_t m_index;

	public:
		BsOscConfigStreamAverageChannelInfo() {
			m_value = 0;
			m_index = 0;
		}

		size_t process(float *data, size_t count, size_t offset, size_t stride, size_t average);
};

class BsOscConfigAverage : public QGroupBox
{
	Q_OBJECT

	public:
		BsOscConfigAverage(QWidget *parent = Q_NULLPTR);
		uint average();

		void stream_prepare(uint channel_count);
		size_t stream_process(float *data, size_t count);
		void stream_done();

		void snapshot_prepare(uint channel_count);
		size_t snapshot_process(float *data, size_t count);
		void snapshot_done();

	Q_SIGNALS:
		void performRestart();

	private Q_SLOTS:
		void glueAverageValueChanged(int value);

	private:
		Ui::BsOscConfigAverage ui;
		QList<BsOscConfigStreamAverageChannelInfo *> m_channel_info;
		uint m_snapshot_channel_count;

		/* set to true if stream is running.
		 *  This is used to keep track if we need to send restart signal
		 */
		bool m_stream_prepared = false;
		uint m_average;
};

#endif
