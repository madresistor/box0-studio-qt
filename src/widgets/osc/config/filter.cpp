/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "filter.h"
#include "extra/sigproc/filter.h"
#include <QIcon>

#include "extra/multi_proc.h"

BsOscConfigFilter::BsOscConfigFilter(QWidget *parent) :
	QGroupBox(parent)
{
	ui.setupUi(this);

	/* disable both input at startup */
	ui.lower->setEnabled(false);
	ui.upper->setEnabled(false);

	/* list of available filters */
	ui.cmbFilter->addItem("None");
	addEntry("Low Pass", "low-pass");
	addEntry("Band Pass", "band-pass");
	addEntry("Band Reject", "band-reject");
	addEntry("High Pass", "high-pass");

	QSize iconSize(24, 24);
	ui.cmbFilter->setIconSize(iconSize);

	connect(ui.cmbFilter, SIGNAL(currentIndexChanged(int)), SLOT(filterChanged(int)));
	connect(ui.lower, SIGNAL(valueChanged(double)), SLOT(rePrepare()));
	connect(ui.upper, SIGNAL(valueChanged(double)), SLOT(rePrepare()));

	/* force the gui to update but blocking sampling thread till gui not updated. */
	connect(this, SIGNAL(snapshotSamplingFreqChanged()),
			this, SLOT(guiLimitFreqbyNyquist()), Qt::BlockingQueuedConnection);
}

void BsOscConfigFilter::addEntry(const QString &title, const char *file_name)
{
	QIcon icon(QString(":/filter/%1").arg(file_name));
	ui.cmbFilter->addItem(icon, title);
}

void BsOscConfigFilter::filterChanged(int id)
{
	bool lower = true, upper = true;

	switch (id) {
	case FILTER_NONE:
		lower = upper = false;
	break;
	case FILTER_LOW_PASS:
		upper = false;
	break;
	case FILTER_BAND_PASS:
	case FILTER_BAND_REJECT:
	break;
	case FILTER_HIGH_PASS:
		lower = false;
	break;
	}

	ui.lower->setEnabled(lower);
	ui.upper->setEnabled(upper);

	m_filterSelected = id;

	rePrepare();
}

/**
 * @thread{gui}
 */
void BsOscConfigFilter::stream_prepare(qreal sampling_freq, uint ch_size)
{
	m_sampling_freq = sampling_freq;
	m_channel_count = ch_size;
	m_prepared = true;
	m_acquisition_stream = true;

	guiLimitFreqbyNyquist();

	if (m_filterSelected != FILTER_NONE) {
		buildUpFilter();
	}
}

void BsOscConfigFilter::stream_done()
{
	for (int i = 0; i < m_channels.size(); i++) {
		bs_filter *ch_filter = m_channels.at(i);
		bs_filter_fini(ch_filter);
	}

	m_channels.clear();

	m_sampling_freq = 0;
	m_channel_count = 0;
	m_prepared = false;
}

/**
 * @thread{sampler}
 */
void BsOscConfigFilter::stream_process(float *data, size_t size)
{
	if (m_filterSelected == FILTER_NONE) {
		return;
	}

	MULTI_PROC_FOR_LOOP
	for (size_t i = 0; i < m_channel_count; i++) {
		bs_filter *ch_filter = m_channels.at(i);
		bs_filter_apply(ch_filter, data, size, i, m_channel_count);
	}
}

void BsOscConfigFilter::rePrepare()
{
	/* not prepared */
	if (!m_prepared) {
		return;
	}

	m_freq_lower = ui.lower->value();
	m_freq_upper = ui.upper->value();

	if (m_acquisition_stream) {
		buildUpFilter();
	} else {
		m_sampling_freq = 0; /* force recalculation of filter */
	}
}

void BsOscConfigFilter::snapshot_prepare(uint ch_size)
{
	m_channel_count = ch_size;
	m_sampling_freq = 0;
	m_prepared = true;
	m_acquisition_stream = false;
}

void BsOscConfigFilter::snapshot_done()
{
	for (int i = 0; i < m_channels.size(); i++) {
		bs_filter *ch_filter = m_channels.at(i);
		bs_filter_fini(ch_filter);
	}

	m_channels.clear();

	m_sampling_freq = 0;
	m_channel_count = 0;
	m_prepared = false;
}

void BsOscConfigFilter::guiLimitFreqbyNyquist()
{
	qreal sampling_freq_per_ch = m_sampling_freq / m_channel_count;

	/* filter frequency cannot be greater
	 *   than Nyquist frequency of channel */
	ui.lower->setMaximum(sampling_freq_per_ch / 2);
	ui.upper->setMaximum(sampling_freq_per_ch / 2);
}

void BsOscConfigFilter::buildUpFilter()
{
	if (m_filterSelected == FILTER_NONE) {
		return;
	}

	qreal sampling_freq_per_ch = m_sampling_freq / m_channel_count;
	qreal lower = m_freq_lower;
	qreal upper = m_freq_upper;

	/* be forgiving if user exchange upper and lower values */
	if (m_filterSelected == FILTER_BAND_PASS ||
		m_filterSelected == FILTER_BAND_REJECT) {
		if (upper < lower) {
			qSwap(lower, upper);
		}
	}

	for (int i = 0; i < (int) m_channel_count; i++) {
		bs_filter *ch_filter = NULL;

		if (i < m_channels.count()) {
			ch_filter = m_channels.at(i);
		}

		switch (m_filterSelected) {
		case FILTER_LOW_PASS:
			ch_filter = bs_filter_init_lp(ch_filter,
				sampling_freq_per_ch, lower);
		break;
		case FILTER_HIGH_PASS:
			ch_filter = bs_filter_init_hp(ch_filter,
				sampling_freq_per_ch, upper);
		break;
		case FILTER_BAND_PASS:
			ch_filter = bs_filter_init_bp(ch_filter,
				sampling_freq_per_ch, lower, upper);
		break;
		case FILTER_BAND_REJECT:
			ch_filter = bs_filter_init_br(ch_filter,
				sampling_freq_per_ch, lower, upper);
		break;
		}

		if (i < m_channels.count()) {
			m_channels.replace(i, ch_filter);
		} else {
			m_channels.append(ch_filter);
		}
	}
}

/**
 * @thread{sampler}
 */
void BsOscConfigFilter::snapshot_process(qreal sampling_freq,
		float *data, size_t size)
{
	/* sampling frequency has changed? */
	if (m_sampling_freq != sampling_freq) {
		m_sampling_freq = sampling_freq;
		snapshotSamplingFreqChanged();
		buildUpFilter();
	}

	if (m_filterSelected == FILTER_NONE) {
		return;
	}

	MULTI_PROC_FOR_LOOP
	for (size_t i = 0; i < m_channel_count; i++) {
		bs_filter *ch_filter = m_channels.at(i);
		bs_filter_state_reset(ch_filter);
		bs_filter_apply(ch_filter, data, size, i, m_channel_count);
	}
}
