/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "average.h"

#include "extra/multi_proc.h"

BsOscConfigAverage::BsOscConfigAverage(QWidget *parent) :
	QGroupBox(parent)
{
	ui.setupUi(this);

	m_average = ui.spinAverage->value();
	connect(ui.spinAverage, SIGNAL(valueChanged(int)),
		SLOT(glueAverageValueChanged(int)));
}

uint BsOscConfigAverage::average()
{
	return m_average;
}

void BsOscConfigAverage::stream_prepare(uint channel_count)
{
	qDeleteAll(m_channel_info);
	m_channel_info.clear();

	for (uint i = 0; i < channel_count; i++) {
		BsOscConfigStreamAverageChannelInfo *info;
		info = new BsOscConfigStreamAverageChannelInfo();
		m_channel_info.append(info);
	}

	/* store the value */
	m_average = ui.spinAverage->value();
	m_stream_prepared = true;
}

/**
 * @return the number of bytes written back to the array
 */
size_t BsOscConfigStreamAverageChannelInfo::process(float *data, size_t count,
	size_t offset, size_t stride, size_t average)
{
	size_t written_back = 0;

	/* redback the info */
	qreal avg_value = m_value;
	size_t avg_index = m_index;

	for (size_t i = offset; i < count; i += stride) {
		if (avg_index >= average) {
			size_t write_index = offset + (written_back * stride);
			data[write_index] = avg_value / avg_index;
			avg_value = avg_index = 0;
			written_back++;
		}

		avg_index++;
		avg_value += data[i];
	}

	/* store back the info */
	m_value = avg_value;
	m_index = avg_index;

	return written_back;
}

/**
 * @thread{sampler}
 */
size_t BsOscConfigAverage::stream_process(float *data, size_t count)
{
	Q_ASSERT(count > 0);

	if (!(m_average > 1)) {
		/* not required to process */
		return count;
	}

	size_t chan_count = (size_t) m_channel_info.count();
	size_t written = 0;

	MULTI_PROC_OPENMP(omp parallel for reduction(+:written))
	for (size_t i = 0; i < chan_count; i++) {
		size_t w = m_channel_info.at(i)->process(data, count, i, chan_count, m_average);
		written += w;
	}

	return written;
}

void BsOscConfigAverage::stream_done()
{
	m_stream_prepared = false;
}

void BsOscConfigAverage::snapshot_prepare(uint channel_count)
{
	m_snapshot_channel_count = channel_count;
}

/**
 * @thread{sampler}
 */
size_t BsOscConfigAverage::snapshot_process(float *data, size_t count)
{
	uint average_count = m_average;

	if (!(average_count > 1)) {
		return count;
	}

	size_t written = 0;

	MULTI_PROC_OPENMP(omp parallel for reduction(+:written))
	for (uint i = 0; i < m_snapshot_channel_count; i++) {

		size_t written_tmp = 0;
		size_t avg_index = 0;
		qreal avg_value = 0;
		size_t stride = m_snapshot_channel_count;

		for (size_t j = i; j < count; j += stride) {
			if (avg_index >= average_count) {
				data[i + (written_tmp * stride)] = avg_value / avg_index;
				written_tmp++;
				avg_index = avg_value = 0;
			}

			avg_index++;
			avg_value += data[j];
		}

		written += written_tmp;
	}

	return written;
}

void BsOscConfigAverage::snapshot_done()
{
}

void BsOscConfigAverage::glueAverageValueChanged(int value)
{
	m_average = value;

	/* we are in stream mode, we have to restart */
	if (m_stream_prepared) {
		Q_EMIT performRestart();
	}
}
