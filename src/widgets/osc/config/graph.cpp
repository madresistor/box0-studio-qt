/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "graph.h"
#include <QSettings>

const static QString GROUP_ID = "osc";
const static QString GRAPH_LINE_WIDTH_ID = "graph-line-width";
const static QString GRAPH_FLOW_PLOT_ID = "graph-flow-plot";

BsOscConfigGraph::BsOscConfigGraph(QWidget *parent) :
	QGroupBox(parent)
{
	ui.setupUi(this);

	loadSetting();

	connect(ui.graphFlowPlot, SIGNAL(toggled(bool)),
		SIGNAL(flowPlotChanged(bool)));

	connect(ui.graphLineWidth, SIGNAL(valueChanged(double)),
		SIGNAL(lineWidthChanged(double)));
}

bool BsOscConfigGraph::flowPlot()
{
	return ui.graphFlowPlot->isChecked();
}

float BsOscConfigGraph::lineWidth()
{
	return ui.graphLineWidth->value();
}

void BsOscConfigGraph::loadSetting()
{
	QSettings s;
	s.beginGroup(GROUP_ID);

	if (s.contains(GRAPH_LINE_WIDTH_ID)) {
		float value = s.value(GRAPH_LINE_WIDTH_ID).toReal();
		ui.graphLineWidth->setValue(value);
	}

	if (s.contains(GRAPH_FLOW_PLOT_ID)) {
		bool value = s.value(GRAPH_FLOW_PLOT_ID).toBool();
		ui.graphFlowPlot->setChecked(value);
	}

	s.endGroup();
}

void BsOscConfigGraph::storeSetting()
{
	QSettings s;
	s.beginGroup(GROUP_ID);
	s.setValue(GRAPH_LINE_WIDTH_ID, ui.graphLineWidth->value());
	s.setValue(GRAPH_FLOW_PLOT_ID, ui.graphFlowPlot->isChecked());
	s.endGroup();
}
