/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_OSC_MEASURE_MODEL_H
#define BOX0_STUDIO_OSC_MEASURE_MODEL_H

#include <QObject>
#include <QAbstractItemModel>
#include <QAbstractItemView>
#include <QMutex>
#include <QList>
#include "widgets/osc/channel_manager/channel_manager.h"

/* TODO: implement Frequency */

struct BsOscMeasureEntry {
	public:
		enum Type {
			Custom,

			/* data */
			DataPeakToPeak,
			DataAverage,
			DataMaximum,
			DataMinimum,
			DataRms,

			/* channel */
			ChannelInputLow,
			ChannelInputHigh,
			ChannelInputRange,
			ChannelSampleRate,
			ChannelResolution,
			ChannelBitsize,

			/* curve fit */
			CurveFitFrequency,
			CurveFitAmplitude,
			CurveFitOffset,
			CurveFitPhase
		};

		/* type of measurement to perform */
		Type type;

		/* physical channel */
		int chan;

		/* custom */
		QString name, equation, unit;
		QColor color;

		/* last calculation value */
		qreal value;
};

Q_DECLARE_METATYPE(BsOscMeasureEntry::Type)

class BsOscMeasureModel : public QAbstractItemModel, private QMutex
{
	Q_OBJECT

	public:
		BsOscMeasureModel(QObject *parent = Q_NULLPTR);

		int columnCount(const QModelIndex &parent = QModelIndex()) const;
		int rowCount(const QModelIndex &parent = QModelIndex()) const;
		QVariant data(const QModelIndex &index, int role) const;
		Qt::ItemFlags flags(const QModelIndex &index) const;
		QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
		QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
		QModelIndex parent(const QModelIndex & index) const;

	public:
		void setChannelManager(BsOscChannelManager *channelManager);
		int addEntry(int ch, BsOscMeasureEntry::Type type);
		int addEntry(const QString &name, const QString &eq, const QString &unit, QColor color);
		void removeEntry(int i);
		void exchangeEntry(int a, int b);

		void process(const QList<uint> &chan_seq,
			qreal sampling_freq, uint bitsize, qreal ref_low, qreal ref_high,
				float *data, size_t count);

	public Q_SLOTS:
		void fakeReset();
		void resetEntriesValue();
		void allValuesChanged();

	Q_SIGNALS:
		/** this signal is emitted after @a process() finished.
		 *  @thread{sampler}  */
		void processFinished();

	private:
		QList<BsOscMeasureEntry> m_entries;
		BsOscChannelManager *m_channelManager = Q_NULLPTR;

};

#endif
