/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "model.h"
#include <QtMath>
#include <cmath>
#include <lmmin.h>
#include <lmstruct.h>
#include <QDebug>
#include "extra/parser/parser.h"
#include "extra/sigproc/array.h"

using namespace std;

BsOscMeasureModel::BsOscMeasureModel(QObject *parent) :
	QAbstractItemModel(parent),
	QMutex()
{
	/* when process is finised,
	 *  signal the GUI thread that values have been updated */
	connect(this, SIGNAL(processFinished()),
		this, SLOT(allValuesChanged()), Qt::QueuedConnection);
}

/**
 * Add a entry
 * @param ch Physical Channel
 * @param type Measurement type
 * @return the ID of the entry
 */
int BsOscMeasureModel::addEntry(int ch, BsOscMeasureEntry::Type type)
{
	BsOscMeasureEntry entry;
	entry.value = NAN;
	entry.chan = ch;
	entry.type = type;

	beginResetModel();

	lock();
	int id = m_entries.count();
	m_entries.append(entry);
	unlock();

	endResetModel();

	return id;
}

void BsOscMeasureModel::setChannelManager(BsOscChannelManager *channelManager)
{
	m_channelManager = channelManager;

	/* when data change, force updating the view */
	/* TODO: headerDataChanged() is more appropriate? */
	connect(channelManager, SIGNAL(physicalChannelDataChanged()), SLOT(fakeReset()));
}

int BsOscMeasureModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return 2;
}

int BsOscMeasureModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return m_entries.count();
}

/**
 * Convert @a type to string description
 * @param type Type
 * @return String description
 */
QString entry_type_as_string(const BsOscMeasureEntry &entry)
{
	switch (entry.type) {
	/* data */
	case BsOscMeasureEntry::DataPeakToPeak:
	return "Peak To Peak";
	case BsOscMeasureEntry::DataAverage:
	return "Average";
	case BsOscMeasureEntry::DataMaximum:
	return "Maximum";
	case BsOscMeasureEntry::DataMinimum:
	return "Minimum";
	case BsOscMeasureEntry::DataRms:
	return "RMS";

	/* curve fit */
	case BsOscMeasureEntry::CurveFitFrequency:
	return "Frequency";
	case BsOscMeasureEntry::CurveFitAmplitude:
	return "Amplitude";
	case BsOscMeasureEntry::CurveFitOffset:
	return "Offset";
	case BsOscMeasureEntry::CurveFitPhase:
	return "Phase";

	/* channel */
	case BsOscMeasureEntry::ChannelInputLow:
	return "Input Low";
	case BsOscMeasureEntry::ChannelInputHigh:
	return "Input High";
	case BsOscMeasureEntry::ChannelInputRange:
	return "Input Range";
	case BsOscMeasureEntry::ChannelSampleRate:
	return "Sample Rate";
	case BsOscMeasureEntry::ChannelResolution:
	return "Resolution";
	case BsOscMeasureEntry::ChannelBitsize:
	return "Bitsize";

	case BsOscMeasureEntry::Custom:
		if (entry.name.isEmpty()) {
			return entry.equation;
		}

		return entry.name;
	}

	return QString();
}

QVariant BsOscMeasureModel::data(const QModelIndex &index, int role) const
{
	if (! index.isValid()) {
		return QVariant();
	}

	if (role != Qt::DisplayRole) {
		return QVariant();
	}

	int r = index.row();
	int c = index.column();

	/* out of index check */
	if (r >= m_entries.count()) {
		return QVariant();
	}

	const BsOscMeasureEntry &entry = m_entries[r];

	if (c == 0) {
		return entry_type_as_string(entry);
	} else if (c == 1) {
		if (!qIsNaN(entry.value)) {
			const BsOscChannelPhysical *chan = m_channelManager->m_physical_channel.at(entry.chan);
			QString unit;

			switch (entry.type) {
			case BsOscMeasureEntry::DataPeakToPeak:
			case BsOscMeasureEntry::DataAverage:
			case BsOscMeasureEntry::DataMaximum:
			case BsOscMeasureEntry::DataMinimum:
			case BsOscMeasureEntry::DataRms:
			case BsOscMeasureEntry::CurveFitAmplitude:
			case BsOscMeasureEntry::CurveFitOffset:
			case BsOscMeasureEntry::ChannelInputLow:
			case BsOscMeasureEntry::ChannelInputHigh:
			case BsOscMeasureEntry::ChannelInputRange:
			case BsOscMeasureEntry::ChannelResolution:
				unit = chan->unit;
			break;

			case BsOscMeasureEntry::ChannelSampleRate:
			case BsOscMeasureEntry::CurveFitFrequency:
				unit = "Hz";
			break;
			case BsOscMeasureEntry::CurveFitPhase:
				unit = "°";
			break;

			case BsOscMeasureEntry::ChannelBitsize:
			break;

			case BsOscMeasureEntry::Custom:
				unit = entry.unit;
			break;
			}

			return QString("%1%2").arg(entry.value).arg(unit);
		}
	}

	return QVariant();
}

Qt::ItemFlags BsOscMeasureModel::flags(const QModelIndex &index) const
{
	Q_UNUSED(index);
	return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

QVariant BsOscMeasureModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Vertical) {

		/* out of index check */
		if (section >= m_entries.count()) {
			return QVariant();
		}

		/* channel manager check */
		if (!m_channelManager) {
			return QVariant();
		}

		const BsOscMeasureEntry &entry = m_entries.at(section);

		/* entry have no entry */
		if (entry.type == BsOscMeasureEntry::Custom) {
			if (role == Qt::ForegroundRole) {
				return entry.color;
			} else if (role == Qt::DisplayRole) {
				return "▶";
			}
			return QVariant();
		}

		/* channel check */
		if (entry.chan >= m_channelManager->m_physical_channel.count()) {
			return QVariant();
		}

		const BsOscChannelPhysical *chan =
			m_channelManager->m_physical_channel.at(entry.chan);

		if (role == Qt::ForegroundRole) {
			return chan->color;
		} else if (role == Qt::DisplayRole) {
			return chan->title;
		}
	} else {
		if (role == Qt::DisplayRole) {
			if (section == 0) return QString("Type");
			else if (section == 1) return QString("Value");
		}
	}

	return QVariant();
}

QModelIndex BsOscMeasureModel::index(int row, int column, const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	Q_ASSERT(column == 0);
	return createIndex(row, column, (void *) 0);
}

QModelIndex BsOscMeasureModel::parent(const QModelIndex & index) const
{
	Q_UNUSED(index);
	return QModelIndex();
}

void BsOscMeasureModel::fakeReset()
{
	beginResetModel();
	endResetModel();
}

/**
 * Clear all entries value
 */
void BsOscMeasureModel::resetEntriesValue()
{
	for (int i = 0; i < m_entries.count(); i++) {
		BsOscMeasureEntry &entry = m_entries[i];
		entry.value = NAN;
	}

	/* update */
	QVector<int> roles;
	roles << Qt::DisplayRole;
	dataChanged(index(0, 1), index(m_entries.count(), 1), roles);
}

struct sine_curve_fit_data {
	qreal sampling_freq;
	float *data;
	size_t offset;
	size_t stride;
	size_t count;
};

struct sine_curve_fit_param {
	double frequency;
	double amplitude;
	double phase;
	double offset;
};

/**
 * lmfit callback.
 * see (demo/curve1.c + lib/lmcurve.c)
 *
 * @note variable constraint has been applied
 *    ampltude' = exp(amplitude)
 *
 *  see: http://apps.jcns.fz-juelich.de/doku/sc/lmfit:constraints
 */
static void sine_fit_eval(const double *_param, const int m_data,
		const void *_data, double *fvec, int *info)
{
	Q_UNUSED(info);

	const struct sine_curve_fit_param *param = (const struct sine_curve_fit_param *) _param;
	const struct sine_curve_fit_data *data = (const struct sine_curve_fit_data *) _data;
	float *real = &data->data[data->offset];

	for (int i = 0; i < m_data; i++) {
		/* sine angle */
		qreal angle = 2 * M_PI * param->frequency * i / data->sampling_freq;
		angle += param->phase;

		/* the ideal value that we can expect */
		qreal amplitude_dash = exp(param->amplitude);
		qreal ideal = (amplitude_dash * sin(angle)) + param->offset;

		/* store back the difference from real value */
		fvec[i] = real[i * data->stride] - ideal;
	}
}

/**
 * Extract inital parameter for the curve fit from frequency domain data
 * @param chan Channel
 * @param curveFit Store the result to
 */
static void prepare_sine_fit_param(BsOscChannelPhysical *chan,
					struct sine_curve_fit_param *param)
{
	if (chan->domain.freq.valid < 2) {
		return;
	}

	size_t max_index = 0;
	float max_value = -INFINITY;
	for (size_t i = 1; i < chan->domain.freq.valid; i++) {
		float tmp = chan->domain.freq.y[i];
		if (tmp > max_value) {
			max_index = i;
			max_value = tmp;
		}
	}

	param->phase = 0;
	param->offset = chan->domain.freq.y[0];
	param->amplitude = log(chan->domain.freq.y[max_index]);
	param->frequency = chan->domain.freq.x[max_index];
}

/**
 * Perform curve fitting on the Channel @a chan
 * @param Entry
 * @return true on success
 * @return false on failure
 */
static bool perform_sine_curve_fit(BsOscChannelPhysical *chan,
	struct sine_curve_fit_param *param, struct sine_curve_fit_data *data)
{
	/* number of parameters in model function f */
	int independent_variable = 4;

	/* starting parameters */
	prepare_sine_fit_param(chan, param);

	/* data points */
	int sample_count = (data->count - data->offset) / data->stride;

	lm_control_struct control = lm_control_float;
	lm_status_struct status;
	control.verbosity = 0;
	control.patience = 20; /* total evaluation = 10 * (4 + 1) = 50 */

	/* now the call to lmfit */
	lmmin(independent_variable, (double *) param, sample_count, NULL,
		(const void *) data, sine_fit_eval, &control, &status);

	/* reverse the transform */
	param->amplitude = exp(param->amplitude);

	param->phase = BsParser::wrapTo2Pi(param->phase);
	param->phase = qRadiansToDegrees(param->phase);

	return status.outcome >= 0 && status.outcome < 4;
}


struct single_valued_result {
	bool performed;
	qreal value;
};

struct curve_fit_result {
	bool performed;
	struct sine_curve_fit_param value;
};

/**
 * Ensure Curve fitting on data has been performed
 * @param sampling_freq Sampling frequency of channel
 * @param data Data
 * @param count Total samples in @a data
 * @param offset Number of samples to skip for first sample
 * @param stride Number of samples to skip for next sample
 * @param[in] res Store back the result of calculation
 */
void ensure_curve_fit_performed(
	BsOscChannelPhysical *chan,
	qreal sampling_freq,
	float *data, size_t count,
	size_t offset, size_t stride,
	struct curve_fit_result *res)
{
	if (res->performed) {
		return;
	}

	/* context data */
	struct sine_curve_fit_data eval_data;
	eval_data.sampling_freq = sampling_freq;
	eval_data.data = data;
	eval_data.offset = offset;
	eval_data.stride = stride;
	eval_data.count = count;

	/* perform curve fitting */
	if (!perform_sine_curve_fit(chan, &res->value, &eval_data)) {
		/* did not succeed */
		res->value.frequency = NAN;
		res->value.offset = NAN;
		res->value.phase = NAN;
		res->value.amplitude = NAN;
	}

	res->performed = true;
}

/**
 * Ensure that the single valued calculation has been performed.
 * If not performed, call @a performer and store the result
 */
static void ensure_single_valued_result_performed(
	struct single_valued_result *rcv,
	float (*performer)(float *, size_t, size_t, size_t),
	float *data, size_t count, size_t offset, size_t stride)
{
	if (!rcv->performed) {
		rcv->performed = true;
		rcv->value = performer(data, count, offset, stride);
	}
}

/**
 * return a list of all used variables in equation @a eq
 * @param eq Equation
 * @return list of variables
 */
QList<QString> get_used_variables(const QString &eq)
{
	QSet<QString> set;
	BsParser parser;
	parser.setExpression(eq);

	mu::varmap_type vars = parser.GetUsedVar();
	mu::varmap_type::const_iterator item;

	for (item = vars.begin(); item != vars.end(); item++) {
		QString name = QString::fromStdString(item->first);
		set.insert(name);
	}

	return set.values();
}

/**
 * Process the data
 * @param chan_seq Channel Sequence
 * @param sampling_freq Sampling frequency
 * @param data Data
 * @param count Number of samples in @a data
 * @thread{sampler}
 */
void BsOscMeasureModel::process(const QList<uint> &chan_seq,
	qreal sampling_freq, uint bitsize, qreal ref_low, qreal ref_high,
	float *data, size_t count)
{
	size_t stride = (size_t) chan_seq.count();
	qreal sampling_freq_per_ch = sampling_freq / stride;
	qreal resolution = (ref_high - ref_low) / pow(2, bitsize);

	/* a central repository to store already
	 *  performed cpu intensive calculations. */
	struct result_cache_value {
		struct single_valued_result
		data_p2p,
		data_avg,
		data_max,
		data_min,
		data_rms;

		struct curve_fit_result curve_fit;
	} result_cache[stride];

	/* mark all performed=false */
	memset(result_cache, 0, sizeof(result_cache));

	/* parser for custom equations */
	BsParser parser;

	/* define all variables that any expression would require */
	for (size_t i = 0; i < stride; i++) {
		int ch = chan_seq[i];
		struct result_cache_value *rcv = &result_cache[i];
		QString fmt = QString("%1_%2").arg(BsOscChannelPhysical::strID(ch));

		parser.defineVariable(fmt.arg("MIN"), &rcv->data_min.value);
		parser.defineVariable(fmt.arg("MAX"), &rcv->data_max.value);
		parser.defineVariable(fmt.arg("AVG"), &rcv->data_avg.value);
		parser.defineVariable(fmt.arg("P2P"), &rcv->data_p2p.value);
		parser.defineVariable(fmt.arg("RMS"), &rcv->data_rms.value);

		parser.defineVariable(fmt.arg("AMP"), &rcv->curve_fit.value.amplitude);
		parser.defineVariable(fmt.arg("FREQ"), &rcv->curve_fit.value.frequency);
		parser.defineVariable(fmt.arg("OFFSET"), &rcv->curve_fit.value.offset);
		parser.defineVariable(fmt.arg("PHASE"), &rcv->curve_fit.value.phase);

		/* defining as constant because same for all equations */
		parser.defineConstant(fmt.arg("INP_LOW"), ref_low);
		parser.defineConstant(fmt.arg("INP_HIGH"), ref_high);
		parser.defineConstant(fmt.arg("INP_RANGE"), ref_high - ref_low);
		parser.defineConstant(fmt.arg("SAMP_RATE"), sampling_freq_per_ch);
		parser.defineConstant(fmt.arg("RESOL"), resolution);
		parser.defineConstant(fmt.arg("BITSIZE"), bitsize);
	}

	/* regular expression for seperating variable name into ID and TYPE */
	QRegExp var_regx("CH(\\d+)_(\\w+)");

	/* lock the model, we are going to modify the entries */
	lock();

	for (int i = 0; i < m_entries.count(); i++) {
		BsOscMeasureEntry &entry = m_entries[i];

		/* custom measurements. ==> */
		if (entry.type == BsOscMeasureEntry::Custom) {
			QList<QString> deps = get_used_variables(entry.equation);
			bool problem_in_processing = false;
			qreal value = NAN;

			/* Make sure that all dependencies are
			 *  processed before this equation is processed */
			for (int j = 0; j < deps.size(); j++) {
				QString d = deps.at(j);

				/* valid variable name? */
				if (var_regx.indexIn(d) == -1) {
					problem_in_processing = true;
					break;
				}

				/* extract channel ID from dependency "d" */
				bool ok;
				int ch = var_regx.cap(1).toInt(&ok);
				if (!ok) {
					problem_in_processing = true;
					break;
				}

				/* channel is being sampled? */
				int off = chan_seq.indexOf(ch);
				if (off == -1) {
					problem_in_processing = true;
					break;
				}

				BsOscChannelPhysical *chan =
					m_channelManager->m_physical_channel.at(ch);
				struct result_cache_value *rcv = &result_cache[off];
				QString type = var_regx.cap(2);

				/* list of dependency <type> that require curve fit */
				QList<QString> curve_fit_dep;
				curve_fit_dep << "AMP" << "FREQ" << "OFFSET" << "PHASE";

				/* list of dependency that require nothing, because they are
				 *  same for all equation and defined as constants */
				QList<QString> no_work_req;
				no_work_req << "INP_LOW" << "INP_HIGH" << "SAMP_RATE";
				no_work_req << "RESOL" << "BITSIZE";

				/* perform equaluation as per <type> */
				if (type == "MIN") {
					ensure_single_valued_result_performed(&rcv->data_min,
						bs_array_min, data, count, off, stride);
				} else if (type == "MAX") {
					ensure_single_valued_result_performed(&rcv->data_max,
						bs_array_max, data, count, off, stride);
				} else if (type == "AVG") {
					ensure_single_valued_result_performed(&rcv->data_avg,
						bs_array_avg, data, count, off, stride);
				} else if (type == "P2P") {
					ensure_single_valued_result_performed(&rcv->data_p2p,
						bs_array_p2p, data, count, off, stride);
				} else if (type == "RMS") {
					ensure_single_valued_result_performed(&rcv->data_rms,
						bs_array_rms, data, count, off, stride);
				} else if (curve_fit_dep.indexOf(type) != -1) {
					ensure_curve_fit_performed(chan, sampling_freq_per_ch,
						data, count, off, stride, &rcv->curve_fit);
				} else if (no_work_req.indexOf(type) != -1) {
					/* No work required */
				} else {
					/* we have problem! */
					problem_in_processing = true;
					qDebug() << "unknown type" << type
						<< "in equation" << entry.equation;
					break;
				}
			}

			if (!problem_in_processing) {
				/* evalulate and store the result */
				parser.setExpression(entry.equation);
				try {
					value = parser.Eval();
				} catch (mu::Parser::exception_type &e) {
					qDebug() << "eval failed for equation" << entry.equation;
				}
			}

			entry.value = value;
			continue;
		}

		/* inbuild measurements ==> */

		int off = chan_seq.indexOf(entry.chan);
		if (off == -1) {
			/* channel is not being sampled */
			continue;
		}

		BsOscChannelPhysical *chan = m_channelManager->m_physical_channel.at(entry.chan);
		struct result_cache_value *rcv = &result_cache[off];
		qreal value = NAN;

		switch (entry.type) {
		case BsOscMeasureEntry::DataPeakToPeak:
			ensure_single_valued_result_performed(&rcv->data_p2p,
				bs_array_p2p, data, count, off, stride);
			value = rcv->data_p2p.value;
		break;
		case BsOscMeasureEntry::DataAverage:
			ensure_single_valued_result_performed(&rcv->data_avg,
				bs_array_avg, data, count, off, stride);
			value = rcv->data_avg.value;
		break;
		case BsOscMeasureEntry::DataMaximum:
			ensure_single_valued_result_performed(&rcv->data_max,
				bs_array_max, data, count, off, stride);
			value = rcv->data_max.value;
		break;
		case BsOscMeasureEntry::DataMinimum:
			ensure_single_valued_result_performed(&rcv->data_min,
				bs_array_min, data, count, off, stride);
			value = rcv->data_min.value;
		break;
		case BsOscMeasureEntry::DataRms:
			ensure_single_valued_result_performed(&rcv->data_rms,
				bs_array_rms, data, count, off, stride);
			value = rcv->data_rms.value;
		break;

		/* channel */
		case BsOscMeasureEntry::ChannelInputLow:
			value = ref_low;
		break;
		case BsOscMeasureEntry::ChannelInputHigh:
			value = ref_high;
		break;
		case BsOscMeasureEntry::ChannelInputRange:
			value = ref_high - ref_low;
		break;
		case BsOscMeasureEntry::ChannelSampleRate:
			value = sampling_freq_per_ch;
		break;
		case BsOscMeasureEntry::ChannelResolution:
			value = resolution;
		break;
		case BsOscMeasureEntry::ChannelBitsize:
			value = bitsize;
		break;

		/* curve fitting */
		case BsOscMeasureEntry::CurveFitFrequency:
			ensure_curve_fit_performed(chan, sampling_freq_per_ch,
				data, count, off, stride, &rcv->curve_fit);
			value = rcv->curve_fit.value.frequency;
		break;
		case BsOscMeasureEntry::CurveFitAmplitude:
			ensure_curve_fit_performed(chan, sampling_freq_per_ch,
				data, count, off, stride, &rcv->curve_fit);
			value = rcv->curve_fit.value.amplitude;
		break;
		case BsOscMeasureEntry::CurveFitOffset:
			ensure_curve_fit_performed(chan, sampling_freq_per_ch,
				data, count, off, stride, &rcv->curve_fit);
			value = rcv->curve_fit.value.offset;
		break;
		case BsOscMeasureEntry::CurveFitPhase:
			ensure_curve_fit_performed(chan, sampling_freq_per_ch,
				data, count, off, stride, &rcv->curve_fit);
			value = rcv->curve_fit.value.phase;
		break;

		case BsOscMeasureEntry::Custom:
			/* nothing, just to make compiler happy! */
		break;
		}

		entry.value = value;
	}

	/* unlock the model, we are done modifying entries */
	unlock();

	/* update */
	processFinished();
}

/**
 * slot called via signal processFinished().
 *  signal the view widget that we need to update values
 */
void BsOscMeasureModel::allValuesChanged()
{
	QVector<int> roles;
	roles << Qt::DisplayRole;
	dataChanged(index(0, 1), index(m_entries.count(), 1), roles);
}

/**
 * remove entry (with Index = @a id) from the entries list
 * @param id Index/ID
 */
void BsOscMeasureModel::removeEntry(int id)
{
	if (id >= 0 && id < m_entries.count()) {
		beginResetModel();
		lock();
		m_entries.removeAt(id);
		unlock();
		endResetModel();
	}
}

/**
 * Exchange position of entry at @a a and @a b
 * @param a Entry first
 * @param b Entry second
 */
void BsOscMeasureModel::exchangeEntry(int a, int b)
{
	int count = m_entries.count();
	if (a >= 0 && a < count && b >= 0 && b < count) {
		beginResetModel();
		lock();
		m_entries.swap(a, b);
		unlock();
		endResetModel();
	}
}

/**
 * Add a custom equation @a eq entry for channel @a ch
 * @param name Name
 * @param eq Equation
 * @param unit Unit
 */
int BsOscMeasureModel::addEntry(const QString &name, const QString &eq, const QString &unit, QColor color)
{
	BsOscMeasureEntry entry;
	entry.value = NAN;
	entry.type = BsOscMeasureEntry::Custom;
	entry.name = name;
	entry.equation = eq;
	entry.unit = unit;
	entry.color = color;
	entry.chan=  -1;
	entry.value = NAN;

	beginResetModel();

	lock();
	int id = m_entries.count();
	m_entries.append(entry);
	unlock();

	endResetModel();

	return id;
}
