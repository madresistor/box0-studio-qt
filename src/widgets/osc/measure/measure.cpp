/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "measure.h"
#include "add/add.h"
#include <QDebug>

/*
 * TODO: ability to add custom measurement (for processing the array)
 *  - problem: muparser do not support loop
 */

BsOscMeasure::BsOscMeasure(QWidget *parent) :
	QWidget(parent)
{
	ui.setupUi(this);

	/* setup the table to show entries */
	m_model = new BsOscMeasureModel(this);
	ui.list->setModel(m_model);

	/* when user click "add" button, show add dialog */
	connect(ui.add, SIGNAL(clicked()), SLOT(addDialog()));

	/* remove (the selected) entry when user click "remove" button */
	connect(ui.remove, SIGNAL(clicked()), SLOT(removeSelectedEntry()));

	/* move the selected entry one up when user click "up" button */
	connect(ui.moveUp, SIGNAL(clicked()), SLOT(moveUp()));

	/* move the selected entry one down when user click "down" button */
	connect(ui.moveDown, SIGNAL(clicked()), SLOT(moveDown()));
}

/**
 * Set Channel manager
 * @param channelManager Channel Manager
 */
void BsOscMeasure::setChannelManager(BsOscChannelManager *channelManager)
{
	m_model->setChannelManager(channelManager);
	m_channelManager = channelManager;
}

/**
 * Show Add dialog to user.
 */
void BsOscMeasure::addDialog()
{
	if (m_channelManager == Q_NULLPTR) {
		qWarning() << Q_FUNC_INFO << "m_channelManager is NULL";
		return;
	}

	BsOscMeasureAdd dialog(m_channelManager, this);

	connect(&dialog, SIGNAL(userWantToAdd(int, BsOscMeasureEntry::Type)),
		SLOT(add(int, BsOscMeasureEntry::Type)));

	connect(&dialog,
		SIGNAL(userWantToAdd(const QString &, const QString &, const QString &, QColor)),
		SLOT(add(const QString &, const QString &, const QString &, QColor)));

	dialog.exec();
}

/** Add a entry for channel @a ch of @a type
 * @param ch Channel
 * @param type Type
 */
void BsOscMeasure::add(int ch, BsOscMeasureEntry::Type type)
{
	m_model->addEntry(ch, type);
}

/** Add a custom equation entry for channel @a ch
 * @param ch Channel
 * @param eq Equation
 */
void BsOscMeasure::add(const QString &name, const QString &eq, const QString &unit, QColor color)
{
	m_model->addEntry(name, eq, unit, color);
}

void BsOscMeasure::stream_prepare(qreal sampling_freq, uint bitsize,
	qreal ref_low, qreal ref_high, const QList<uint> &chan_seq)
{
	m_sampling_freq = sampling_freq;
	m_bitsize = bitsize;
	m_ref_low = ref_low;
	m_ref_high = ref_high;
	m_chan_seq = chan_seq;
	m_model->resetEntriesValue();
}

/**
 * @thread{sampler}
 */
void BsOscMeasure::stream_process(float *data, size_t count)
{
	m_model->process(m_chan_seq, m_sampling_freq, m_bitsize, m_ref_low,
					m_ref_high, data, count);
}

void BsOscMeasure::stream_done()
{
	/* no use */
}

void BsOscMeasure::snapshot_prepare(const QList<uint> &chan_seq,
			qreal ref_low, qreal ref_high)
{
	m_chan_seq = chan_seq;
	m_ref_low = ref_low;
	m_ref_high = ref_high;
	m_model->resetEntriesValue();
}

/**
 * @thread{sampler}
 */
void BsOscMeasure::snapshot_process(qreal sampling_freq, uint bitsize,
	float *data, size_t count)
{
	m_model->process(m_chan_seq, sampling_freq, bitsize, m_ref_low,
			m_ref_high, data, count);
}

void BsOscMeasure::snapshot_done()
{
	/* no use */
}

/**
 * slot called when user click "remove" button.
 * Remove the selected entry
 */
void BsOscMeasure::removeSelectedEntry()
{
	int id = ui.list->selectedRow();
	if (id != -1) {
		m_model->removeEntry(id);
		ui.list->selectRow(qMax(id - 1, 0));
	}
}

void BsOscMeasure::moveUp()
{
	int id = ui.list->selectedRow();
	if (id > 0) {
		int prev = id - 1;
		m_model->exchangeEntry(id, prev);
		ui.list->selectRow(prev);
	}
}

void BsOscMeasure::moveDown()
{
	int id = ui.list->selectedRow();
	if (id >= 0) {
		int next = id + 1;
		m_model->exchangeEntry(id, next);
		ui.list->selectRow(next);
	}
}
