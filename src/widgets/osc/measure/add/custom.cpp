/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "custom.h"
#include <QMenu>
#include <QSignalMapper>
#include "extra/parser/parser.h"
#include "extra/parser/constant.h"
#include "extra/parser/function.h"
#include "extra/parser/operator.h"

#define VARIABLE_DATA_DATA_COUNT 5
#define VARIABLE_CURVE_FIT_DATA_COUNT 4
#define VARIABLE_CHANNEL_DATA_COUNT 6

struct variable_data_value {
	const char *name;
	const char *id;
} variable_data_data[VARIABLE_DATA_DATA_COUNT] = {
	{"Minimum", "MIN"},
	{"Maximum", "MAX"},
	{"Average", "AVG"},
	{"Peak To Peak", "P2P"},
	{"Root Mean Square", "RMS"},
}, variable_curve_fit_data[VARIABLE_CURVE_FIT_DATA_COUNT] = {
	{"Amplitude", "AMP"},
	{"Frequency", "FREQ"},
	{"Offset", "OFFSET"},
	{"Phase", "PHASE"},
}, variable_channel_data[VARIABLE_CHANNEL_DATA_COUNT] = {
	{"Input Low", "INP_LOW"},
	{"Input High", "INP_HIGH"},
	{"Input Range", "INP_RANGE"},
	{"Sample Rate", "SAMP_RATE"},
	{"Resolution", "RESOL"},
	{"Bitsize", "BITSIZE"},
};

static QMenu *build_channel_menu(BsOscMeasureAddCustom *that,
	BsOscChannelManager *channelManager, const char *var_append,
	BsParser *parser, double *parser_value)
{
	QMenu *channelMenu = new QMenu(that);

	for (int i = 0; i < channelManager->m_physical_channel.count(); i++) {
		BsOscChannel *chan = channelManager->m_physical_channel.at(i);
		QString strID = BsOscChannelPhysical::strID(i);
		QString title = QString("%1 ― %2").arg(chan->title).arg(strID);

		QAction *action = channelMenu->addAction(title);
		QString name = QString("%1_%2").arg(strID).arg(var_append);
		action->setData(name);

		parser->defineVariable(name, parser_value);
	}

	QObject::connect(channelMenu, SIGNAL(triggered(QAction*)),
		that, SLOT(insertAsVariable(QAction*)));

	return channelMenu;
}

static void add_section(const char *section, BsOscMeasureAddCustom *that,
	QMenu *parent, struct variable_data_value* variable, int count,
	BsOscChannelManager *channelManager,
	BsParser *parser, double *parser_value)
{
	parent->addSection(section);

	for (int i = 0; i < count; i++) {
		const char *name = variable[i].name;
		const char *id = variable[i].id;

		QMenu *sub = build_channel_menu(that, channelManager, id,
			parser, parser_value);

		QString title = QString("%1 ― %2").arg(name).arg(id);
		QAction *action = parent->addAction(title);
		action->setMenu(sub);
		action->setData(QString(id));
	}
}

BsOscMeasureAddCustom::BsOscMeasureAddCustom(BsOscChannelManager *channelManager,
		QWidget *parent) :
	QWidget(parent)
{
	ui.setupUi(this);

	BsParserFunction *func = new BsParserFunction(ui.insertFunction);
	ui.insertFunction->setMenu(func);
	connect(func, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsFunction(const QString &)));

	BsParserOperator *oper = new BsParserOperator(ui.insertOperator);
	ui.insertOperator->setMenu(oper);
	connect(oper, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsOperator(const QString &)));

	BsParserConstant *constant = new BsParserConstant(ui.insertConstant);
	ui.insertConstant->setMenu(constant);
	connect(constant, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsConstant(const QString &)));

	setupVariable(channelManager);

	connect(ui.add, SIGNAL(clicked()), SLOT(glueUserWantToAdd()));
}

void BsOscMeasureAddCustom::setupVariable(BsOscChannelManager *channelManager)
{
	QMenu *variableMenu = new QMenu(this);

	add_section("Data", this, variableMenu,
		variable_data_data, VARIABLE_DATA_DATA_COUNT,
		channelManager, &m_parser, &m_parser_value);

	add_section("Curve Fit", this, variableMenu,
		variable_curve_fit_data, VARIABLE_CURVE_FIT_DATA_COUNT,
		channelManager, &m_parser, &m_parser_value);

	add_section("Channel", this, variableMenu,
		variable_channel_data, VARIABLE_CHANNEL_DATA_COUNT,
		channelManager, &m_parser, &m_parser_value);

	ui.insertVariable->setMenu(variableMenu);
}

/**
 * Validate the equation and return the result
 * @return true on success
 */
bool BsOscMeasureAddCustom::equationValid()
{
	m_parser.setExpression(ui.equation->text());

	try {
		m_parser.Eval();
	} catch (mu::Parser::exception_type &e) {
		QString msg = QString::fromStdString(e.GetMsg());
		ui.equationError->setText(msg);
		return false;
	}

	return true;
}

void BsOscMeasureAddCustom::glueUserWantToAdd()
{
	if (!equationValid()) {
		return;
	}

	QString eq = ui.equation->text();
	QString name = ui.name->text();
	QString unit = ui.unit->text();
	QColor color = ui.color->color();
	Q_EMIT userWantToAdd(name, eq, unit, color);
}

void BsOscMeasureAddCustom::insertAsVariable(QAction *action)
{
	ui.equation->insertAsVariable(action->data().toString());
}
