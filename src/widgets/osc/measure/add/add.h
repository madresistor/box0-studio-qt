/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_OSC_MEASURE_ADD_H
#define BOX0_STUDIO_OSC_MEASURE_ADD_H

#include <QObject>
#include <QWidget>
#include <QDialog>
#include <QAction>
#include "widgets/osc/measure/model.h"

#include "ui_osc_measure_add.h"

class BsOscMeasureAdd: public QDialog
{
	Q_OBJECT

	public:
		BsOscMeasureAdd(BsOscChannelManager *channelManager,
			QWidget *parent = Q_NULLPTR);

	Q_SIGNALS:
		/** emited when user want to add a channel
		 * @param ch Physical Channel ID
		 * @param type Measurement type
		 */
		void userWantToAdd(int ch, BsOscMeasureEntry::Type);
		void userWantToAdd(const QString &name, const QString &eq,
			const QString &unit, QColor color);

	private Q_SLOTS:
		void glueUserWantToAdd(QAction *action);

	private:
		Ui::BsOscMeasureAdd ui;
};

#endif
