/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "add.h"
#include <QMenu>
#include <QDebug>
#include <QPushButton>
#include <QSignalMapper>
#include "custom.h"

BsOscMeasureAdd::BsOscMeasureAdd(BsOscChannelManager *channelManager, QWidget *parent) :
	QDialog(parent)
{
	ui.setupUi(this);

	/* Initalize a common QMenu that will be shown in each button */
	QMenu *channelMenu = new QMenu(this);

	for (int i = 0; i < channelManager->m_physical_channel.count(); i++) {
		BsOscChannel *chan = channelManager->m_physical_channel.at(i);
		QString strID = BsOscChannelPhysical::strID(i);
		QString title = QString("%1 ― %2").arg(chan->title).arg(strID);

		QAction *action = channelMenu->addAction(title);
		action->setData(i);
	}

	/* data */
	ui.dataPeakToPeak->setMenu(channelMenu);
	ui.dataAverage->setMenu(channelMenu);
	ui.dataMinimum->setMenu(channelMenu);
	ui.dataMaximum->setMenu(channelMenu);
	ui.dataRms->setMenu(channelMenu);

	/* curve fit */
	ui.curveFitFrequency->setMenu(channelMenu);
	ui.curveFitAmplitude->setMenu(channelMenu);
	ui.curveFitOffset->setMenu(channelMenu);
	ui.curveFitPhase->setMenu(channelMenu);

	/* channel */
	ui.channelInputLow->setMenu(channelMenu);
	ui.channelInputHigh->setMenu(channelMenu);
	ui.channelInputRange->setMenu(channelMenu);
	ui.channelSampleRate->setMenu(channelMenu);
	ui.channelResolution->setMenu(channelMenu);
	ui.channelBitsize->setMenu(channelMenu);

	connect(channelMenu, SIGNAL(triggered(QAction*)), SLOT(glueUserWantToAdd(QAction*)));

	BsOscMeasureAddCustom *custom = new BsOscMeasureAddCustom(channelManager, this);
	ui.tabWidget->addTab(custom, "Custom");

	connect(custom,
		SIGNAL(userWantToAdd(const QString &, const QString &, const QString &, QColor)),
		SIGNAL(userWantToAdd(const QString &, const QString &, const QString &, QColor)));
}

/**
 * Called when user click a channel action (from channel menu)
 * @param action The triggering action
 */
void BsOscMeasureAdd::glueUserWantToAdd(QAction *action)
{
	int ch = action->data().toInt();

	BsOscMeasureEntry::Type type;

	/* data */
	if (ui.dataPeakToPeak->isDown()) type = BsOscMeasureEntry::DataPeakToPeak;
	else if (ui.dataAverage->isDown()) type = BsOscMeasureEntry::DataAverage;
	else if (ui.dataMaximum->isDown()) type = BsOscMeasureEntry::DataMaximum;
	else if (ui.dataMinimum->isDown()) type = BsOscMeasureEntry::DataMinimum;
	else if (ui.dataRms->isDown()) type = BsOscMeasureEntry::DataRms;

	/* curve fit */
	else if (ui.curveFitFrequency->isDown()) type = BsOscMeasureEntry::CurveFitFrequency;
	else if (ui.curveFitAmplitude->isDown()) type = BsOscMeasureEntry::CurveFitAmplitude;
	else if (ui.curveFitOffset->isDown()) type = BsOscMeasureEntry::CurveFitOffset;
	else if (ui.curveFitPhase->isDown()) type = BsOscMeasureEntry::CurveFitPhase;

	/* channel */
	else if (ui.channelInputLow->isDown()) type = BsOscMeasureEntry::ChannelInputLow;
	else if (ui.channelInputHigh->isDown()) type = BsOscMeasureEntry::ChannelInputHigh;
	else if (ui.channelInputRange->isDown()) type = BsOscMeasureEntry::ChannelInputRange;
	else if (ui.channelSampleRate->isDown()) type = BsOscMeasureEntry::ChannelSampleRate;
	else if (ui.channelResolution->isDown()) type = BsOscMeasureEntry::ChannelResolution;
	else if (ui.channelBitsize->isDown()) type = BsOscMeasureEntry::ChannelBitsize;

	else {
		qWarning() << Q_FUNC_INFO << "unknown button";
		return;
	}

	Q_EMIT userWantToAdd(ch, type);
}
