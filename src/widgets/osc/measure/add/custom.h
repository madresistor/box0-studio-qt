/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_OSC_MEASURE_ADD_CUSTOM_H
#define BOX0_STUDIO_OSC_MEASURE_ADD_CUSTOM_H

#include <QObject>
#include <QWidget>
#include "widgets/osc/measure/model.h"
#include "widgets/osc/channel_manager/channel_manager.h"
#include "extra/parser/parser.h"

#include "ui_osc_measure_add_custom.h"

class BsOscMeasureAddCustom : public QWidget
{
	Q_OBJECT

	public:
		BsOscMeasureAddCustom(BsOscChannelManager *channelManager,
			QWidget *parent = Q_NULLPTR);

	Q_SIGNALS:
		void userWantToAdd(const QString &name, const QString &eq,
			const QString &unit, QColor color);

	private Q_SLOTS:
		void glueUserWantToAdd();

		void insertAsVariable(QAction *action);

	private:
		Ui::BsOscMeasureAddCustom ui;
		void setupVariable(BsOscChannelManager *channelManager);
		bool equationValid();
		BsParser m_parser;
		double m_parser_value = 0;

};

#endif
