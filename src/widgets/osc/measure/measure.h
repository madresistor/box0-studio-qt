/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_OSC_MEASURE_H
#define BOX0_STUDIO_OSC_MEASURE_H

#include <QObject>
#include <QWidget>
#include "widgets/osc/channel_manager/channel_manager.h"
#include "model.h"

#include "ui_osc_measure.h"

class BsOscMeasure : public QWidget
{
	Q_OBJECT

	public:
		BsOscMeasure(QWidget *parent = Q_NULLPTR);

		void setChannelManager(BsOscChannelManager *channelManager);

		void stream_prepare(qreal sampling_freq, uint bitsize, qreal ref_low,
			qreal ref_high, const QList<uint> &chan_seq);
		void stream_process(float *data, size_t count);
		void stream_done();

		void snapshot_prepare(const QList<uint> &chan_seq, qreal ref_low, qreal high);
		void snapshot_process(qreal sampling_freq, uint bitsize,
			float *data, size_t count);
		void snapshot_done();

	private Q_SLOTS:
		void addDialog();
		void add(int ch, BsOscMeasureEntry::Type type);
		void add(const QString &name, const QString &eq, const QString &unit, QColor);
		void removeSelectedEntry();
		void moveUp();
		void moveDown();

	private:
		Ui::BsOscMeasure ui;
		BsOscChannelManager *m_channelManager = Q_NULLPTR;
		BsOscMeasureModel *m_model;
		QList<uint> m_chan_seq;
		qreal m_sampling_freq;
		uint m_bitsize;
		qreal m_ref_low, m_ref_high;
};

#endif
