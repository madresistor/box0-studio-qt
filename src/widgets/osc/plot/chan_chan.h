/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_OSC_PLOT_CHAN_CHAN_H
#define BOX0_STUDIO_OSC_PLOT_CHAN_CHAN_H

#include <QWidget>
#include <QList>
#include "extra/replot/cartesian.h"
#include "widgets/osc/channel_manager/channel_manager.h"

class BsOscPlotChanChan : public Cartesian
{
	Q_OBJECT

	public:
		BsOscPlotChanChan(QWidget *parent = Q_NULLPTR);
		~BsOscPlotChanChan();

		void setX(bool physical, int id);
		void setY(bool physical, int id);

		void setChannelManager(BsOscChannelManager *channelManager);

	private Q_SLOTS:
		void channelDataChanged();

	protected:
		void initializeGL() Q_DECL_OVERRIDE;
		void paintGL() Q_DECL_OVERRIDE;

	private:
		BsOscChannel *searchChannel(bool physical, int id);
		void updateAxisUnit(lp_cartesian_axis *axis, bool physical, int id);
		struct {
			bool physical;
			int id;
		} m_x = {true, 0}, m_y = {true, 1};
		lp_cartesian_curve *m_curve = NULL;
		BsOscChannelManager *m_channelManager = Q_NULLPTR;
};

#endif
