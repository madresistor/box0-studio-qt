/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016, 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "chan_chan.h"

BsOscPlotChanChan::BsOscPlotChanChan(QWidget *parent) :
	Cartesian(parent)
{
}

BsOscPlotChanChan::~BsOscPlotChanChan()
{
	if (m_curve != NULL) lp_cartesian_curve_del(m_curve);
}

void BsOscPlotChanChan::setChannelManager(BsOscChannelManager *channelManager)
{
	m_channelManager = channelManager;

	connect(m_channelManager, SIGNAL(channelDataChanged()),
		SLOT(channelDataChanged()));
}

void BsOscPlotChanChan::initializeGL()
{
	Cartesian::initializeGL();

	updateAxisUnit(m_axis_bottom, m_x.physical, m_x.id);
	updateAxisUnit(m_axis_left, m_y.physical, m_y.id);

	m_curve = lp_cartesian_curve_gen();
	lp_cartesian_curve_bool(m_curve, LP_CARTESIAN_CURVE_DOT_SHOW, true);
	lp_cartesian_curve_bool(m_curve, LP_CARTESIAN_CURVE_LINE_SHOW, false);
}

BsOscChannel *BsOscPlotChanChan::searchChannel(bool physical, int id)
{
	if (m_channelManager == Q_NULLPTR) {
		return Q_NULLPTR;
	}

	if (physical) {
		if (id < m_channelManager->m_physical_channel.count()) {
			return m_channelManager->m_physical_channel.at(id);
		}
	} else {
		Q_FOREACH(BsOscChannelVirtual *channel,
					m_channelManager->m_virtual_channel) {
			if (channel->id == id) {
				return channel;
			}
		}
	}

	return Q_NULLPTR;
}

void BsOscPlotChanChan::updateAxisUnit(lp_cartesian_axis *axis, bool physical, int id)
{
	if (axis == NULL) {
		return;
	}

	std::string unit;
	const BsOscChannel *chan = searchChannel(physical, id);
	if (chan != Q_NULLPTR) {
		if (!chan->unit.isEmpty()) {
			unit = chan->unit.toStdString();
		}
	}

	lp_cartesian_axis_pointer(axis,
		LP_CARTESIAN_AXIS_VALUE_APPEND, (void *) unit.c_str());
}

void BsOscPlotChanChan::setX(bool physical, int id)
{
	m_x.physical = physical;
	m_x.id = id;
	updateAxisUnit(m_axis_bottom, physical, id);
	update();
}

void BsOscPlotChanChan::setY(bool physical, int id)
{
	m_y.physical = physical;
	m_y.id = id;
	updateAxisUnit(m_axis_left, physical, id);
	update();
}

void BsOscPlotChanChan::channelDataChanged()
{
	updateAxisUnit(m_axis_bottom, m_x.physical, m_x.id);
	updateAxisUnit(m_axis_left, m_y.physical, m_y.id);
	update();
}

/**
 * @thread{opengl}
 */
static void draw_curve(lp_cartesian *cartesian, lp_cartesian_curve *curve,
	BsOscChannel *x, BsOscChannel *y)
{
	/* NOTE: we are ignore channel visible field */

	if (!x->domain.time.valid || !y->domain.time.valid) {
		/* no data to plot */
		return;
	}

	x->lock();

	/* Mutex is initalized with QMutex::NonRecursive,
	 * so if multiple time the lock is hold,
	 * it will cause it to block for infinite.
	 *
	 * As per Qt docs: "Recursive mutexes are slower and take
	 *  more memory than non-recursive ones."
	 * so, we will not use QMutex::Recursive here.
	 */
	if (y != x) {
		y->lock();
	}

	/* X axis data */
	lp_cartesian_curve_data(curve, LP_CARTESIAN_CURVE_X,
		0, GL_FLOAT, sizeof(float), x->domain.time.y, x->domain.time.valid);

	/* Y axis data */
	lp_cartesian_curve_data(curve, LP_CARTESIAN_CURVE_Y,
		0, GL_FLOAT, sizeof(float), y->domain.time.y, y->domain.time.valid);

	/* set line width */
	lp_cartesian_curve_float(curve,
		LP_CARTESIAN_CURVE_DOT_WIDTH, y->line_width);

	/* set color */
	qreal r, g, b, a;
	y->color.getRgbF(&r, &g, &b, &a);
	lp_cartesian_curve_4float(curve, LP_CARTESIAN_CURVE_DOT_COLOR, r, g, b, 0.5);

	lp_cartesian_draw_curve(cartesian, curve);

	x->unlock();

	if (x != y) {
		y->unlock();
	}
}

/**
 * @thread{opengl}
 */
void BsOscPlotChanChan::paintGL()
{
	Cartesian::paintGL();

	lp_cartesian_draw_start(m_cartesian);

	if (m_channelManager != Q_NULLPTR) {
		/* search for the channels */
		BsOscChannel *x = searchChannel(m_x.physical, m_x.id);
		BsOscChannel *y = searchChannel(m_y.physical, m_y.id);

		if (x != Q_NULLPTR && y != Q_NULLPTR) {
			draw_curve(m_cartesian, m_curve, x, y);
		}
	}

	lp_cartesian_draw_end(m_cartesian);
}
