/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "time_domain.h"
#include "widgets/osc/osc.h"
#include <iostream>
#include <QToolTip>
#include <QDebug>
#include "extra/box0/box0.h"

BsOscPlotTimeDomain::BsOscPlotTimeDomain(QWidget *parent) :
	Cartesian(parent)
{
}

BsOscPlotTimeDomain::~BsOscPlotTimeDomain()
{
	if (m_curve != NULL) lp_cartesian_curve_del(m_curve);
	if (m_line != NULL) lp_line_del(m_line);
}

void BsOscPlotTimeDomain::setModule(b0_ain *module)
{
	m_module = module;
	updateBothAxis();
}

void BsOscPlotTimeDomain::setChannelManager(BsOscChannelManager *channelManager)
{
	m_channelManager = channelManager;
	connect(m_channelManager, SIGNAL(channelDataChanged()), SLOT(update()));
	updateBothAxis();
}

void BsOscPlotTimeDomain::updateBothAxis()
{
	if (m_axis_bottom != NULL) {
		lp_cartesian_axis_2float(m_axis_bottom,
			LP_CARTESIAN_AXIS_VALUE_RANGE, 0, m_timeDuration);
	}

	if (m_module != NULL && m_axis_left != NULL) {
		lp_cartesian_axis_2float(m_axis_left,
			LP_CARTESIAN_AXIS_VALUE_RANGE,
			m_module->ref.low, m_module->ref.high);

		std::string unit = ref_type_to_unit(m_module->ref.type).toStdString();
		lp_cartesian_axis_pointer(m_axis_left,
			LP_CARTESIAN_AXIS_VALUE_APPEND, (void *) unit.c_str());
	}
}

void BsOscPlotTimeDomain::configure(qreal timeDuration)
{
	m_timeDuration = timeDuration;
	updateBothAxis();
}

void BsOscPlotTimeDomain::initializeGL()
{
	Cartesian::initializeGL();

	lp_cartesian_axis_pointer(m_axis_bottom,
		LP_CARTESIAN_AXIS_VALUE_APPEND, (void *) "s");

	m_curve = lp_cartesian_curve_gen();

	m_line = lp_line_gen();
	lp_line_4float(m_line, LP_LINE_COLOR, 0, 0, 0, 0.3);
	lp_line_float(m_line, LP_LINE_WIDTH, 3);

	updateBothAxis();
}

/**
 * @thread{opengl}
 */
static qreal draw_curve(lp_cartesian *cartesian, lp_cartesian_curve *curve,
	BsOscChannel *channel)
{
	if (!channel->visible) {
		return 0;
	}

	if (!channel->domain.time.valid) {
		/* no data to plot */
		return 0;
	}

	channel->lock();

	/* X axis data */
	lp_cartesian_curve_data(curve, LP_CARTESIAN_CURVE_X, 0, GL_FLOAT,
		sizeof(float), channel->domain.time.x, channel->domain.time.valid);

	/* Y axis data */
	lp_cartesian_curve_data(curve, LP_CARTESIAN_CURVE_Y, 0, GL_FLOAT,
		sizeof(float), channel->domain.time.y, channel->domain.time.valid);

	/* set line width */
	lp_cartesian_curve_float(curve,
		LP_CARTESIAN_CURVE_LINE_WIDTH, channel->line_width);

	/* set color */
	qreal r, g, b, a;
	channel->color.getRgbF(&r, &g, &b, &a);
	lp_cartesian_curve_4float(curve, LP_CARTESIAN_CURVE_LINE_COLOR, r, g, b, a);

	lp_cartesian_draw_curve(cartesian, curve);

	qreal lastest_value = 0;
	size_t lastest_index = channel->domain.time.index;
	if (lastest_index) {
		/* index past this is having newest data */
		lastest_value = channel->domain.time.x[lastest_index - 1];
	}

	channel->unlock();

	return lastest_value;
}

/**
 * @thread{opengl}
 */
void BsOscPlotTimeDomain::paintGL()
{
	Cartesian::paintGL();

	lp_cartesian_draw_start(m_cartesian);

	/* the position of the line that shows the location of current sample */
	float line_x = 0;

	if (m_channelManager != Q_NULLPTR) {
		Q_FOREACH(BsOscChannel *channel, m_channelManager->m_physical_channel) {
			float tmp = draw_curve(m_cartesian, m_curve, channel);
			line_x = qMax(tmp, line_x);
		}

		Q_FOREACH(BsOscChannel *channel, m_channelManager->m_virtual_channel) {
			draw_curve(m_cartesian, m_curve, channel);
		}
	}

	/* the line (vertical) positioned at line_x */
	lp_line_2float(m_line, LP_LINE_START, line_x, -INFINITY);
	lp_line_2float(m_line, LP_LINE_END, line_x, +INFINITY);
	lp_cartesian_draw_line(m_cartesian, m_line);

	lp_cartesian_draw_end(m_cartesian);
}
