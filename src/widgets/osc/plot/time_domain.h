/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_OSC_PLOT_TIME_DOMAIN_H
#define BOX0_STUDIO_OSC_PLOT_TIME_DOMAIN_H

#include <QObject>
#include <QList>
#include "extra/replot/cartesian.h"
#include "widgets/osc/channel_manager/channel_manager.h"

class BsOscPlotTimeDomain : public Cartesian
{
	Q_OBJECT

	public:
		BsOscPlotTimeDomain(QWidget *parent);
		~BsOscPlotTimeDomain();

		void configure(qreal timeDuration);
		void setModule(b0_ain *module);
		void setChannelManager(BsOscChannelManager *channelManager);

	protected:
		void initializeGL() Q_DECL_OVERRIDE;
		void paintGL() Q_DECL_OVERRIDE;

	private:
		void updateBothAxis();
		lp_cartesian_curve *m_curve = NULL;
		lp_line *m_line = NULL;
		b0_ain *m_module = NULL;
		BsOscChannelManager *m_channelManager = Q_NULLPTR;
		float m_timeDuration = 1;
};

#endif

