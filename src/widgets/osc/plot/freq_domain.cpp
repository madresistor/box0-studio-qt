/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "freq_domain.h"
#include "widgets/osc/osc.h"
#include <QFont>
#include <QDebug>
#include "extra/box0/box0.h"

BsOscPlotFreqDomain::BsOscPlotFreqDomain(QWidget *parent) :
	Cartesian(parent)
{
}

BsOscPlotFreqDomain::~BsOscPlotFreqDomain()
{
	if (m_curve != NULL) lp_cartesian_curve_del(m_curve);
}

void BsOscPlotFreqDomain::configure(qreal speed, const QList<uint> &chan_seq)
{
	size_t chanSeq_size = (size_t) chan_seq.count();

	if (m_axis_bottom != NULL) {
		lp_cartesian_axis_2float(m_axis_bottom,
			LP_CARTESIAN_AXIS_VALUE_RANGE, 0, speed / (chanSeq_size * 2));
	}

	/* set the y axis MIN = 0; */
	if (m_axis_left != NULL) {
		float min, max;
		lp_get_cartesian_axis_2float(m_axis_left,
			LP_CARTESIAN_AXIS_VALUE_RANGE, &min, &max);
		min = -0.3;
		lp_cartesian_axis_2float(m_axis_left,
			LP_CARTESIAN_AXIS_VALUE_RANGE, min, max);
	}
}

void BsOscPlotFreqDomain::setChannelManager(BsOscChannelManager *channelManager)
{
	m_channelManager = channelManager;

	connect(m_channelManager, SIGNAL(channelDataChanged()), SLOT(update()));
}

void BsOscPlotFreqDomain::initializeGL()
{
	Cartesian::initializeGL();

	lp_cartesian_axis_pointer(m_axis_bottom,
		LP_CARTESIAN_AXIS_VALUE_APPEND, (void *) "Hz");

	updateYAxisUnit();

	m_curve = lp_cartesian_curve_gen();
}

/**
 * @thread{opengl}
 */
static void draw_curve(lp_cartesian *cartesian, lp_cartesian_curve *curve,
	BsOscChannel *channel)
{
	if (!channel->visible) {
		return;
	}

	if (!channel->domain.freq.valid) {
		/* no data to plot */
		return;
	}

	channel->lock();

	/* X axis data */
	lp_cartesian_curve_data(curve, LP_CARTESIAN_CURVE_X, 0, GL_FLOAT,
		sizeof(float), channel->domain.freq.x, channel->domain.freq.valid);

	/* Y axis data */
	lp_cartesian_curve_data(curve, LP_CARTESIAN_CURVE_Y, 0, GL_FLOAT,
		sizeof(float), channel->domain.freq.y, channel->domain.freq.valid);

	/* set line width */
	lp_cartesian_curve_float(curve, LP_CARTESIAN_CURVE_LINE_WIDTH,
		channel->line_width);

	/* set color */
	qreal r, g, b, a;
	channel->color.getRgbF(&r, &g, &b, &a);
	lp_cartesian_curve_4float(curve, LP_CARTESIAN_CURVE_LINE_COLOR, r, g, b, a);

	lp_cartesian_draw_curve(cartesian, curve);

	channel->unlock();
}

/**
 * @thread{opengl}
 */
void BsOscPlotFreqDomain::paintGL()
{
	Cartesian::paintGL();

	lp_cartesian_draw_start(m_cartesian);

	if (m_channelManager != Q_NULLPTR) {
		Q_FOREACH(BsOscChannel *channel, m_channelManager->m_physical_channel) {
			draw_curve(m_cartesian, m_curve, channel);
		}

		Q_FOREACH(BsOscChannel *channel, m_channelManager->m_virtual_channel) {
			draw_curve(m_cartesian, m_curve, channel);
		}
	}

	lp_cartesian_draw_end(m_cartesian);
}

/**
 * @note assume makeCurrent() has already been called
 */
void BsOscPlotFreqDomain::updateYAxisUnit()
{
	if (m_module == NULL || m_axis_left == NULL) {
		return;
	}

	std::string unit = ref_type_to_unit(m_module->ref.type).toStdString();
	lp_cartesian_axis_pointer(m_axis_left,
		LP_CARTESIAN_AXIS_VALUE_APPEND, (void *) unit.c_str());
}

void BsOscPlotFreqDomain::setModule(b0_ain *module)
{
	m_module = module;

	if (m_axis_left != NULL) {
		makeCurrent();
		updateYAxisUnit();
		doneCurrent();
	}

	update();
}
