/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "osc.h"
#include "extra/box0/result_code.h"
#include <QDebug>

void BsOsc::stream_constructor(const QFont &font)
{
	/* speed selector */
	stream_ui_speed = new BsSpeed(this);
	stream_ui_speed->setMinimumSize(100, 32);
	stream_ui_speed->setFont(font);
	stream_ui_speed_action = ui.toolBar->addWidget(stream_ui_speed);

	/* when user change stream speed, perform a restart */
	connect(stream_ui_speed, SIGNAL(currentIndexChanged(int)), ui_hw_run, SLOT(restart()));

	/* stream sampler
	 *  note: DirectConnection = calculate results in sample reader thread */
	stream_sampler = new BsAinStreamSampler(this);
	connect(stream_sampler, SIGNAL(output(float *, uint)),
			SLOT(stream_feed(float *, uint)), Qt::DirectConnection);
}

/**
 * Called when user change bitsize
 * @param value New bitsize value
 */
void BsOsc::stream_bitsizeChanged(uint bitsize)
{
	stream_ui_speed->clear();

	struct b0_ain::b0_ain_mode_bitsize_speeds::b0_ain_bitsize_speeds
		*bitsize_speeds = NULL, *item;
	for (size_t i = 0; i < module->stream.count; i++) {
		item = &module->stream.values[i];
		if (item->bitsize == bitsize) {
			bitsize_speeds = item;
			break;
		}
	}

	Q_ASSERT(bitsize_speeds != NULL);

	for (size_t i = 0; i < bitsize_speeds->speed.count; i++) {
		stream_ui_speed->addValue(bitsize_speeds->speed.values[i]);
	}

	/* set index to invalid state */
	stream_ui_speed->setCurrentIndex(0);

	ui_hw_run->restart();
}

void BsOsc::stream_setAcquisitionMode(bool stream)
{
	/* show/hide all stream specific elements */
	stream_ui_speed_action->setVisible(stream);

	if (!stream) {
		return;
	}

	/* prepare the module for stream mode */
	b0_result_code r = b0_ain_stream_prepare(module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Prepare failed"));
		rc.setMessage(tr("Unable to prepare for stream mode"));
		rc.exec();
		deleteLater();
		return;
	}

	/* insert the bitsizes */
	for (size_t i = 0; i < module->stream.count; i++) {
		ui_bitsize->addValue(module->stream.values[i].bitsize);
	}

	stream_bitsizeChanged(ui_bitsize->currentValue());
}

/**
 * @thread{sampler}
 */
void BsOsc::stream_feed(float *data, uint size)
{
	/* pre-process */
	ui.channelManager->applyCalib(data, size);

	/* average */
	size = ui.configAverage->stream_process(data, size);

	/* filter */
	ui.configFilter->stream_process(data, size);

	bool visible_fft = ui.plotFreqDomain->isVisible();
	bool visible_measure = ui.measure->isVisible();
	bool perform_fft = visible_fft || visible_measure;

	/* store to channels */
	ui.channelManager->stream_process(data, size, perform_fft);

	/* perform measurements */
	if (visible_measure) {
		ui.measure->stream_process(data, size);
	}

	/* visualization */
	ui.plotTimeDomain->update();

	/* fft */
	if (visible_fft) {
		ui.plotFreqDomain->update();
	}

	/* chan vs chan */
	BsOscPlotChanChan *chan_chan = ui.chanChan->plot();
	if (chan_chan->isVisible()) {
		chan_chan->update();
	}

	/* delete the data */
	stream_sampler->dispose(data);
}

bool BsOsc::stream_start(const QList<uint> &channelSequence, uint bitsize)
{
	b0_result_code r;
	unsigned long speed = stream_ui_speed->currentValue();

	Q_ASSERT(speed > 0);
	qDebug() << "speed" << speed;

	/* prepare the device for stream operations */
	r = b0_ain_stream_prepare(module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Stream prepare failed"));
		rc.setMessage(tr("Unable to prepeare AIN module for stream mode"));
		rc.exec();
		return false;
	}

	/* convert channelSequence to uint8_t array */
	size_t chan_size = channelSequence.count();
	unsigned int _seq[chan_size];
	for (size_t i = 0; i < chan_size; i++) {
		_seq[i] = channelSequence.at(i);
	}

	/* apply the channel sequence to device */
	r = b0_ain_chan_seq_set(module, _seq, chan_size);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Channel sequence set"));
		rc.setMessage(tr("Unable to apply channel sequence to module"));
		rc.exec();
		return false;
	}

	r = b0_ain_bitsize_speed_set(module, bitsize, speed);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Bitsize,Speed set"));
		rc.setMessage(tr("Unable to set bitsize,speed to module"));
		rc.exec();
		return false;
	}

	/* start the module */
	r = b0_ain_stream_start(module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Start failed"));
		rc.setMessage(tr("Unable to start AIN module in stream mode"));
		rc.exec();
		return false;
	}

	/* we are ready to accept data */
	uint samples_per_callback;
	uint updateRate = ui.configUpdateRate->updateRate();
	samples_per_callback = speed / updateRate;
	samples_per_callback -= samples_per_callback % channelSequence.size();
	qDebug() << "requested samples_per_callback is " << samples_per_callback;

	/* apply the configuration on graphs */
	uint average = ui.configAverage->average();
	qreal sampling_freq = speed / (qreal) average;
	qreal window_size =  ui.channelManager->stream_timeWindowSize();
	ui.plotTimeDomain->configure(window_size);
	ui.plotFreqDomain->configure(sampling_freq, channelSequence);

	/* initalize operations */
	ui.configAverage->stream_prepare(chan_size);
	ui.configFilter->stream_prepare(sampling_freq, chan_size);
	ui.channelManager->stream_prepare(sampling_freq, channelSequence);
	ui.measure->stream_prepare(sampling_freq, bitsize, module->ref.low,
				module->ref.high, channelSequence);

	/* start the sampler thread */
	stream_sampler->start(module, samples_per_callback);

	return true;
}

void BsOsc::stream_stop()
{
	/* signal and wait for the thread to exit */
	stream_sampler->requestInterruption();
	stream_sampler->wait();

	/* send a stop */
	BsResultCode::handleInternal(b0_ain_stream_stop(module),
		"Unable to stop ain module");

	ui.configAverage->stream_done();
	ui.configFilter->stream_done();
	ui.channelManager->stream_done();
	ui.measure->stream_done();
}
