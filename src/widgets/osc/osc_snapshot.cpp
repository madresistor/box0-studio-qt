/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "osc.h"
#include "extra/box0/result_code.h"
#include <QDebug>

void BsOsc::snapshot_constructor(const QFont &font)
{
	snapshot_ui_speed = new QSpinBox(this);
	//snapshot_ui_speed->setKeyboardTracking(false);
	snapshot_ui_speed->setMinimumSize(100, 32);
	snapshot_ui_speed->setFont(font);
	snapshot_ui_speed_action = ui.toolBar->addWidget(snapshot_ui_speed);

	connect(snapshot_ui_speed, SIGNAL(editingFinished()),
			SLOT(snapshot_speedEditFinished()));

	//ui.snapshot_speedSlider->setTracking(false);
	connect(ui.snapshot_speedSlider, SIGNAL(valueChanged(int)),
			SLOT(snapshot_speedSlider(int)));

	/* snapshot sampler
	 *  note: DirectConnection = calculate results in sample reader thread */
	snapshot_sampler = new BsAinSnapshotSampler(this);
	connect(snapshot_sampler,
		SIGNAL(output(unsigned long, unsigned int, float *, uint)),
		SLOT(snapshot_feed(unsigned long, unsigned int, float *, uint)),
		Qt::DirectConnection);
}

/**
 * Convert speed to readable string representation
 * @param value Frequency (Hz)
 * @param return a readable form (with SI prefix)
 */
QString speed_to_string(unsigned long value)
{
	if (value >= 1E6) {
		return QString("%1 Mhz").arg(value / 1.0E6);
	} else if (value >= 1E3) {
		return QString("%1 Khz").arg(value / 1.0E3);
	} else {
		return QString("%1 Hz").arg(value);
	}
}

static struct b0_ain::b0_ain_mode_bitsize_speeds::b0_ain_bitsize_speeds *
	search_bitsize_speeds(b0_ain *module, uint bitsize)
{
	struct b0_ain::b0_ain_mode_bitsize_speeds::b0_ain_bitsize_speeds
		*item;
	for (size_t i = 0; i < module->snapshot.count; i++) {
		item = &module->snapshot.values[i];
		if (item->bitsize == bitsize) {
			return item;
		}
	}

	return NULL;
}

/**
 * Slot calle when user change bitsize
 * @param value New bitsize value
 */
void BsOsc::snapshot_bitsizeChanged(uint bitsize)
{
	/* assuming the range (min_index, max_index) of selected value is contineous */
	unsigned long max, min;
	unsigned long calc_min;

	/*
	 * calc_min is calculated so we can exclude those speed that
	 *  will take more time than UPDATE_INTERVAL
	 *
	 * if this exclusion is not performed, the software
	 *  will go into undeterministic state
	 */
	unsigned int byte_per_sample = (bitsize + 7) / 8;
	unsigned int update_intv = snapshot_sampler->updateInterval() + 50; /* 50ms added for worst case */
	calc_min = (module->buffer_size * 1000.0) / (byte_per_sample * update_intv);
	min = calc_min;
	max = calc_min;

	qDebug() << "calculated minimum speed:" << calc_min;

	struct b0_ain::b0_ain_mode_bitsize_speeds::b0_ain_bitsize_speeds
		*bitsize_speeds = search_bitsize_speeds(module, bitsize);

	Q_ASSERT(bitsize_speeds != NULL);

	int max_index = -1, min_index = -1;
	for (size_t i = 0; i < bitsize_speeds->speed.count; i++) {
		unsigned long val = bitsize_speeds->speed.values[i];

		/* ignore speed less than calculated-minimum */
		if (val < calc_min) {
			continue;
		}

		if (min_index < 0 || val <= min) {
			min = val;
			min_index = i;
		}

		if (val >= max) {
			max = val;
			max_index = i;
		}
	}

	if (max_index < 0 || min_index < 0) {
		qCritical() << "unable to find a suitable speed range";
		return;
	}

	qDebug() << "actual minimum speed:" << min;
	qDebug() << "actual maximum speed:" << max;

	ui.snapshot_speedSlider->setRange(qMin(min_index, max_index), qMax(min_index, max_index));
	ui.snapshot_speedMin->setText(speed_to_string(min));
	ui.snapshot_speedMax->setText(speed_to_string(max));
	snapshot_ui_speed->setRange(min, max);
	snapshot_ui_speed->setValue(max);
	ui.snapshot_speedSlider->setValue(max_index);

	ui_hw_run->restart();
}

void BsOsc::snapshot_setAcquisitionMode(bool stream)
{
	/* show/hide all stream specific elements */
	snapshot_ui_speed_action->setVisible(!stream);
	ui.snapshot_speedFrame->setVisible(!stream);

	if (stream) {
		return;
	}

	/* prepare the module for snapshot mode */
	b0_result_code r = b0_ain_snapshot_prepare(module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Prepare failed"));
		rc.setMessage(tr("Unable to prepare for snapshot mode"));
		rc.exec();
		deleteLater();
		return;
	}

	/* set the bitsizes */
	for (size_t i = 0; i < module->snapshot.count; i++) {
		uint bs = module->snapshot.values[i].bitsize;
		ui_bitsize->addValue(bs);
	}

	snapshot_bitsizeChanged(ui_bitsize->currentValue());
}

bool BsOsc::snapshot_start(const QList<uint> &channelSequence, uint bitsize)
{
	b0_result_code r;

	unsigned long speed = (unsigned long) snapshot_ui_speed->value();
	qDebug() << "Speed" << speed;

	size_t chan_size = channelSequence.size();
	unsigned int _seq[chan_size];
	for (size_t i = 0; i < chan_size; i++) {
		_seq[i] = channelSequence.at(i);
	}

	/* apply the channel sequence to device */
	r = b0_ain_chan_seq_set(module, _seq, chan_size);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Channel sequence"));
		rc.setMessage(tr("Unable to apply channel sequence to module"));
		rc.exec();
		return false;
	}

	r = b0_ain_bitsize_speed_set(module, bitsize, speed);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Bitsize,Speed set"));
		rc.setMessage(tr("Unable to set bitsize,speed to module"));
		rc.exec();
		return false;
	}

	ui.configAverage->snapshot_prepare(chan_size);
	ui.configFilter->snapshot_prepare(chan_size);
	ui.channelManager->snapshot_prepare(channelSequence);
	ui.measure->snapshot_prepare(channelSequence, module->ref.low, module->ref.high);

	m_snapshot_feed_last_speed = 0;

	snapshot_sampler->start(module, bitsize, speed);

	return true;
}

void BsOsc::snapshot_stop()
{
	/* wait for thread to exit */
	snapshot_sampler->requestInterruption();
	snapshot_sampler->wait();

	ui.configAverage->snapshot_done();
	ui.configFilter->snapshot_done();
	ui.channelManager->snapshot_done();
	ui.measure->snapshot_done();
}

/**
 * Common code for speed input as well as speed slider.
 * This will read the speed value from speed input and set to device
 */
void BsOsc::snapshot_speedChanged()
{
	/* check if the speed is in list */

	ui_hw_run->restart();

	/* TODO: tell plots (fft and plot) to update the X Axis values */
}

/**
 * slot called from snapshot sampler when data is ready
 * @param speed Speed at which data was sampled
 * @param data pointer to data
 * @param size number of item in @a data
 * @thread{sampler}
 */
void BsOsc::snapshot_feed(unsigned long _speed, unsigned int bitsize,
					float *data, uint size)
{
	/* pre-process */
	ui.channelManager->applyCalib(data, size);

	/* average */
	qreal speed = _speed / (qreal) ui.configAverage->average();
	size = ui.configAverage->snapshot_process(data, size);

	/* filter */
	ui.configFilter->snapshot_process(speed, data, size);

	bool visible_fft = ui.plotFreqDomain->isVisible();
	bool visible_measure = ui.measure->isVisible();
	bool perform_fft = visible_fft || visible_measure;

	/* store to channels */
	ui.channelManager->snapshot_process(speed, data, size, perform_fft);

	/* perform measurement */
	if (visible_measure) {
		ui.measure->snapshot_process(speed, bitsize, data, size);
	}

	if (m_snapshot_feed_last_speed != speed) {
		m_snapshot_feed_last_speed = speed;
		ui.plotTimeDomain->configure(size / speed);
		ui.plotFreqDomain->configure(speed, m_channelSequence);
	}

	/* visualization */
	ui.plotTimeDomain->update();

	/* fft */
	if (visible_fft) {
		ui.plotFreqDomain->update();
	}

	/* chan vs chan */
	BsOscPlotChanChan *chan_chan = ui.chanChan->plot();
	if (chan_chan->isVisible()) {
		chan_chan->update();
	}

	/* delete the data */
	snapshot_sampler->dispose(data);
}

/**
 * slot called when user interact with the speed slider
 * @param value new slider value
 * @note @a value is actually a index of @a module->speed->values
 */
void BsOsc::snapshot_speedSlider(int value)
{
	if (module == NULL) {
		qWarning() << "module is null";
		return;
	}

	unsigned int bitsize = (unsigned int) ui_bitsize->currentValue();
	struct b0_ain::b0_ain_mode_bitsize_speeds::b0_ain_bitsize_speeds
		*bitsize_speeds = search_bitsize_speeds(module, bitsize);

	Q_ASSERT(bitsize_speeds != NULL);

	if (value < 0 || ((size_t)value) >= bitsize_speeds->speed.count) {
		qWarning() << "speedSlider value is invalid";
		return;
	}

	unsigned long speed = bitsize_speeds->speed.values[value];
	snapshot_ui_speed->setValue(speed);
	snapshot_speedChanged();

	/* TODO: incase it fails, reset to previous value */
}

/**
 * slot called when user is editing the speed number input (in toolbar)
 * Search for the index that matches with the speeed
 */
void BsOsc::snapshot_speedEditFinished()
{
	if (module == NULL) {
		qWarning() << "module is null";
		return;
	}

	unsigned int bitsize = (unsigned int) ui_bitsize->currentValue();
	struct b0_ain::b0_ain_mode_bitsize_speeds::b0_ain_bitsize_speeds
			*bitsize_speeds = search_bitsize_speeds(module, bitsize);

	Q_ASSERT(bitsize_speeds != NULL);

	unsigned long speed = (unsigned long) snapshot_ui_speed->value();
	for (unsigned i = 0; i < bitsize_speeds->speed.count; i++) {
		if (speed == bitsize_speeds->speed.values[i]) {
			ui.snapshot_speedSlider->setValue(i);
			return;
		}
	}

	qCritical() << "speed (" << speed << ")" << " matching index not found";
}
