/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "osc.h"
#include <Qt>
#include <QSettings>
#include <QComboBox>
#include "plot/time_domain.h"
#include "extra/widget/window_func.h"
#include "extra/box0/result_code.h"
#include <QDebug>
#include "config/average.h"
#include "config/filter.h"
#include "config/update_rate.h"
#include "config/window_func.h"
#include <libbox0/misc/box0-v5/box0-v5.h>
#include <libbox0/misc/box0-v5/calib.h>

#ifdef Q_WS_WIN
# include <QSysInfo>
#endif

BsOsc::BsOsc(QWidget *parent) :
	BsInstrument(B0_AIN, parent)
{
	/* UI things */
	ui.setupUi(this);

	ui.plotTimeDomain->setChannelManager(ui.channelManager);
	ui.plotFreqDomain->setChannelManager(ui.channelManager);
	ui.chanChan->setChannelManager(ui.channelManager);
	ui.measure->setChannelManager(ui.channelManager);

	/* Start/Stop button */
	ui_hw_run = new BsHwRun(this);
	ui.toolBar->addAction(ui_hw_run);
	connect(ui_hw_run, SIGNAL(userWantToStart()), SLOT(start()));
	connect(ui_hw_run, SIGNAL(userWantToStop()), SLOT(stop()));

	ui.toolBar->addSeparator();

	/* font for bitsize dropown and speed dropdown */
	QFont font;
	font.setPointSize(10);

	/* mode selector */
	ui_mode = new QComboBox(this);
	ui_mode->setMinimumSize(100, 32);
	ui_mode->setFont(font);
	ui.toolBar->addWidget(ui_mode);

	/* when mode change, update the ui */
	connect(ui_mode, SIGNAL(currentIndexChanged(int)),
		SLOT(acquisitionModeChanged()));

	/* bitsize selector */
	ui_bitsize = new BsBitsize(this);
	ui_bitsize->setMinimumSize(100, 32);
	ui_bitsize->setFont(font);
	ui.toolBar->addWidget(ui_bitsize);

	/* start of snapshot */
	connect(ui_bitsize, SIGNAL(currentValueChanged(uint)),
		SLOT(bitsizeChanged(uint)));

	snapshot_constructor(font);
	stream_constructor(font);

	/* background color of window */
	QBrush brush(QColor(0xCC, 0xCC, 0xCC));
	brush.setStyle(Qt::SolidPattern);
	QPalette palette;
	palette.setBrush(QPalette::Active, QPalette::Window, brush);
	palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
	palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
	palette.setBrush(QPalette::Active, QPalette::Base, brush);
	palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
	palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
	setPalette(palette);

	//ui.toolBar->addSeparator();

	/* qtcentre.org/threads/41016 */
	QWidget *spacerHoriz = new QWidget(ui.toolBar);
	QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	spacerHoriz->setSizePolicy(sizePolicy);
	ui.toolBar->addWidget(spacerHoriz);

	/* chan vs chan */
	ui.dockChanChan->setVisible(false);
	QAction *dockLeftAction = ui.dockChanChan->toggleViewAction();
	dockLeftAction->setIcon(QIcon(":/dock/left"));
	ui.menuView->addAction(dockLeftAction);
	ui.toolBar->addAction(dockLeftAction);

	/* fft */
	ui.dockPlotFreqDomain->setVisible(true);
	QAction *dockBottomAction = ui.dockPlotFreqDomain->toggleViewAction();
	dockBottomAction->setIcon(QIcon(":/dock/bottom"));
	ui.menuView->addAction(dockBottomAction);
	ui.toolBar->addAction(dockBottomAction);

	/* right channels */
	ui.dockRight->setVisible(true);
	QAction *dockRightAction = ui.dockRight->toggleViewAction();
	dockRightAction->setIcon(QIcon(":/dock/right"));
	ui.menuView->addAction(dockRightAction);
	ui.toolBar->addAction(dockRightAction);

	/* measure */
	ui.dockMeasure->setVisible(false);
	QAction *dockMeasureAction = ui.dockMeasure->toggleViewAction();
	ui.menuView->addAction(dockMeasureAction);

	connect(ui.configWindowFunc, SIGNAL(windowFuncChanged(BsWindowFunc::Type)),
		ui.channelManager, SLOT(setWindowFunc(BsWindowFunc::Type)));

	connect(ui.configGraph, SIGNAL(lineWidthChanged(double)),
		ui.channelManager, SLOT(setLineWidth(double)));

	connect(ui.configGraph, SIGNAL(flowPlotChanged(bool)),
		ui.channelManager, SLOT(setFlowPlot(bool)));

	/* set for first time */
	ui.channelManager->setLineWidth(ui.configGraph->lineWidth());
	ui.channelManager->setFlowPlot(ui.configGraph->flowPlot());

	/* things that perform restart */
	connect(ui.channelManager, SIGNAL(channelSequenceChanged()), ui_hw_run, SLOT(restart()));
	connect(ui.configAverage, SIGNAL(performRestart()), ui_hw_run, SLOT(restart()));
	connect(ui.configUpdateRate, SIGNAL(performRestart()), ui_hw_run, SLOT(restart()));
}

void BsOsc::acquisitionModeChanged()
{
	bool running = ui_hw_run->isRunning();
	if (running) {
		stop();
	}

	/* we need to change mode only, no restart */
	ui_bitsize->clear();
	bool stream = ui_mode->currentData().toBool();
	qDebug() << "switching to" << (stream ? "Stream" : "Snapshot");
	snapshot_setAcquisitionMode(stream);
	stream_setAcquisitionMode(stream);

	/* restore the state of running */
	if (running) {
		start();
	}
}

/**
 * Read calibration from device
 * It check if the device is box0-v5 and read the calibration data for AIN0
 */
static void read_calib(b0_device *dev, b0_ain *mod,
			QList<BsOscChannelPhysical *> &chans)
{
	/* reset previous calib */
	for (int i = 0; i < chans.count(); i++) {
		BsOscChannelPhysical *ch = chans.at(i);
		ch->calib.gain = 1;
		ch->calib.offset = 0;
	}

	if (B0_ERR_RC(b0v5_valid_test(dev))) {
		/* not box0-v5 */
		return;
	}

	if (mod->header.index != 0) {
		/* box0-v5 only have AIN0 calib data */
		return;
	}

	/* read calibration data */
	b0v5_calib_value values[mod->chan_count];
	b0v5_calib calib = {
		/* .ain0 = */ {
			/* .values = */ values,
			/* .count = */ mod->chan_count
		},

		/* .aout0 = */ {
			/* .values = */ NULL,
			/* .count = */ 0
		}
	};

	if (B0_ERR_RC(b0v5_calib_read(dev, &calib))) {
		qWarning("Unable to read box0-v5 AIN0 calib");
		return;
	}

	for (int i = 0; i < chans.count(); i++) {
		BsOscChannelPhysical *ch = chans.at(i);
		ch->calib.gain = values[i].gain;
		ch->calib.offset = values[i].offset;
	}
}

void BsOsc::loadModule(b0_device *dev, int index)
{
	b0_result_code r;

	/* search for module to bind to */
	r = b0_ain_open(dev, &module, index);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("AIN"));
		rc.setMessage(tr("Open failed"));
		rc.exec();
		deleteLater();
		return;
	}

	ui.plotTimeDomain->setModule(module);
	ui.plotFreqDomain->setModule(module);
	ui.channelManager->setModule(module);

	showModuleInTitle(&module->header);

	/* Read calibration data */
	read_calib(dev, module, ui.channelManager->m_physical_channel);

	/* setup mode */
	ui_mode->clear();

	const bool show_snapshot_first =
#ifdef Q_WS_WIN
		/* before Windows 8.1 streaming have problem */
		(QSysInfo::windowsVersion() < QSysInfo::WV_WINDOWS8_1)
#else
		false;
#endif

	if (show_snapshot_first) {
		if (module->snapshot.count > 0) {
			ui_mode->addItem("Snapshot", false);
		}
	}

	if (module->stream.count > 0) {
		ui_mode->addItem("Stream", true);
	}

	if (!show_snapshot_first) {
		if (module->snapshot.count > 0) {
			ui_mode->addItem("Snapshot", false);
		}
	}

	/* prepare for mode */
	acquisitionModeChanged();
}

void BsOsc::start()
{
	m_channelSequence = ui.channelManager->channelSequence();
	uint bitsize = ui_bitsize->currentValue();
	bool success;

	Q_ASSERT(bitsize > 0);

	if (!m_channelSequence.count()) {
		QMessageBox::critical(this, tr("Nothing selected"),
			tr("No channel has been selected."));
		return;
	}

	qDebug() << "Channel sequence" << m_channelSequence;
	qDebug() << "bitsize" << bitsize;

	m_running_stream = ui_mode->currentData().toBool();

	if (m_running_stream) {
		success = stream_start(m_channelSequence, bitsize);
	} else {
		success = snapshot_start(m_channelSequence, bitsize);
	}

	/* yay, we are running... */
	ui_hw_run->setRunning(success);
}

void BsOsc::stop()
{
	/* the stream/snapshot stop cannot fail! */
	if (m_running_stream) {
		stream_stop();
	} else {
		snapshot_stop();
	}

	ui_hw_run->setRunning(false);
}

/**
 * Method to route bitsize change notification to mode specific code
 * @param value new bitsize value
 * @note this will not route bitsize change event in case
 *  there are no value in bitsize input.
 *  Prevent: when mode is changed,
 *    bitsize values change event could pass on to another mode
 */
void BsOsc::bitsizeChanged(uint value)
{
	Q_ASSERT(value > 0);

	if (!ui_bitsize->count()) {
		/* there is no value to switch to */
		return;
	}

	/* route to appropriate mode function */
	bool stream = ui_hw_run->isRunning() ?
		m_running_stream :
		ui_mode->currentData().toBool();

	if (stream) {
		stream_bitsizeChanged(value);
	} else {
		snapshot_bitsizeChanged(value);
	}
}

void BsOsc::closeEvent(QCloseEvent *event)
{
	if (module != NULL) {
		/* if plotting is in running, stop it */
		if (ui_hw_run->isRunning()) {
			stop();
		}

		/* close the module */
		BsResultCode::handleInternal(b0_ain_close(module),
			"unable to close AIN module");
		module = NULL;
	}

	/* for unknown reason, if the dock is floating and the window is closed.
	 *  the dock is not closed (since parent is closed).
	 * This will force close the dock.
	 */
	ui.dockPlotFreqDomain->close();
	ui.dockRight->close();
	ui.dockChanChan->close();
	ui.dockMeasure->close();

	/* store settings */
	ui.configGraph->storeSetting();

	BsInstrument::closeEvent(event);
}
