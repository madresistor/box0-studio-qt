/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "chan_chan.h"
#include "extra/generic/abstract_item_view.h"

BsOscChanChan::BsOscChanChan(QWidget *parent) :
	QWidget(parent)
{
	ui.setupUi(this);

	connect(ui.selX, SIGNAL(currentIndexChanged(int)), SLOT(xChanged()));
	connect(ui.selY, SIGNAL(currentIndexChanged(int)), SLOT(yChanged()));
}

BsOscPlotChanChan *BsOscChanChan::plot()
{
	return ui.plot;
}

void BsOscChanChan::setChannelManager(BsOscChannelManager *channelManager)
{
	ui.plot->setChannelManager(channelManager);

	/* X list */
	m_model_x = new BsOscChanChanModel(channelManager, ui.selX);
	ui.selX->setModel(m_model_x);
	ui.selX->setCurrentIndex(0);

	/* Y list */
	m_model_y = new BsOscChanChanModel(channelManager, ui.selY);
	ui.selY->setModel(m_model_y);
	ui.selY->setCurrentIndex(1);

	/* clear X axis list when changed */
	connect(channelManager, SIGNAL(channelDataChanged()),
		ui.selX->view(), SLOT(reset()));

	/* clear Y axis list when changed */
	connect(channelManager, SIGNAL(channelDataChanged()),
		ui.selY->view(), SLOT(reset()));
}

/**
 * Called when X Axis channel selection changed
 * This will convert the row to actual channel information.
 *   id and if it is a physical channel or not
 *  and pass on that information to graph
 */
void BsOscChanChan::xChanged()
{
	int id;
	bool physical;

	int row = BsAbstractItemView::selectedRow(ui.selX->view());
	if (row == -1) {
		return;
	}

	if (m_model_x != Q_NULLPTR && m_model_x->rowToInfo(row, physical, id)) {
		ui.plot->setX(physical, id);
	}
}

/**
 * Called when Y Axis channel selection changed
 * This will convert the row to actual channel information.
 *   id and if it is a physical channel or not
 *  and pass on that information to graph
 */
void BsOscChanChan::yChanged()
{
	int id;
	bool physical;

	int row = BsAbstractItemView::selectedRow(ui.selY->view());
	if (row == -1) {
		return;
	}

	if (m_model_y != Q_NULLPTR && m_model_y->rowToInfo(row, physical, id)) {
		ui.plot->setY(physical, id);
	}
}
