/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_OSC_CHAN_CHAN_H
#define BOX0_STUDIO_OSC_CHAN_CHAN_H

#include <QObject>
#include <QWidget>
#include "widgets/osc/channel_manager/channel_manager.h"
#include "model.h"
#include "widgets/osc/plot/chan_chan.h"

#include "ui_osc_chan_chan.h"

class BsOscChanChan : public QWidget
{
	Q_OBJECT

	public:
		BsOscChanChan(QWidget *parent = Q_NULLPTR);

		void setChannelManager(BsOscChannelManager *channelManager);
		BsOscPlotChanChan *plot();

	private Q_SLOTS:
		void xChanged();
		void yChanged();

	private:
		Ui::BsOscChanChan ui;
		BsOscChanChanModel *m_model_x = Q_NULLPTR, *m_model_y = Q_NULLPTR;
};

#endif
