/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "model.h"
#include <QDebug>

BsOscChanChanModel::BsOscChanChanModel(
	BsOscChannelManager *channelManager, QObject *parent) :
	QAbstractItemModel(parent),
	m_channelManager(channelManager)
{
}

/* return true if succeeded, false on failure */
bool BsOscChanChanModel::rowToInfo(int r, bool &physical, int &id)
{
	int physical_channels = m_channelManager->m_physical_channel.count();
	int virtual_channels = m_channelManager->m_virtual_channel.count();

	if (r < physical_channels) {
		physical = true;
		id = r;
		return true;
	} else {
		r -= physical_channels;
		if (r < virtual_channels) {
			physical = false;
			id = r;
			return true;
		}
	}

	return false;
}

int BsOscChanChanModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return 1;
}

int BsOscChanChanModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	int physical_channels = m_channelManager->m_physical_channel.count();
	int virtual_channels = m_channelManager->m_virtual_channel.count();
	return physical_channels + virtual_channels;
}

static QVariant physical_data(BsOscChannelManager *channelManager,
	int r, int role)
{
	const BsOscChannelPhysical *channel =
		channelManager->m_physical_channel.at(r);

	if (role == Qt::ForegroundRole) {
		return channel->color;
	} else if (role == Qt::DisplayRole) {
		QString id = BsOscChannelPhysical::strID(r);
		return QString("%1 ― %2").arg(id).arg(channel->title);
	}

	return QVariant();
}

static QVariant virtual_data(BsOscChannelManager *channelManager,
	int r, int role)
{
	const BsOscChannelVirtual *channel =
		channelManager->m_virtual_channel.at(r);

	if (role == Qt::ForegroundRole) {
		return channel->color;
	} else if (role == Qt::DisplayRole) {
		QString title = channel->title;
		if (title.isEmpty()) {
			title = channel->equation;
		}
		return QString("%1 ― %2").arg(channel->strID()).arg(title);
	}

	return QVariant();
}

QVariant BsOscChanChanModel::data(const QModelIndex &index, int role) const
{
	if (! index.isValid()) {
		return QVariant();
	}

	if (index.column() != 0) {
		return QVariant();
	}

	int physical_channels = m_channelManager->m_physical_channel.count();
	int virtual_channels = m_channelManager->m_virtual_channel.count();
	int r = index.row();


	if (r < physical_channels) {
		return physical_data(m_channelManager, r, role);
	} else {
		r -= physical_channels;
		if (r < virtual_channels) {
			return virtual_data(m_channelManager, r, role);
		}
	}

	return QVariant();
}


bool BsOscChanChanModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	Q_UNUSED(index);
	Q_UNUSED(value);
	Q_UNUSED(role);
	return false;
}

Qt::ItemFlags BsOscChanChanModel::flags(const QModelIndex &index) const
{
	Q_UNUSED(index);
	return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

QVariant BsOscChanChanModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	Q_UNUSED(section);
	Q_UNUSED(orientation);
	Q_UNUSED(role);
	return QVariant();
}

QModelIndex BsOscChanChanModel::index(int row, int column, const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	Q_ASSERT(column == 0);
	return createIndex(row, column, (void *) 0);
}

QModelIndex BsOscChanChanModel::parent(const QModelIndex & index) const
{
	Q_UNUSED(index);
	return QModelIndex();
}

void BsOscChanChanModel::fakeReset()
{
	beginResetModel();
	endResetModel();
}
