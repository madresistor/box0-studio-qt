/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dio_pin.h"
#include <QMessageBox>
#include <QShortcut>
#include <QMenu>
#include <QPainter>
#include <QDebug>
#include <QSizePolicy>
#include <QSize>
#include <QSvgRenderer>
#include "extra/box0/result_code.h"
#include "extra/box0/box0.h"

#define INDEX_INPUT 0
#define INDEX_OUTPUT 1

BsDioPin::BsDioPin(uint pin, b0_dio *module, QWidget *parent) :
	QGroupBox(parent)
{
	ui.setupUi(this);

	connect(this, SIGNAL(toggled(bool)), SLOT(hizChanged(bool)));
	connect(ui.value, SIGNAL(toggled(bool)), SLOT(valueChanged(bool)));
	connect(ui.dir, SIGNAL(currentIndexChanged(int)), SLOT(dirChanged(int)));

	/* get pin string */
	if (module->label.pin[pin] != NULL) {
		pinName = utf8_to_qstring(module->label.pin[pin]);
	} else {
		pinName = QString::number(pin);
	}

	this->module = module;
	this->pinIndex = pin;
	this->refreshTimerId = 0;
	this->pinHiz = false;
	this->pinValue = false;
	this->pinDir = PinOutput;

	setTitle(pinName);
	setChecked(true);
	ui.dir->setCurrentIndex(INDEX_OUTPUT);
}

void BsDioPin::hizChanged(bool value)
{
	hardwareHiz(!value);
	ui.dir->setEnabled(value);
	ui.value->setEnabled(value);
}
void BsDioPin::valueChanged(bool value)
{
	if (pinDir != PinOutput) {
		return;
	}

	hardwareWrite(value);
}

void BsDioPin::dirChanged(int index)
{
	hardwareMode(index == INDEX_OUTPUT ? PinOutput : PinInput);
}

QSize BsDioPin::sizeHint() const
{
	return QSize(160, 180);
}

/**
 * Set the mode of the pin
 * @param mode Mode
 */
void BsDioPin::hardwareMode(PinMode mode)
{
	stopUpdate();

	/* true: output, false: input */
	bool dir = (mode == PinOutput) ? B0_DIO_OUTPUT : B0_DIO_INPUT;
	b0_result_code r = b0_dio_dir_set(module, pinIndex, dir);
	if (r < 0) {
		BsResultCode rc(r);
		rc.setTitle(tr("configuration failed"));
		rc.setMessage(tr("Unable to apply direction to pin ") + pinName);
		rc.exec();
	} else {
		pinDir = mode;
	}

	ui.value->setOutput(mode == PinOutput);

	if (mode == PinOutput) {
		/* reset to low when mode is output */
		hardwareWrite(false);
	} else {
		/* IMPORTANT TODO: read value using multiple pins, to mantain effeciency
		 * using that method, for n channels, request can be made in one call
		 */
		startUpdate();
	}
}

/**
 * Toggle the pin value
 * @note if function is useful only when pin is in output mode
 */
void BsDioPin::hardwareToggle()
{
	if (pinDir != PinOutput) {
		return;
	}

	b0_result_code r = b0_dio_value_toggle(module, pinIndex);
	if (r < 0) {
		BsResultCode rc(r);
		rc.setTitle(tr("Toggle failed"));
		rc.setMessage(tr("Unable to toggle pin ") + pinName);
		rc.exec();
	}
}

/**
 * Modify the pin hiz bit
 * @param value new HiZ value
 */
void BsDioPin::hardwareHiz(bool value)
{
	const char *value_str = value ? "On" : "Off";
	qDebug() << "performing hiz = " << value_str;

	b0_result_code r = b0_dio_hiz_set(module, pinIndex, value);
	if (r < 0) {
		BsResultCode rc(r);
		rc.setTitle(tr("HiZ Value set failed"));
		rc.setMessage(tr("Unable to turn pin ") + pinName + tr(" HiZ ") + tr(value_str));
		rc.exec();
	} else {
		this->pinHiz = value;
	}

	ui.value->setEnabled(!this->pinHiz);
}

/**
 * Set value to the pin
 * @param value Pin new value
 * @note this method is only valid when pin is in output mode
 */
void BsDioPin::hardwareWrite(bool value)
{
	if (pinDir != PinOutput) {
		return;
	}

	const char *value_str = value ? "High" : "Low";
	qDebug() << "performing" << value_str;

	b0_result_code r = b0_dio_value_set(module, pinIndex, value);
	if (r < 0) {
		BsResultCode rc(r);
		rc.setTitle(tr("Value set failed"));
		rc.setMessage(tr("Unable to make pin ") + pinName + tr(" to ") + tr(value_str));
		rc.exec();
	} else {
		this->pinValue = value;
	}

	ui.value->setChecked(this->pinValue);
}

/**
 * Read pin value and update the ui accordingly
 * @note this method is only valid when pin is in input mode
 * @note this method is periodically called by a timer
 */
void BsDioPin::hardwareRead()
{
	if (pinDir != PinInput) {
		return;
	}

	if (b0_dio_value_get(module, this->pinIndex, &(this->pinValue)) < 0) {
		//QString msg = tr("Unable to read pin ") + pinName;
		//QMessageBox::warning(this, tr("Value set failed"), msg);
		//stopUpdate();
		qWarning() << "read failed for " << this->pinIndex;
	}

	ui.value->setChecked(this->pinValue);
}

/**
 * Timer event handler.
 * In background, periodically input pin data is updated
 */
void BsDioPin::timerEvent(QTimerEvent * event)
{
	if (event->timerId() == refreshTimerId) {
		hardwareRead();
	} else {
		QWidget::timerEvent(event);
	}
}

/**
 * Start the pin background value updater
 * @note this method is only valid when the pin is in input mode
 */
void BsDioPin::startUpdate()
{
	if (pinDir != PinInput) {
		return;
	}

	refreshTimerId = startTimer(100);
	if (refreshTimerId == 0) {
		qCritical() << "unable to start timer for pin" << pinName;
	}
}

/**
 * Stop the timer (background value updater)
 */
void BsDioPin::stopUpdate()
{
	if (refreshTimerId > 0) {
		killTimer(refreshTimerId);
		refreshTimerId = 0;
	}
}
