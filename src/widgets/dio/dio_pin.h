/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_DIO_PIN_H
#define BOX0_STUDIO_DIO_PIN_H

#include <QObject>
#include <QWidget>
#include <QGroupBox>
#include <libbox0/libbox0.h>
#include <QPoint>
#include <QAbstractButton>
#include <QTimerEvent>
#include <QResizeEvent>
#include <QPaintEvent>
#include <QPixmap>
#include <QMenu>
#include <QAction>

#include "ui_dio_pin.h"

/* a portion of this class is from QLedIndicator (TODO: place project URL) */
class BsDioPin : public QGroupBox
{
	Q_OBJECT

	public:
		enum PinMode {PinOutput, PinInput};

		BsDioPin(uint pin, b0_dio *module, QWidget *widget = Q_NULLPTR);
		void stopUpdate();
		void startUpdate();
		QSize sizeHint() const;

	/* worker method */
	private:
		void hardwareToggle();
		void hardwareHiz(bool value);
		void hardwareWrite(bool value);
		void hardwareRead();
		void hardwareMode(PinMode mode);

	/* user interaction */
	private Q_SLOTS:
		void hizChanged(bool value);
		void valueChanged(bool value);
		void dirChanged(int value_index);

	protected:
		void timerEvent(QTimerEvent * event);

	private:
		b0_dio *module;
		uint pinIndex;
		bool pinValue;
		bool pinHiz;
		PinMode pinDir;
		int refreshTimerId;
		QString pinName;

		QAction *action_input, *action_output, *action_hiz;
		Ui::BsDioPin ui;
};

#endif
