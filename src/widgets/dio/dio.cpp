/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dio.h"
#include <QMessageBox>
#include <QApplication>
#include <QDesktopWidget>
#include "extra/box0/result_code.h"

BsDio::BsDio(QWidget *parent) :
	BsInstrument(B0_DIO, parent)
{
	ui.setupUi(this);

	layout = new FlowLayout(ui.scrollAreaWidgetContents, 5, -1, -1);
	ui.scrollAreaWidgetContents->setLayout(layout);
}

void BsDio::loadModule(b0_device *dev, int index)
{
	b0_result_code r;

	/* search for module to bind to */
	r = b0_dio_open(dev, &module, index);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("DIO"));
		rc.setMessage(tr("Open failed"));
		rc.exec();
		deleteLater();
		return;
	}

	r = b0_dio_basic_prepare(module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Prepare failed"));
		rc.setMessage(tr("Failed to prepare DIO for basic mode"));
		rc.exec();
		deleteLater();
		return;
	}

	/* make all pin to output */
	r = b0_dio_all_dir_set(module, B0_DIO_OUTPUT);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("DIO prepare"));
		rc.setMessage(tr("Failed to set all DIO pin to output mode"));
		rc.exec();
		return;
	}

	/* make all pin low */
	r = b0_dio_all_value_set(module, B0_DIO_LOW);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("DIO prepare"));
		rc.setMessage(tr("Failed to set all DIO pin value to low"));
		rc.exec();
		deleteLater();
		return;
	}

	/* disable hiz */
	r = b0_dio_all_hiz_set(module, B0_DIO_DISABLE);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("DIO prepare"));
		rc.setMessage(tr("Failed to disable all DIO pin hiz"));
		rc.exec();
		deleteLater();
		return;
	}

	/* add pins */
	for (uint i = 0; i < module->pin_count; i++) {
		BsDioPin *pin = new BsDioPin(i, module, this);
		pin->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
		pins.append(pin);
		layout->addWidget(pin);
	}

	/* Start basic mode */
	r = b0_dio_basic_start(module);
	if (B0_ERR_RC(r)) {
		BsResultCode rc(r);
		rc.setTitle(tr("Start failed"));
		rc.setMessage(tr("Failed to start DIO for basic mode"));
		rc.exec();
		deleteLater();
		return;
	}

	showModuleInTitle(&module->header);
}

void BsDio::closeEvent(QCloseEvent *event)
{
	if (module != NULL) {
		/* make all pins in output, low */
		for (int i = 0; i < pins.count(); i++) {
			/* stop reading from hardware */
			pins.at(i)->stopUpdate();
		}

		/* stop basic */
		BsResultCode::handleInternal(b0_dio_basic_stop(module),
			"unable to stop in basic mode");

		/* close the module */
		BsResultCode::handleInternal(b0_dio_close(module),
			"unable to close DIO module");

		module = NULL;
	}

	BsInstrument::closeEvent(event);
}
