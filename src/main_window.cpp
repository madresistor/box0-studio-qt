/*
 * This file is part of box0-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "main_window.h"
#include <QApplication>
#include <QDesktopWidget>
#include <QMdiSubWindow>
#include <QDebug>
#include "about.h"

#include <QSettings>
#include "widgets/welcome/welcome.h"
#include "widgets/widget/widget.h"
#include "extra/box0/result_code.h"
#include "extra/box0/box0.h"

static const QString GROUP_ID = "main-window";
static const QString WIN_GEO_ID = "window-geometry";
static const QString WIN_STATE_ID = "window-state";

BsMainWindow::BsMainWindow(QWidget *parent) :
	QMainWindow(parent)
{
	ui.setupUi(this);

	/* load settings */
	QSettings s;
	s.beginGroup(GROUP_ID);
	if (s.contains(WIN_GEO_ID) && s.contains(WIN_STATE_ID)) {
		restoreGeometry(s.value(WIN_GEO_ID).toByteArray());
		restoreState(s.value(WIN_STATE_ID).toByteArray());
	} else {
		showMaximized();
	}
	s.endGroup();

	BsWelcome *front = new BsWelcome(this);
	addWidget(front);

	/* BsWelcome is a bit special BsWidget */
	connect(front, SIGNAL(addWidget(BsWidget*)), SLOT(addWidget(BsWidget*)));

	/* user requested to close a widget */
	connect(ui.tabWidget, SIGNAL(tabCloseRequested(int)), SLOT(removeWidget(int)));

	/* exit the application */
	connect(ui.actionExit, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));

	device_status = new QLabel(this);
	device_status->setObjectName("device-status");
	statusBar()->addPermanentWidget(device_status);

	findDevice();

	pingDevice();
}

BsMainWindow::~BsMainWindow()
{
	if (device != NULL) {
		BsResultCode::handleInternal(b0_device_close(device), "unable to close device");
	}
}

void BsMainWindow::findDevice()
{
	if (device != NULL) {
		BsResultCode::handleInternal(b0_device_close(device), "unable to close device");
		device = NULL;
	}

	/* open device */
	if (b0_usb_open_supported(&device) < 0) {
		QString msg = "No Box0 compatible device found";
		statusBar()->showMessage(msg, 5000);
		device = NULL;
		return;
	}

	for (int i = 0; i < ui.tabWidget->count(); i++) {
		BsWidget *widget = static_cast<BsWidget *>(ui.tabWidget->widget(i));
		widget->setDevice(device);
	}
}

void BsMainWindow::addWidget(BsWidget *widget)
{
	int i = ui.tabWidget->addTab(widget, widget->windowTitle());
	ui.tabWidget->setCurrentIndex(i);
	/* FIXME: connect windowTitle changed to update tab title */

	/* when the widget is closed, delete it.
	 *  This will trigger a close of the tab too..
	 */
	widget->setAttribute(Qt::WA_DeleteOnClose);

	connect(widget, SIGNAL(showAsWindow(BsWidget *, bool)),
		SLOT(showAsWindow(BsWidget *, bool)));
}

void BsMainWindow::closeEvent(QCloseEvent *event)
{
	QSettings s;
	s.beginGroup(GROUP_ID);
	s.setValue(WIN_GEO_ID, saveGeometry());
	s.setValue(WIN_STATE_ID, saveState());
	s.endGroup();

	/* Close tabs */
	for (int i = 0; i < ui.tabWidget->count(); i++) {
		QWidget *widget = ui.tabWidget->widget(i);
		widget->close();
	}

	/* close full windows */
	for (int i = 0; i < full_windows.size(); i++) {
		QWidget *widget = full_windows[i];
		widget->close();
	}

	QMainWindow::closeEvent(event);
}

void BsMainWindow::removeWidget(int index)
{
	QWidget *widget = ui.tabWidget->widget(index);
	if (widget->close()) {
		ui.tabWidget->removeTab(index);
	}
}

bool BsMainWindow::pingDevice()
{
	if (device == NULL) {
		setDeviceStatus("Nothing connected!", Qt::darkRed);
		return false;
	}

	b0_result_code r = b0_device_ping(device);
	if (B0_ERR_RC(r)) {
		BsResultCode::handleInternal(r, "device ping failed, disconnected?");
		setDeviceStatus("Failed to communicated with device", Qt::darkRed);
	} else {
		QString name = utf8_to_qstring(device->name);
		QString msg = QString("\"%1\" connected!").arg(name);
		setDeviceStatus(msg, Qt::darkGreen);
	}

	return B0_SUC_RC(r);
}

void BsMainWindow::setDeviceStatus(const QString &status, const QColor &color)
{
	device_status->setText(status);
	setWindowTitle(status);

	QPalette palette = device_status->palette();
	palette.setColor(device_status->foregroundRole(), color);
	device_status->setPalette(palette);
}

void BsMainWindow::showAsWindow(BsWidget *w, bool show_as_window)
{
	if (show_as_window) {
		/* search for the item in `ui.tabWidget` and make it full window */
		int index = ui.tabWidget->indexOf(w);
		if (index < 0) {
			qWarning() << w << " not found in ui.tabWidget";
			return;
		}

		/* remove the tab and show the widget as a standalone window. */
		full_windows.append(w);
		ui.tabWidget->removeTab(index);
		w->setParent(Q_NULLPTR);
		w->show();
	} else {
		/* search for the item in `full_windows` and move it to tab */
		int index = full_windows.indexOf(w);
		if (index < 0) {
			qWarning() << w << " not found in full_windows";
			return;
		}

		/* add a tab and make the widget a child of ui.tabWidget */
		full_windows.removeAt(index);
		index = ui.tabWidget->addTab(w, w->windowTitle());
		ui.tabWidget->setCurrentIndex(index);
	}
}
