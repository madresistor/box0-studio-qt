# - Try to find lmfit
#
#  LMFIT_FOUND - system has lmfit
#  LMFIT_INCLUDE_DIRS - the lmfit include directory
#  LMFIT_LIBRARIES - Link these to use lmfit
#  LMFIT_DEFINITIONS - Compiler switches required for using lmfit
#
#  Adapted from cmake-modules Google Code project
#
#  Copyright (c) 2006 Andreas Schneider <mail@cynapses.org>
#  Copyright (c) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# Redistribution and use is allowed according to the terms of the New BSD license.
#
# CMake-Modules Project New BSD License
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# * Neither the name of the CMake-Modules Project nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

FIND_PATH(LMFIT_INCLUDE_DIR
	NAMES lmmin.h
	PATHS
		/usr/include
		/usr/local/include
		/opt/local/include
		/sw/include
)

# Note: Only searching for "lmfit.so"
FIND_LIBRARY(LMFIT_LIBRARY
	NAMES lmfit
	PATHS
		/usr/lib
		/usr/local/lib
		/opt/local/lib
		/sw/lib
)

IF (LMFIT_INCLUDE_DIR AND LMFIT_LIBRARY)
	SET(LMFIT_FOUND TRUE)
	SET(LMFIT_INCLUDE_DIRS ${LMFIT_INCLUDE_DIR})
	SET(LMFIT_LIBRARIES ${LMFIT_LIBRARY})
ENDIF (LMFIT_INCLUDE_DIR AND LMFIT_LIBRARY)

IF (LMFIT_FOUND)
	IF (NOT lmfit_FIND_QUIETLY)
		MESSAGE(STATUS "Found lmfit:")
		MESSAGE(STATUS " - Includes: ${LMFIT_INCLUDE_DIRS}")
		MESSAGE(STATUS " - Libraries: ${LMFIT_LIBRARIES}")
	ENDIF (NOT lmfit_FIND_QUIETLY)
ELSE (LMFIT_FOUND)
	IF (lmfit_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "Could not find lmfit")
	ENDIF (lmfit_FIND_REQUIRED)
ENDIF (LMFIT_FOUND)

# show the LMFIT_INCLUDE_DIRS and LMFIT_LIBRARIES variables only in the advanced view
MARK_AS_ADVANCED(LMFIT_INCLUDE_DIRS LMFIT_LIBRARIES)
