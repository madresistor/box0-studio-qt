# - Try to find libbox0
#
#  LIBBOX0_FOUND - system has libbox0
#  LIBBOX0_INCLUDE_DIRS - the libbox0 include directory
#  LIBBOX0_LIBRARIES - Link these to use libbox0
#  LIBBOX0_DEFINITIONS - Compiler switches required for using libbox0
#
#  Adapted from cmake-modules Google Code project
#
#  Copyright (c) 2006 Andreas Schneider <mail@cynapses.org>
#  Copyright (c) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# Redistribution and use is allowed according to the terms of the New BSD license.
#
# CMake-Modules Project New BSD License
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# * Neither the name of the CMake-Modules Project nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

FIND_PATH(LIBBOX0_INCLUDE_DIR
	NAMES libbox0/libbox0.h
	PATHS
		/usr/include
		/usr/local/include
		/opt/local/include
		/sw/include
)

# Note: Only searching for "libbox0.so"
FIND_LIBRARY(LIBBOX0_LIBRARY
	NAMES box0
	PATHS
		/usr/lib
		/usr/local/lib
		/opt/local/lib
		/sw/lib
)

IF (LIBBOX0_INCLUDE_DIR AND LIBBOX0_LIBRARY)
	SET(LIBBOX0_FOUND TRUE)
	SET(LIBBOX0_INCLUDE_DIRS ${LIBBOX0_INCLUDE_DIR})
	SET(LIBBOX0_LIBRARIES ${LIBBOX0_LIBRARY})
ENDIF (LIBBOX0_INCLUDE_DIR AND LIBBOX0_LIBRARY)

IF (LIBBOX0_FOUND)
	IF (NOT libbox0_FIND_QUIETLY)
		MESSAGE(STATUS "Found libbox0:")
		MESSAGE(STATUS " - Includes: ${LIBBOX0_INCLUDE_DIRS}")
		MESSAGE(STATUS " - Libraries: ${LIBBOX0_LIBRARIES}")
	ENDIF (NOT libbox0_FIND_QUIETLY)
ELSE (LIBBOX0_FOUND)
	IF (libbox0_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "Could not find libbox0")
	ENDIF (libbox0_FIND_REQUIRED)
ENDIF (LIBBOX0_FOUND)

# show the LIBBOX0_INCLUDE_DIRS and LIBBOX0_LIBRARIES variables only in the advanced view
MARK_AS_ADVANCED(LIBBOX0_INCLUDE_DIRS LIBBOX0_LIBRARIES)
